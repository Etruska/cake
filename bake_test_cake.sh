#!/bin/bash

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

CMD="cd $DIR && "

BUILD_FOLDER="$DIR/build-test"

CMAKE=$(which cmake)

rm -rf $BUILD_FOLDER/*

BUILD_CMD="cd $BUILD_FOLDER && $CMAKE cmake -G \"Unix Makefiles\" .."
echo $BUILD_CMD

eval $BUILD_CMD

cd $BUILD_FOLDER && make cake-test

$BUILD_FOLDER/cake-test

