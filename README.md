Cake++
====================

Features
--------------------
### Parsing

Syntax example:
```
* MyClass
	@x
	@y
	@z
	+ init(x, y, z)
	    @x = x
		@y = y
		@z = z

	+ publicMethod(x, y, z)
		@x = 10 + 5
		@y = new(MyClass)
		@y.init(z,y,z)

		tmp = x.data(x, y)

		? protectedMethod(tmp, y, z) > x
			| "ok"
		:? x < 1
			| "maybe ok"
		:
			| "not ok"

		i = 0
		% i < 10
			| i
			i = i + 1

		<< @x
```
### Compilation

After parsing source to ast, it is compiled to bytecode (example in `debug_output.txt`)

### Virtual Machine
* Primitives: String, Integer
* User defined functions and classes, object allocation on heap, setters and getters
* Built-in classes
* Trampoline used when creating new stack frames
* Available instructions:
    * _STORE_: stores variable to locals pool
    * _LOAD_: loads variable from locals pool
    * _PUSH_CONST_: pushes constant from constant pool
    * _GOTO_: skips to selected bytecode location
    * _NEW_: creates new instance on heap
    * _IF_CMPR_: conditional jump
    * _GETFIELD_: gets property from object
    * _SETFIELD_: sets property to object
    * _INVOKE_: invokes object method
    * And other operator instructions
 
#### Garbage collection
Basic Baker's garbage collector. Interpreter uses proxy object to refer to objects on heap. That creates abstraction layer between Interpreter and actual position of object in heap and therefore simplifying moving objects on heap.

### Testing
Source code is tested with library gtest. Output example is in `test_output.txt`.

Dependencies
--------------------
- C++11
- CMake >= 2.8.8

### Test dependencies
- SVN > 1.7


Compile
--------------------
Following section expects to current director be `source_code`.

```
./bake_cake.sh
```

### Run
```
build/cake --file="/path/to/source/file.cake" --class="MyClass" --method="myMethod"
```

Class represents methods represents initial instanace and method invoked. Default value for class is `Base` and for method `main`. File parameter is mandatory.

Compile and run tests
--------------------
```
./bake_test_cake.sh
```

Test program
--------------------
As example program is implemented the SAT CNF solver. Source code is in `sat.cake`, output in `output.txt` and a part of output with debug in `debug_output.txt`. In file `test.dat` is tested formula in format of variable ids concatenated with ',' and to them is appended mask with flag if variable is negated. Every line represent one clause. 

Output is printed on standard output so to save it to file use `./cake <params> > output.txt`. 
