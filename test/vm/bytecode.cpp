#include "gtest/gtest.h"
#include "VM/Bytecode.h"
#include "VM/VMException.h"
#include "VM/IS.h"

using namespace VM;


TEST(VM__BYTECODE, bytecode_empty) {

    unsigned char *bytecode = NULL;

    Bytecode *test1 = new Bytecode();
    test1->loadCode(bytecode, 0);


    ASSERT_FALSE(test1->hasNext());

}

TEST(VM__BYTECODE, bytecode) {

    unsigned char *bytecode = new unsigned char[3];
    bytecode[0] = IS::FETCH;
    bytecode[1] = IS::STORE;
    bytecode[2] = IS::LOAD;
    Bytecode *bytecodeObj = new Bytecode();

    bytecodeObj->loadCode(bytecode, 3);

    ASSERT_TRUE(bytecodeObj->hasNext());
    unsigned char next = bytecodeObj->getNext();
    cout << "FETCH: " << IS::FETCH << " =? " << (int) next << endl;
    ASSERT_EQ(IS::FETCH, (int) next);
    ASSERT_EQ(IS::STORE, (int) bytecodeObj->getNext());
    ASSERT_TRUE(bytecodeObj->hasNext());

    ASSERT_EQ(IS::LOAD, (int) bytecodeObj->getNext());
    ASSERT_FALSE(bytecodeObj->hasNext());

}

TEST(VM__BYTECODE, set_bp) {

    unsigned char *bytecode = new unsigned char[3];
    bytecode[0] = IS::FETCH;
    bytecode[1] = IS::STORE;
    bytecode[2] = IS::LOAD;
    Bytecode *bytecodeObj = new Bytecode();

    bytecodeObj->loadCode(bytecode, 3);

    bytecodeObj->setPointer(2);
    ASSERT_EQ((int) bytecodeObj->getNext(), IS::LOAD);

    ASSERT_THROW(bytecodeObj->setPointer(999), VMException::Bytecode);

}
