#include "gtest/gtest.h"
#include "VM/ConstantPool.h"
#include "VM/Integer.h"
#include "VM/VMString.h"
#include <string>

using namespace VM;
using namespace std;


TEST(VM__CONSTANTPOOL, poolIndex) {

    ConstantPool *pool = new ConstantPool();
    Integer *myInt = new Integer(8);
    Integer *myInt2 = new Integer(8);
    Integer *myInt3 = new Integer(10);

    int index = pool->add(myInt);
    int index2 = pool->add(myInt2);
    ASSERT_EQ(index2, index);
    int index3 = pool->add(myInt3);
    ASSERT_EQ(index3, index + 1);
}

TEST(VM__CONSTANTPOOL, pool) {

    ConstantPool *pool = new ConstantPool();
    Integer *myInt = new Integer(5);
    Integer *myInt2 = new Integer(9);
    string hello = "Hello World.";
    string nope = "nope.";
    VMString *myString = new VMString(hello);
    VMString *myString2 = new VMString(hello);
    VMString *myString3 = new VMString(nope);

    int myIntIndex = pool->add(myInt);
    int myInt2Index = pool->add(myInt2);
    int stringIndex = pool->add(myString);
    VMString *sFromPool = (VMString *) pool->get(stringIndex);

    Integer *fromPool = (Integer *) pool->get(myInt2Index);
    ASSERT_EQ(fromPool->getValue(), myInt2->getValue());
    ASSERT_NE(fromPool->getValue(), myInt->getValue());
    ASSERT_TRUE(myString2->equals(sFromPool));
    ASSERT_FALSE(myString3->equals(sFromPool));
}
