#include <VM/Bytecode.h>
#include <VM/IS.h>
#include <VM/VMCore.h>
#include "gtest/gtest.h"
#include "VM/VMCore.h"
#include "VM/VMObject.h"
#include "VM/Integer.h"
#include "VM/VMException.h"
#include "VM/VMClass.h"
#include "VM/VMField.h"
#include "Heap/HeapManager.h"
#include "Heap/HeapException.h"
#include <string>
#include <sstream>

using namespace VM;
using namespace Heap;
using namespace std;


VMClass *myClass = new VMClass("cat", NULL);
VMField *intField = new VMField();
int intFieldIndex = myClass->addField(intField);
HeapManager *heapManager = new HeapManager(1000000, false);

TEST(HEAP__CORE, allocate_integer) {
    Integer* intObj = (Integer*)heapManager->createInteger(8)->instance();

    ASSERT_NE(intObj->getValue(), 0);
    ASSERT_EQ(intObj->getValue(), 8);
}

TEST(HEAP__CORE, gc_insufficient_memory) {
    HeapManager *heap = new HeapManager(sizeof(Integer) / 2);

    ASSERT_THROW(heap->createInteger(1), HeapException::GC);
}


TEST(HEAP__CORE, gc_trigger__integer) {
    HeapManager *heap = new HeapManager(sizeof(Integer) * 3, false);

    VMObjectProxy* intProxy1 = heap->createInteger(1);
    VMObjectProxy* intProxy2 = heap->createInteger(2);
    VMObjectProxy* intProxy3 = heap->createInteger(3);
    intProxy1->alive = true;
    intProxy2->alive = false;
    intProxy3->alive = true;

    VMObjectProxy* intProxy4 = heap->createInteger(4);

    Integer* intObj1 = (Integer*)intProxy1->instance();
    Integer* intObj3 = (Integer*)intProxy3->instance();
    Integer* intObj4 = (Integer*)intProxy4->instance();

    ASSERT_EQ(intObj1->getValue(), 1);
    ASSERT_EQ(NULL, intProxy2->instance());
    ASSERT_EQ(intObj3->getValue(), 3);
    ASSERT_EQ(intObj4->getValue(), 4);

    intProxy1->alive = true;
    intProxy3->alive = true;
    intProxy4->alive = false;

    intProxy2 = heap->createInteger(2);

    intObj1 = (Integer*)intProxy1->instance();
    Integer* intObj2 = (Integer*)intProxy2->instance();
    intObj3 = (Integer*)intProxy3->instance();

    ASSERT_EQ(intObj1->getValue(), 1);
    ASSERT_EQ(intObj2->getValue(), 2);
    ASSERT_EQ(intObj3->getValue(), 3);
    ASSERT_EQ(NULL, intProxy4->instance());
}



TEST(HEAP__CORE, get_field) {

    VMObjectRef *obj = (VMObjectRef*)heapManager->createObject(myClass)->instance();
    heapManager->setProperty(obj, intFieldIndex, (Integer *) heapManager->createInteger(8));
    Integer *intProp = (Integer *) heapManager->getProperty(obj, intFieldIndex)->instance();

    ASSERT_NE(intProp->getValue(), 0);
    ASSERT_EQ(intProp->getValue(), 8);
}

TEST(HEAP__CORE, field_copy) {

    //first we copy int prop from obj1 to obj2
    VMObjectRef *obj1 = (VMObjectRef*)heapManager->createObject(myClass)->instance();
    heapManager->setProperty(obj1, intFieldIndex, (Integer*)heapManager->createInteger(8)->instance());
    Integer *obj1Int = (Integer *) heapManager->getProperty(obj1, intFieldIndex)->instance();

    ASSERT_EQ(obj1Int->getValue(), 8);

    VMObjectRef *obj2 = (VMObjectRef*)heapManager->createObject(myClass)->instance();
    heapManager->setProperty(obj2, intFieldIndex, obj1Int);
    Integer *obj2Int = (Integer *) heapManager->getProperty(obj2, intFieldIndex)->instance();

    ASSERT_EQ(obj2Int->getValue(), 8);

    //now we change the obj1 property, but obj2 should remain the same
    obj1Int->setValue(11);
    Integer *obj1Int2 = (Integer *) heapManager->getProperty(obj1, intFieldIndex)->instance();
    Integer *obj2Int2 = (Integer *) heapManager->getProperty(obj2, intFieldIndex)->instance();

    ASSERT_EQ(obj1Int2->getValue(), 11);
    ASSERT_EQ(obj2Int2->getValue(), 8);

}


TEST(HEAP__CORE, object__gc) {

    // just to get propper size
    VMObjectRef *obj = (VMObjectRef*)(heapManager->createObject(myClass)->instance());

    HeapManager *heap = new HeapManager(obj->heapSize() + 3 * sizeof(Integer), false);

    VMObjectProxy* objProxy = heap->createObject(myClass);
    obj = (VMObjectRef*)objProxy->instance();

    VMObjectProxy* toBeCopied = heap->createInteger(8);

    heap->setProperty(obj, intFieldIndex, toBeCopied);
    VMObjectProxy* intPropProxy = heap->getProperty(obj, intFieldIndex);


    objProxy->alive = true;
    intPropProxy->alive = true;

    // invoke GC
    heap->createInteger(2);
    heap->createInteger(666);

    Integer *intProp = (Integer *) intPropProxy->instance();

    ASSERT_EQ(intProp->getValue(), 8);
    ASSERT_EQ(NULL, toBeCopied->instance());

}
