#include <VM/Bytecode.h>
#include <VM/IS.h>
#include <VM/VMCore.h>
#include "gtest/gtest.h"
#include "VM/VMCore.h"
#include "VM/VMObject.h"
#include "VM/Stack.h"
#include "VM/Integer.h"
#include "VM/VMString.h"
#include "VM/VMException.h"
#include "Heap/HeapManager.h"
#include "VM/ClassPool.h"
#include "VM/VMClass.h"
#include <string>
#include <sstream>

using namespace VM;
using namespace Heap;
using namespace std;

#define NEW__VM_CORE new VMCore(new HeapManager(100000));

ConstantPool *pool = new ConstantPool();
Integer *myInt = new Integer(999);
Integer *int1 = new Integer(1);
Integer *int2 = new Integer(2);

VMString *helloString = new VMString("Hello World.");
VMString *byeString = new VMString("Bye.");
VMString *ifString = new VMString("IF part");
VMString *elseString = new VMString("ELSE part");
VMString * dogName = new VMString("Lenny the dog");
int intIndex = pool->add(myInt);
int helloIndex = pool->add(helloString);
int byeIndex = pool->add(byeString);
int int1Index = pool->add(int1);
int int2Index = pool->add(int2);
int ifStringIndex = pool->add(ifString);
int elseStringIndex = pool->add(elseString);
int dogNameIndex = pool->add(dogName);
int gnMethodStringIndex = pool->add(new VMString("Get name function"));

VMMethod *getNameMethod(){
    stringstream ss;
    ss << (char) IS::PUSH_CONST << (char) gnMethodStringIndex;
    ss << (char) IS::D_PRINT;
    ss << (char) IS::PUSH_CONST << (char) dogNameIndex;
    ss << (char) IS::STORE;
    ss << (char) 1;
    ss << (char) IS::LOAD;
    ss << (char) 1;
    ss << (char) IS::RETURN;
    string codeString = ss.str();
    Bytecode *bytecode = new Bytecode(codeString);

    return new VMMethod("getName", bytecode);
}
VMMethod * getPrintHelloMethod(int getNameIndex){
    stringstream ss;
    ss << (char) IS::PUSH_CONST << (char) helloIndex;
    ss << (char) IS::STORE;
    ss << (char) 1;
    ss << (char) IS::LOAD;
    ss << (char) 0; //load this object
    ss << (char) IS::INVOKE;
    ss << (char) getNameIndex;
    ss << (char) IS::STORE;
    ss << (char) 2;
    ss << (char) IS::LOAD;
    ss << (char) 1;
    ss << (char) IS::D_PRINT;
    ss << (char) IS::LOAD;
    ss << (char) 2;
    ss << (char) IS::D_PRINT;
    ss << (char) IS::LOAD;
    ss << (char) 0; //load this object
    ss << (char) IS::INVOKE;
    ss << (char) getNameIndex;
    ss << (char) IS::D_PRINT;
    ss << (char)IS::RETURN;
    string codeString = ss.str();

    Bytecode * bytecode = new Bytecode(codeString);
    return new VMMethod("printHello", bytecode);
}

/* creating methods */
int gnMethodIndex = 4;
int printHelloIndex = 5;
VMMethod * printNameMethod = getNameMethod();
VMMethod * printHelloMethod = getPrintHelloMethod(gnMethodIndex);


/* creating classPool */
ClassPool <VMClass *> * classPool = new ClassPool<VMClass *>();
VMClass *catClass = new VMClass("cat", NULL);
VMField *catIntField = new VMField();
int catIntFieldIndex = catClass->addField(catIntField);

VMClass *dogClass = new VMClass("dog", NULL);
VMField *dogIntField = new VMField();
VMField *dogStringField = new VMField();
int dogIntFieldIndex = dogClass->addField(dogIntField);
int dogStringFieldIndex = dogClass->addField(dogStringField);
int a = dogClass->methods.setItem((Method *)printNameMethod, gnMethodIndex);
int b = dogClass->methods.setItem((Method *)printHelloMethod, printHelloIndex);
//
int catClassIndex = classPool->add(catClass);
int dogClassIndex = classPool->add(dogClass);

TEST(VM__CORE, push_const) {

    Integer *myInt2 = new Integer(100);

    VMCore *core = NEW__VM_CORE;
    unsigned char *bytecode = new unsigned char[2];

    bytecode[0] = IS::PUSH_CONST;
    bytecode[1] = (char) intIndex;

    Bytecode *bytecodeObj = new Bytecode();
    bytecodeObj->loadCode(bytecode, 2);

    core->loadConstantPool(pool);
    core->runBytecode(bytecodeObj);

    VMObject *obj = core->getStack()->pop();
    ASSERT_TRUE(obj->equals(myInt));
    ASSERT_FALSE(obj->equals(myInt2));
}

TEST(VM__CORE, debug_print) {

    unsigned char *bytecode = new unsigned char[3];

    bytecode[0] = IS::PUSH_CONST;
    bytecode[1] = (char) helloIndex;
    bytecode[2] = IS::D_PRINT;

    VMCore *core = NEW__VM_CORE;
    Bytecode *bytecodeObj = new Bytecode();
    bytecodeObj->loadCode(bytecode, 3);
    core->loadConstantPool(pool);
    core->runBytecode(bytecodeObj);

    EXPECT_STREQ(core->getLastPrintline().c_str(), "(string) \"Hello World.\"\n");

    bytecode = new unsigned char[3];

    bytecode[0] = IS::PUSH_CONST;
    bytecode[1] = (char) intIndex;
    bytecode[2] = IS::D_PRINT;

    bytecodeObj = new Bytecode();
    bytecodeObj->loadCode(bytecode, 3);
    core->runBytecode(bytecodeObj);

    EXPECT_STREQ(core->getLastPrintline().c_str(), "(integer) 999\n");
}


TEST(VM__CORE, goto_test) {

    unsigned char *bytecode = new unsigned char[10];

    bytecode[0] = IS::PUSH_CONST;
    bytecode[1] = (unsigned char) helloIndex;
    bytecode[2] = IS::GOTO; //skip "Hello World." string printing and jump to end
    bytecode[3] = (char) 0;
    bytecode[4] = (char) 6;
    bytecode[5] = IS::D_PRINT;
    bytecode[6] = IS::PUSH_CONST;
    bytecode[7] = (unsigned char) byeIndex;
    bytecode[8] = IS::D_PRINT;
    bytecode[9] = IS::RETURN;

    VMCore *core = NEW__VM_CORE;
    Bytecode *bytecodeObj = new Bytecode();
    bytecodeObj->loadCode(bytecode, 10);
    core->loadConstantPool(pool);
    core->runBytecode(bytecodeObj);

    EXPECT_STREQ(core->printStream.str().c_str(), "(string) \"Bye.\"\n");
}

TEST(VM__CORE, if_lt) {

    //test if(1 < 2)
    stringstream ss;
    ss << (char) IS::PUSH_CONST << (char) int1Index;
    ss << (char) IS::PUSH_CONST << (char) int2Index;
    ss << (char) VM__INSTRUCTION__OP__LESS_THAN;
    ss << (char) IS::IF_CMPR; //if 1 >= 2, go to else part
    ss << (char) 0 << (char) 14;
    ss << (char) IS::PUSH_CONST << (char) ifStringIndex;
    ss << (char) IS::D_PRINT;  //print "IF part"
    ss << (char) IS::GOTO << (char) 0 << (char) 17; //skip else part
    ss << (char) IS::PUSH_CONST << (char) elseStringIndex;
    ss << (char) IS::D_PRINT;  //print "ELSE part"
    ss << (char) IS::RETURN;

    string codeString = ss.str();

    VMCore *core = NEW__VM_CORE;
    Bytecode *bytecodeObj = new Bytecode((unsigned char *) codeString.c_str(), (int) codeString.size());

    core->loadConstantPool(pool);
    core->runBytecode(bytecodeObj);

    EXPECT_STREQ(core->printStream.str().c_str(), "(string) \"IF part\"\n");

}

TEST(VM__CORE, if_gt) {

    //test if(1 > 2)
    stringstream ss;
    ss << (char) IS::PUSH_CONST << (char) int1Index;
    ss << (char) IS::PUSH_CONST << (char) int2Index;
    ss << (char) VM__INSTRUCTION__OP__GREATER_THAN;
    ss << (char) IS::IF_CMPR; //if 1 <= 2, go to else part
    ss << (char) 0 << (char) 14;
    ss << (char) IS::PUSH_CONST << (char) ifStringIndex;
    ss << (char) IS::D_PRINT;  //print "IF part"
    ss << (char) IS::GOTO << (char) 0 << (char) 17; //skip else part
    ss << (char) IS::PUSH_CONST << (char) elseStringIndex;
    ss << (char) IS::D_PRINT;  //print "ELSE part"
    ss << (char) IS::RETURN;

    string codeString = ss.str();

    VMCore *core = NEW__VM_CORE;
    Bytecode *bytecodeObj = new Bytecode((unsigned char *) codeString.c_str(), (int) codeString.size());
    core->loadConstantPool(pool);
    core->runBytecode(bytecodeObj);

    EXPECT_STREQ(core->printStream.str().c_str(), "(string) \"ELSE part\"\n");
}

TEST(VM__CORE, if_ge) {

    //test if(1 >= 1)
    stringstream ss;
    ss << (char) IS::PUSH_CONST << (char) int1Index;
    ss << (char) IS::PUSH_CONST << (char) int1Index;
    ss << (char) VM__INSTRUCTION__OP__GREATER_OR_EQ;
    ss << (char) IS::IF_CMPR; //if 1 < 1, go to else part
    ss << (char) 0 << (char) 14;
    ss << (char) IS::PUSH_CONST << (char) ifStringIndex;
    ss << (char) IS::D_PRINT;  //print "IF part"
    ss << (char) IS::GOTO << (char) 0 << (char) 17; //skip else part
    ss << (char) IS::PUSH_CONST << (char) elseStringIndex;
    ss << (char) IS::D_PRINT;  //print "ELSE part"
    ss << (char) IS::RETURN;

    string codeString = ss.str();

    VMCore *core = NEW__VM_CORE;
    Bytecode *bytecodeObj = new Bytecode((unsigned char *) codeString.c_str(), (int) codeString.size());
    core->loadConstantPool(pool);
    core->runBytecode(bytecodeObj);

    EXPECT_STREQ(core->getLastPrintline().c_str(), "(string) \"IF part\"\n");

}

TEST(VM__CORE, locals_simple) {

    //store 1 and "Hello World." to local variables and print the string
    stringstream ss;
    ss << (char) IS::PUSH_CONST << (char) int1Index;
    ss << (char) IS::STORE << (char) 0;
    ss << (char) IS::PUSH_CONST << (char) helloIndex;
    ss << (char) IS::STORE << (char) 1;
    ss << (char) IS::LOAD << (char) 1;
    ss << (char) IS::D_PRINT;

    string codeString = ss.str();

    VMCore *core = NEW__VM_CORE;
    Bytecode *bytecodeObj = new Bytecode((unsigned char *) codeString.c_str(), (int) codeString.size());
    cout << "Bytecode LS length: " << bytecodeObj->getLength() << endl;

    core->loadConstantPool(pool);
    core->runBytecode(bytecodeObj);

    EXPECT_STREQ(core->printStream.str().c_str(), "(string) \"Hello World.\"\n");
}

TEST(VM__CORE, locals_rewrite) {

    //store hello and bye to local variable and print it
    stringstream ss;
    ss << (char) IS::PUSH_CONST << (char) helloIndex;
    ss << (char) IS::STORE << (char) 0;
    ss << (char) IS::LOAD << (char) 0;
    ss << (char) IS::D_PRINT;
    ss << (char) IS::PUSH_CONST << (char) byeIndex;
    ss << (char) IS::STORE << (char) 0;
    ss << (char) IS::LOAD << (char) 0;
    ss << (char) IS::D_PRINT;

    string codeString = ss.str();

    VMCore *core = NEW__VM_CORE;
    Bytecode *bytecodeObj = new Bytecode((unsigned char *) codeString.c_str(), (int) codeString.size());
    core->loadConstantPool(pool);
    core->runBytecode(bytecodeObj);

    EXPECT_STREQ(core->printStream.str().c_str(), "(string) \"Hello World.\"\n(string) \"Bye.\"\n");
}

TEST(VM__CORE, locals_exception) {

    //store hello and bye to local variable and print it
    stringstream ss;
    ss << (char) IS::PUSH_CONST << (char)
    helloIndex;
    ss << (char) IS::STORE << (char) 1;
ss << (char) IS::LOAD << (char) 2;
ss << (char)
IS::D_PRINT;

    string codeString = ss.str();

    VMCore *core = NEW__VM_CORE;
    Bytecode *bytecodeObj = new Bytecode((unsigned char *) codeString.c_str(), (int) codeString.size());
    core->loadConstantPool(pool);

    ASSERT_THROW(core->runBytecode(bytecodeObj), VMException::LocalsPool);
}

TEST(VM__CORE, new_obj) {

    //store hello and bye to local variable and print it
    stringstream ss;
    ss << (char) IS::NEW <<
            (char) dogClassIndex;
    ss << (char)
            IS::D_PRINT;
    ss << (char) IS::NEW <<
        (char) catClassIndex;
    ss << (char)
            IS::D_PRINT;

    string codeString = ss.str();

    VMCore *core = NEW__VM_CORE;
    core->setClassPool(classPool);
    Bytecode *bytecodeObj = new Bytecode((unsigned char *) codeString.c_str(), (int) codeString.size());

    core->runBytecode(bytecodeObj);

    EXPECT_STREQ(core->printStream.str().c_str(), "(object reference) \"(obj) dog\"\n(object reference) \"(obj) cat\"\n");
     }

TEST(VM__CORE, getfield_setfield) {

    //set dog name and print it then set 999 and print it
    int dogLocalIndex = 0;
    stringstream ss;
    ss << (char) IS::NEW <<
            (char) dogClassIndex;
    ss << (char) IS::STORE <<
            (char) dogLocalIndex;
    ss << (char) IS::LOAD <<
            (char) dogLocalIndex;
    ss << (char) IS::PUSH_CONST <<
               (char) dogNameIndex;
    ss << (char) IS::SETFIELD <<
            (char) dogStringFieldIndex;
    ss << (char) IS::LOAD <<
            (char) dogLocalIndex;
    ss << (char) IS::GETFIELD <<
            (char) dogStringFieldIndex;
    ss << (char)
            IS::D_PRINT;
    ss << (char) IS::LOAD <<
            (char) dogLocalIndex;
    ss << (char) IS::PUSH_CONST <<
            (char) intIndex;
    ss << (char) IS::SETFIELD <<
            (char) dogStringFieldIndex;
    ss << (char) IS::LOAD <<
            (char) dogLocalIndex;
    ss << (char) IS::GETFIELD <<
            (char) dogStringFieldIndex;
    ss << (char)
            IS::D_PRINT;

    string codeString = ss.str();

    VMCore *core = NEW__VM_CORE;
    core->setClassPool(classPool);
    core->loadConstantPool(pool);
    Bytecode *bytecodeObj = new Bytecode((unsigned char *) codeString.c_str(), (int) codeString.size());

    core->runBytecode(bytecodeObj);

    EXPECT_STREQ(core->printStream.str().c_str(), "(string) \"Lenny the dog\"\n(integer) 999\n");
}

TEST(VM__CORE, invoke) {

    VMCore *core = NEW__VM_CORE;
    core->setClassPool(classPool);
    core->loadConstantPool(pool);
    core->run(dogClassIndex, printHelloIndex);
    EXPECT_STREQ(core->printStream.str().c_str(), "(string) \"Get name function\"\n(string) \"Hello World.\"\n(string) \"Lenny the dog\"\n(string) \"Get name function\"\n(string) \"Lenny the dog\"\n");


}