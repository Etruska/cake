#include "gtest/gtest.h"
#include "VM/Integer.h"
#include "VM/VMObject.h"
#include "VM/Stack.h"

using namespace VM;


TEST(VM__STACK, stack_empty) {

    Stack *stack = new Stack();

    ASSERT_TRUE(stack->isEmpty());

}

TEST(VM__STACK, stack) {

    Stack *stack = new Stack();
    Integer *int1 = new Integer(5);
    Integer *int2 = new Integer(8);
    stack->push(int1);
    stack->push(int2);

    ASSERT_FALSE(stack->isEmpty());
    ASSERT_EQ(stack->size(), 2);

    VMObject *obj1 = stack->pop();
    ASSERT_EQ(obj1->getType(), VMObject::VMInteger);
    ASSERT_EQ(((Integer *) obj1)->getValue(), 8);

    VMObject *obj2 = stack->pop();
    ASSERT_EQ(obj2->getType(), VMObject::VMInteger);
    ASSERT_EQ(((Integer *) obj2)->getValue(), 5);

    ASSERT_TRUE(stack->isEmpty());

}
