Environment
  Class
    PROPERTIES: 
    METHODS: 
       name: method
       args: 
       body: 
           EXPRESSION
             IF:
               Cond:
                 !CALL (PLUS) +
                   !CALL (EQUAL) ==
                     !CALL (SYMBOL) x
                     CONSTANT | (INTEGER) 2
                   CONSTANT | (INTEGER) 1
               Content:
                 !PRINT (PRINT) 
                   EXPRESSION
                     CONSTANT | (STRING) body
             IF:
               Cond:
                 !CALL (EQUAL) ==
                   !CALL (SYMBOL) x
                   CONSTANT | (INTEGER) 2
               Content:
                 !PRINT (PRINT) 
                   EXPRESSION
                     CONSTANT | (STRING) body
               Else branch:
                 !PRINT (PRINT) 
                   EXPRESSION
                     CONSTANT | (STRING) else
             IF:
               Cond:
                 !CALL (EQUAL) ==
                   !CALL (SYMBOL) x
                   CONSTANT | (INTEGER) 2
               Content:
                 !PRINT (PRINT) 
                   EXPRESSION
                     CONSTANT | (STRING) body
               Else if branches:
                 IF:
                   Cond:
                     !CALL (SYMBOL) x
                   Content:
                     !PRINT (PRINT) 
                       EXPRESSION
                         CONSTANT | (STRING) else if
             IF:
               Cond:
                 !CALL (EQUAL) ==
                   !CALL (SYMBOL) x
                   CONSTANT | (INTEGER) 2
               Content:
                 !PRINT (PRINT) 
                   EXPRESSION
                     CONSTANT | (STRING) body
               Else if branches:
                 IF:
                   Cond:
                     !CALL (SYMBOL) x
                   Content:
                     !PRINT (PRINT) 
                       EXPRESSION
                         CONSTANT | (STRING) else if 1
                 IF:
                   Cond:
                     !CALL (SYMBOL) x
                   Content:
                     !PRINT (PRINT) 
                       EXPRESSION
                         CONSTANT | (STRING) else if 2
             IF:
               Cond:
                 !CALL (EQUAL) ==
                   !CALL (SYMBOL) x
                   CONSTANT | (INTEGER) 2
               Content:
                 !PRINT (PRINT) 
                   EXPRESSION
                     CONSTANT | (STRING) body
               Else if branches:
                 IF:
                   Cond:
                     !CALL (EQUAL) ==
                       !CALL (SYMBOL) x
                       CONSTANT | (INTEGER) 3
                   Content:
                     !PRINT (PRINT) 
                       EXPRESSION
                         CONSTANT | (STRING) else if 1
                 IF:
                   Cond:
                     !CALL (EQUAL) ==
                       !CALL (SYMBOL) x
                       CONSTANT | (INTEGER) 4
                   Content:
                     !PRINT (PRINT) 
                       EXPRESSION
                         CONSTANT | (STRING) else if 2
               Else branch:
                 !PRINT (PRINT) 
                   EXPRESSION
                     CONSTANT | (STRING) else
