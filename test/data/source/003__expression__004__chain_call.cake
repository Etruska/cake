* Class
	+ method()
		x.y()
		x.y().z()
		x.y(a()).z(b())
		x.y(a()).z(b(),c())
		x.y(a(),b()).z(c())

		x + x.y()
		x + x.y().z()
		x + x.y(a()).z(b())
		x + x.y(a()).z(b(),c())
		x + x.y(a(),b()).z(c())

