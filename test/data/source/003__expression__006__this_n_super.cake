* Class
	+ method()
		@
		@x
		@.x()
		@.x(a)
		@.x().y()
		@x + 1
		@.x() + 1
		1 + @x
		1 + @.x()
		<< @
		<< @x

		^
		^.x()
		^.x(a)
		^.x().y()