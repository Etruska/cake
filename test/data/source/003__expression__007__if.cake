* Class
	+ method()
		? (x == 2) + 1
			| "body"

		? x == 2
			| "body"
		:
			| "else"

		? x == 2
			| "body"
		:? x
			| "else if"

		? x == 2
			| "body"
		:? x
			| "else if 1"
		:? x
			| "else if 2"

		? x == 2
			| "body"
		:? x == 3
			| "else if 1"
		:? x == 4
			| "else if 2"
		:
			| "else"