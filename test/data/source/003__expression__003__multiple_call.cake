* Class
	+ method()
		x
		x() + x()
		x() + x(a)
		x(1) + x()
		x(a, b) + x()
		x(a + 1, b)
		x(a, b + 1)
		x(a(), b()) + x(a())
		x(a(), b()) + x()
		x(a() + 1, b()) + x()
		x(a(), b() + 1) + x()
