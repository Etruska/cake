#include "gtest/gtest.h"

#include "Helpers/string.h"
#include <string>


TEST(HELPERS__STRING , ltrim) {
    std::string test1 = " foo";
    std::string test2 = "\tfoo";
    std::string test3 = "\nfoo";
    std::string test4 = "\n foo";
    std::string test5 = " foo bar ";

    Cake_Helpers::String::ltrim(test1);
    Cake_Helpers::String::ltrim(test2);
    Cake_Helpers::String::ltrim(test3);
    Cake_Helpers::String::ltrim(test4);
    Cake_Helpers::String::ltrim(test5);

    ASSERT_STREQ("foo", test1.c_str());
    ASSERT_STREQ("foo", test2.c_str());
    ASSERT_STREQ("foo", test3.c_str());
    ASSERT_STREQ("foo", test4.c_str());
    ASSERT_STREQ("foo bar ", test5.c_str());
}

TEST(HELPERS__STRING , rtrim) {
    std::string test1 = "foo ";
    std::string test2 = "foo\t";
    std::string test3 = "foo\n";
    std::string test4 = "foo \n";
    std::string test5 = " foo bar ";

    Cake_Helpers::String::rtrim(test1);
    Cake_Helpers::String::rtrim(test2);
    Cake_Helpers::String::rtrim(test3);
    Cake_Helpers::String::rtrim(test4);
    Cake_Helpers::String::rtrim(test5);

    ASSERT_STREQ("foo", test1.c_str());
    ASSERT_STREQ("foo", test2.c_str());
    ASSERT_STREQ("foo", test3.c_str());
    ASSERT_STREQ("foo", test4.c_str());
    ASSERT_STREQ(" foo bar", test5.c_str());
}

TEST(HELPERS__STRING , trim) {
    std::string test1 = " foo ";
    std::string test2 = "\tfoo\t";
    std::string test3 = "\nfoo\n";
    std::string test4 = "\n foo \n";
    std::string test5 = " foo bar ";

    Cake_Helpers::String::trim(test1);
    Cake_Helpers::String::trim(test2);
    Cake_Helpers::String::trim(test3);
    Cake_Helpers::String::trim(test4);
    Cake_Helpers::String::trim(test5);

    ASSERT_STREQ("foo", test1.c_str());
    ASSERT_STREQ("foo", test2.c_str());
    ASSERT_STREQ("foo", test3.c_str());
    ASSERT_STREQ("foo", test4.c_str());
    ASSERT_STREQ("foo bar", test5.c_str());
}


TEST(HELPERS__STRING , isPrefixOfString) {
    std::string input = "foobar";
    std::string test1 = "foo";
    std::string test2 = "bar";
    std::string test3 = "foobar";

    ASSERT_TRUE(Cake_Helpers::String::isPrefixOfString(input, test1));
    ASSERT_FALSE(Cake_Helpers::String::isPrefixOfString(input, test2));
    ASSERT_TRUE(Cake_Helpers::String::isPrefixOfString(input, test3));
}

TEST(HELPERS__STRING , matchAndRemovePrefix) {
    std::string input1 = "foobar";
    std::string input2 = "foobar";
    std::string input3 = "foobar";
    std::string test1 = "foo";
    std::string test2 = "bar";
    std::string test3 = "foobar";


    ASSERT_TRUE(Cake_Helpers::String::matchAndRemovePrefix(input1, test1));
    ASSERT_FALSE(Cake_Helpers::String::matchAndRemovePrefix(input2, test2));
    ASSERT_TRUE(Cake_Helpers::String::matchAndRemovePrefix(input3, test3));

    ASSERT_STREQ("bar", input1.c_str());
    ASSERT_STREQ("foobar", input2.c_str());
    ASSERT_STREQ("", input3.c_str());
}

TEST(HELPERS__STRING , splitWithWhitespaces) {
    std::string test1 = "foo bar";
    std::string test2 = "lorem ipsum sit dolor\namet";

    std::vector<std::string> tokens1 = Cake_Helpers::String::splitWithWhitespaces(test1);
    std::vector<std::string> tokens2 = Cake_Helpers::String::splitWithWhitespaces(test2);

    ASSERT_EQ(2, tokens1.size());

    ASSERT_STREQ("foo", tokens1[0].c_str());
    ASSERT_STREQ("bar", tokens1[1].c_str());


    ASSERT_EQ(5, tokens2.size());

    ASSERT_STREQ("lorem", tokens2[0].c_str());
    ASSERT_STREQ("dolor", tokens2[3].c_str());
    ASSERT_STREQ("amet", tokens2[4].c_str());
}

TEST(HELPERS__STRING , removeWhitespaces) {
    std::string test = " lorem \t   ipsum sit    dolor\namet";

    Cake_Helpers::String::removeWhitespaces(test);

    ASSERT_STREQ( "loremipsumsitdoloramet", test.c_str());
}

TEST(HELPERS__STRING , extractDelimitedPrefix) {
    std::string test1 = "foo(bar[])";
    std::string test2 = "foo(bar[])";
    bool matched = false;

    ASSERT_EQ("", Cake_Helpers::String::extactDelimitedPrefix(test1, "{", matched));
    ASSERT_FALSE(matched);

    ASSERT_EQ("foo", Cake_Helpers::String::extactDelimitedPrefix(test1, "(", matched));
    ASSERT_TRUE(matched);

    ASSERT_EQ("foo(bar", Cake_Helpers::String::extactDelimitedPrefix(test2, "[", matched));
    ASSERT_TRUE(matched);
}