#include "gtest/gtest.h"

#include "Parser/parser.h"
#include "Parser/ast/environment.h"
#include "Parser/ast/expression.h"
#include "Parser/ast/signal.h"
#include <string>
#include <vector>
#include <Parser/ast/signal.h>

using namespace Cake_Parser;
using namespace std;

//#define _TESTS__SHOW_SOURCE__AND_AST

TEST(PARSING_CONTENT, indentation) {


    std::string testStr0 = "xx";
    std::string testStr1 = "\txx";
    std::string testStr2 = "\t\txxy";
    std::string testStrEmpty1 = "\t";
    std::string testStrEmpty2 = "\t\t";

    IndentedLine iLine0(testStr0);
    IndentedLine iLine1(testStr1);
    IndentedLine iLine2(testStr2);
    IndentedLine iLineEmpty1(testStrEmpty1);
    IndentedLine iLineEmpty2(testStrEmpty2);

    // Test indentation
    ASSERT_EQ(0, iLine0.indetation);
    ASSERT_EQ(1, iLine1.indetation);
    ASSERT_EQ(2, iLine2.indetation);
    ASSERT_EQ(EMPTY_LINE, iLineEmpty1.indetation);
    ASSERT_EQ(EMPTY_LINE, iLineEmpty2.indetation);

    // Test content
    ASSERT_STREQ("xx", iLine0.content.c_str());
    ASSERT_STREQ("xx", iLine1.content.c_str());
    ASSERT_STREQ("xxy", iLine2.content.c_str());
    ASSERT_STREQ("", iLineEmpty1.content.c_str());
    ASSERT_STREQ("", iLineEmpty2.content.c_str());
}

Cake_Parser::Class_Ast *getClass(Cake_Parser::Environment_Ast *root) {
    return root->classes[0];
}

Cake_Parser::Method_Ast *getMethod(Cake_Parser::Environment_Ast *root) {
    return getClass(root)->methods[0];
}

Cake_Parser::Expression_AstNode *getMethodBody(Cake_Parser::Environment_Ast *root) {
    return getMethod(root)->body;
}


Environment_Ast *getEnvFromRawSource(std::string content) {
    #ifdef _TESTS__SHOW_SOURCE__AND_AST
        std::cout << ">> Parsing:\n" << content << endl;
    #endif
    Cake_Parser::Parser parser = Cake_Parser::Parser();
    Environment_Ast *environment = Cake_Parser::Environment_Ast::parse(parser.parseString(content));
    #ifdef _TESTS__SHOW_SOURCE__AND_AST
        environment->dump();
    #endif
    EXPECT_EXIT(Cake_Parser::Environment_Ast::validate(parser.parseString(content)), ::testing::ExitedWithCode(0), "");

    return environment;
}

Environment_Ast *getEnvFromSource(std::string content) {
    #ifdef _TESTS__SHOW_SOURCE__AND_AST
        std::cout << ">> Parsing:\n" << content << endl;
    #endif
    Cake_Parser::Parser parser = Cake_Parser::Parser();
    Environment_Ast *environment = Cake_Parser::Environment_Ast::parse(parser.parseString(content));
    #ifdef _TESTS__SHOW_SOURCE__AND_AST
        environment->dump();
    #endif
    EXPECT_EXIT(Cake_Parser::Environment_Ast::validate(parser.parseString(content)), ::testing::ExitedWithCode(0), "");

    return environment;
}

#define TEST_RAW_ENVIRONMENT_HELPER(__SOURCE__) environment = getEnvFromSource(lastParsedString = (__SOURCE__));
#define TEST_ENVIRONMENT_HELPER(__SOURCE__) environment = getEnvFromSource(lastParsedString = (headers + __SOURCE__));
#define TEST_MESSAGE_HELPER(__MESSAGE__) << parsingMessage(__MESSAGE__, lastParsedString).c_str()

#define TO_SIGNAL(__EXPRESSION__) dynamic_cast<Signal_Ast*>(__EXPRESSION__)
#define LOAD_SIGNAL(__EXPRESSION__) signal = dynamic_cast<Signal_Ast*>(__EXPRESSION__);
#define LOAD_FIRST_SIGNAL() \
    body = getMethodBody(environment);\
    signal = dynamic_cast<Signal_Ast*>(getMethodBody(environment)->content[0]);


std::string parsingMessage(std::string message, std::string lastParsedString) {
    return std::string("\n !! ERRROR !!\n") + message + std::string("\n------------------------------\n") +
           lastParsedString + std::string("\n------------------------------");
}

TEST(AST, parsing_signal) {

    Cake_Parser::Parser parser = Cake_Parser::Parser();

    std::string content = "";
    std::string lastParsedString = "";

    std::string headers = "* C\n\t+ m()\n\t\t";

    //////////////////////////////////////////
    /////// CHECK STRUCTURE
    //////////////////////////////////////////

    Cake_Parser::Class_Ast *c_class;
    Cake_Parser::Method_Ast *method;
    Cake_Parser::Expression_AstNode *body;
    Cake_Parser::Expression_AstNode *expression;
    Cake_Parser::Signal_Ast *signal;
    Cake_Parser::Environment_Ast *environment;

    //////////////////
    /// Signatures
    //////////////////
    TEST_ENVIRONMENT_HELPER("#");
    // Class
    c_class = getClass(environment);

    ASSERT_STREQ("C", c_class->name.c_str()) TEST_MESSAGE_HELPER("Class name should be 'C'");
    ASSERT_EQ(1, c_class->methods.size()) TEST_MESSAGE_HELPER("Class should have one method");

    // Method (No arguments)
    method = getMethod(environment);
    ASSERT_STREQ("m", method->name.c_str()) TEST_MESSAGE_HELPER("Method name should be 'm'");
    ASSERT_EQ(0, method->arguments->size()) TEST_MESSAGE_HELPER("Method should not have any arguments");

    // Method - one argument
    TEST_RAW_ENVIRONMENT_HELPER("* C\n\t+ m(x)\n\t\t")
    method = getMethod(environment);
    signal = TO_SIGNAL(method->arguments->at(0));
    ASSERT_EQ(1, method->arguments->size()) TEST_MESSAGE_HELPER("Method should not have any arguments");
    ASSERT_STREQ("x", signal->name.c_str()) TEST_MESSAGE_HELPER("Parameter should be named 'x'");
    // Method - two arguments

    //////////////////
    /// Expressions
    //////////////////

    //// Empty method body
    // #
    body = getMethodBody(environment);
    ASSERT_EQ(0, body->content.size()) TEST_MESSAGE_HELPER("Method body should be empty");

    //// Symbol
    // x
    TEST_ENVIRONMENT_HELPER("x");
    LOAD_FIRST_SIGNAL();
    ASSERT_EQ(SIGNAL__CALL, signal->signalType) TEST_MESSAGE_HELPER("Expression should be signal call");
    ASSERT_STREQ("x", signal->name.c_str()) TEST_MESSAGE_HELPER("Expression should be signal call");

    //// Method call
    // x()
    TEST_ENVIRONMENT_HELPER("x()");
    LOAD_FIRST_SIGNAL();
    ASSERT_EQ(SIGNAL__CALL, signal->signalType) TEST_MESSAGE_HELPER("Expression should be signal call");
    LOAD_SIGNAL(signal->parameters->at(0));
    ASSERT_STREQ("x", signal->name.c_str()) TEST_MESSAGE_HELPER("Method name should be 'x'");

    // x(a)
    TEST_ENVIRONMENT_HELPER("x(a)");
    LOAD_FIRST_SIGNAL();
    ASSERT_EQ(SIGNAL__CALL, signal->signalType) TEST_MESSAGE_HELPER("Expression should be signal call");
    LOAD_SIGNAL(signal->parameters->at(0));
    ASSERT_STREQ("x", signal->name.c_str()) TEST_MESSAGE_HELPER("Method name should be 'x'");
    LOAD_FIRST_SIGNAL();
    LOAD_SIGNAL(signal->parameters->at(1));
    ASSERT_STREQ("a", signal->name.c_str()) TEST_MESSAGE_HELPER("Method parameter should be 'a'");

    // x(a())
    TEST_ENVIRONMENT_HELPER("x(a())");
    LOAD_FIRST_SIGNAL();
    ASSERT_EQ(SIGNAL__CALL, signal->signalType) TEST_MESSAGE_HELPER("Expression should be signal call");
    LOAD_SIGNAL(signal->parameters->at(0));
    ASSERT_STREQ("x", signal->name.c_str()) TEST_MESSAGE_HELPER("Method name should be 'x'");
    LOAD_FIRST_SIGNAL();
    LOAD_SIGNAL(signal->parameters->at(1));
    ASSERT_EQ(SIGNAL__CALL, signal->signalType) TEST_MESSAGE_HELPER("Expression should be signal call");
    LOAD_SIGNAL(signal->parameters->at(0));
    ASSERT_STREQ("a", signal->name.c_str()) TEST_MESSAGE_HELPER("Method name should be 'a'");

    // x(a,b)
    TEST_ENVIRONMENT_HELPER("x(a,b)");
    LOAD_FIRST_SIGNAL();
    ASSERT_EQ(SIGNAL__CALL, signal->signalType) TEST_MESSAGE_HELPER("Expression should be signal call");
    LOAD_SIGNAL(signal->parameters->at(0));
    ASSERT_STREQ("x", signal->name.c_str()) TEST_MESSAGE_HELPER("Method name should be 'x'");
    LOAD_FIRST_SIGNAL();
    LOAD_SIGNAL(signal->parameters->at(1));
    ASSERT_STREQ("a", signal->name.c_str()) TEST_MESSAGE_HELPER("Method name should be 'a'");
    LOAD_FIRST_SIGNAL();
    LOAD_SIGNAL(signal->parameters->at(2));
    ASSERT_STREQ("b", signal->name.c_str()) TEST_MESSAGE_HELPER("Method second parameter should be 'b'");

    // x(a(),b())
    TEST_ENVIRONMENT_HELPER("x(a(), b())");
    LOAD_FIRST_SIGNAL();
    ASSERT_EQ(SIGNAL__CALL, signal->signalType) TEST_MESSAGE_HELPER("Expression should be signal call");
    LOAD_SIGNAL(signal->parameters->at(0));
    ASSERT_STREQ("x", signal->name.c_str()) TEST_MESSAGE_HELPER("Method name should be 'x'");
    LOAD_FIRST_SIGNAL();
    LOAD_SIGNAL(signal->parameters->at(1));
    ASSERT_EQ(SIGNAL__CALL, signal->signalType) TEST_MESSAGE_HELPER("Expression should be signal call");
    LOAD_SIGNAL(signal->parameters->at(0));
    ASSERT_STREQ("a", signal->name.c_str()) TEST_MESSAGE_HELPER("Method name should be 'a'");
    LOAD_FIRST_SIGNAL();
    LOAD_SIGNAL(signal->parameters->at(2));
    ASSERT_EQ(SIGNAL__CALL, signal->signalType) TEST_MESSAGE_HELPER("Expression should be signal call");
    LOAD_SIGNAL(signal->parameters->at(0));
    ASSERT_STREQ("b", signal->name.c_str()) TEST_MESSAGE_HELPER("Method name should be 'b'");

    // x(a(),b())
    TEST_ENVIRONMENT_HELPER("x(a + b)");
    LOAD_FIRST_SIGNAL();
    ASSERT_EQ(SIGNAL__CALL, signal->signalType) TEST_MESSAGE_HELPER("Expression should be signal call");
    LOAD_SIGNAL(signal->parameters->at(0));
    ASSERT_STREQ("x", signal->name.c_str()) TEST_MESSAGE_HELPER("Method name should be 'x'");
    LOAD_FIRST_SIGNAL();
    LOAD_SIGNAL(signal->parameters->at(1));
    ASSERT_EQ(SIGNAL__CALL, signal->signalType) TEST_MESSAGE_HELPER("Expression should be signal call");
    ASSERT_STREQ("+", signal->name.c_str()) TEST_MESSAGE_HELPER("Operation name should be '+'");
    LOAD_SIGNAL(signal->parameters->at(0));
    ASSERT_STREQ("a", signal->name.c_str()) TEST_MESSAGE_HELPER("Method name should be 'a'");
    LOAD_FIRST_SIGNAL();
    LOAD_SIGNAL(signal->parameters->at(1));
    ASSERT_EQ(SIGNAL__CALL, signal->signalType) TEST_MESSAGE_HELPER("Expression should be signal call");
    LOAD_SIGNAL(signal->parameters->at(1));
    ASSERT_STREQ("b", signal->name.c_str()) TEST_MESSAGE_HELPER("Method name should be 'b'");

    //// Chaining call
    // x.y()
    TEST_ENVIRONMENT_HELPER("x.y()");
    LOAD_FIRST_SIGNAL();
    ASSERT_EQ(SIGNAL__CALL, signal->signalType) TEST_MESSAGE_HELPER("Expression should be signal call");
    LOAD_SIGNAL(signal->parameters->at(0));
    ASSERT_EQ(SIGNAL__CALL, signal->signalType) TEST_MESSAGE_HELPER("Expression should be signal call");
    ASSERT_EQ(SPEC_SYM__DOT, signal->type) TEST_MESSAGE_HELPER("Expression should be DOT");
    LOAD_SIGNAL(signal->parameters->at(0));
    ASSERT_STREQ("x", signal->name.c_str()) TEST_MESSAGE_HELPER("Called object should be 'x'");
    LOAD_FIRST_SIGNAL();
    LOAD_SIGNAL(signal->parameters->at(1));
    ASSERT_STREQ("y", signal->name.c_str()) TEST_MESSAGE_HELPER("Called method should be 'y'");

    // x.y(a)
    TEST_ENVIRONMENT_HELPER("x.y(a)");
    LOAD_FIRST_SIGNAL();
    ASSERT_EQ(SIGNAL__CALL, signal->signalType) TEST_MESSAGE_HELPER("Expression should be signal call");
    LOAD_SIGNAL(signal->parameters->at(0));
    ASSERT_EQ(SIGNAL__CALL, signal->signalType) TEST_MESSAGE_HELPER("Expression should be signal call");
    ASSERT_EQ(SPEC_SYM__DOT, signal->type) TEST_MESSAGE_HELPER("Expression should be DOT");
    LOAD_SIGNAL(signal->parameters->at(0));
    ASSERT_STREQ("x", signal->name.c_str()) TEST_MESSAGE_HELPER("Called object should be 'x'");
    LOAD_FIRST_SIGNAL();
    LOAD_SIGNAL(signal->parameters->at(1));
    ASSERT_STREQ("y", signal->name.c_str()) TEST_MESSAGE_HELPER("Called method should be 'y'");
    LOAD_FIRST_SIGNAL();
    LOAD_SIGNAL(signal->parameters->at(2));
    ASSERT_STREQ("a", signal->name.c_str()) TEST_MESSAGE_HELPER("Method parameter should be 'a'");

    // x.y().z()
    TEST_ENVIRONMENT_HELPER("x.y(a)");
    LOAD_FIRST_SIGNAL();
    ASSERT_EQ(SIGNAL__CALL, signal->signalType) TEST_MESSAGE_HELPER("Expression should be signal call");
    LOAD_SIGNAL(signal->parameters->at(0));
    ASSERT_EQ(SIGNAL__CALL, signal->signalType) TEST_MESSAGE_HELPER("Expression should be signal call");
    ASSERT_EQ(SPEC_SYM__DOT, signal->type) TEST_MESSAGE_HELPER("Expression should be DOT");
    LOAD_SIGNAL(signal->parameters->at(0));
    ASSERT_STREQ("x", signal->name.c_str()) TEST_MESSAGE_HELPER("Called object should be 'x'");
    LOAD_FIRST_SIGNAL();
    LOAD_SIGNAL(signal->parameters->at(1));
    ASSERT_STREQ("y", signal->name.c_str()) TEST_MESSAGE_HELPER("Called method should be 'y'");
    LOAD_FIRST_SIGNAL();
    LOAD_SIGNAL(signal->parameters->at(2));
    ASSERT_STREQ("a", signal->name.c_str()) TEST_MESSAGE_HELPER("Method parameter should be 'a'");

    // x.y().z()
}


#ifndef TEST__GENERATOR_HELPER
#define TEST__GENERATOR_HELPER

#define TEST_DATA_PATH "/data"

#include <fstream>
#include <streambuf>
#include <sstream>
#include <string>

std::string getTestDataDir() {
    string path(__FILE__);
    size_t found;
    found = path.find_last_of("/");

    return path.substr(0, found);
}

string loadAstTestFile(string name) {
    string fullpath = getTestDataDir() + string(TEST_DATA_PATH) + string("/ast/") + name + string(".txt");
    //cout << "Loading:" << fullpath << endl;
    ifstream t(fullpath);
    string str((istreambuf_iterator<char>(t)),
               istreambuf_iterator<char>());

    return str;
}

string loadTestFile(string name) {
    string fullpath = getTestDataDir() + string(TEST_DATA_PATH) + string("/source/") + name;
    //cout << "Loading:" << fullpath << endl;
    ifstream t(fullpath);
    string str((istreambuf_iterator<char>(t)),
               istreambuf_iterator<char>());

    return str;
}


#include <sys/types.h>
#include <dirent.h>
#include <errno.h>
#include <vector>
#include <string>
#include <iostream>

int getdir(string dir, vector<string> &files) {
    DIR *dp;
    struct dirent *dirp;
    if ((dp = opendir(dir.c_str())) == NULL) {
        //cout << "Error(" << errno << ") opening " << dir << endl;
        return errno;
    }

    while ((dirp = readdir(dp)) != NULL) {
        if (!string(dirp->d_name).compare(".") || !string(dirp->d_name).compare("..") ||
            !string(dirp->d_name).compare("test.cake")) {
            continue;
        }
        files.push_back(string(dirp->d_name));
    }
    closedir(dp);
    return 0;
}

vector<string> getTestFileNames() {
    string fullpath = getTestDataDir() + string(TEST_DATA_PATH) + string("/source/");
    //cout << "Loading:" << fullpath << endl;
    vector<string> files = vector<string>();

    getdir(fullpath, files);

    return files;
}

#endif

TEST (AST, parsing_content) {

    Cake_Parser::Parser parser = Cake_Parser::Parser();

    std::string content = "";
    std::string lastParsedString = "";

    Cake_Parser::Class_Ast *c_class;
    Cake_Parser::Method_Ast *method;
    Cake_Parser::Expression_AstNode *body;
    Cake_Parser::Expression_AstNode *expression;
    Cake_Parser::Signal_Ast *signal;
    Cake_Parser::Environment_Ast *environment;


    for (string testFilename : getTestFileNames()) {
        TEST_RAW_ENVIRONMENT_HELPER(loadTestFile(testFilename));
        loadAstTestFile(testFilename);
        ASSERT_STREQ(loadAstTestFile(testFilename).c_str(), environment->dumpToString().c_str()) << (
                string("Output is not equal to expected ast in: ") + testFilename);
    }
}