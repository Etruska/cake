#include <iostream>


#include <string>
using namespace std;

#include "Parser/parser.h"
#include <fstream>
#include <streambuf>

#include "Parser/node.h"
#include "Parser/ast/environment.h"
#include "Compiler/compiler.h"
#include "VM/Bytecode.h"
#include "Utils/Parameters.h"
#include "VM/VMCore.h"
#include "Heap/HeapManager.h"

#define TEST_DATA_PATH "/test/data"


#ifndef TEST__GENERATOR_HELPER
#define TEST__GENERATOR_HELPER

string getTestDataDir() {
    string path(__FILE__);
    size_t found;
    found = path.find_last_of("/");

    return path.substr(0, found);
}

void saveAsTestFile(string name, string content) {
    string fullpath = getTestDataDir() + string(TEST_DATA_PATH) + string("/ast/") + name + string(".txt");
    cout << "Saving:" << fullpath << endl;
    std::ofstream out(fullpath);
    out << content;
    out.close();
}


void saveAsBytecodeTestFile(string name, string content) {
    string fullpath = getTestDataDir() + string(TEST_DATA_PATH) + string("/bytecode/") + name + string(".txt");
    cout << "Saving:" << fullpath << endl;
    std::ofstream out(fullpath);
    out << content;
    out.close();
}

string loadTestFile(string name) {
    string fullpath = getTestDataDir() + string(TEST_DATA_PATH) + string("/source/") + name;
    cout << "Loading:" << fullpath << endl;
    ifstream t(fullpath);
    string str((istreambuf_iterator<char>(t)),
               istreambuf_iterator<char>());

    return str;
}

#include <sys/types.h>
#include <dirent.h>
#include <errno.h>
#include <vector>
#include <string>
#include <iostream>

int getdir(string dir, vector<string> &files) {
    DIR *dp;
    struct dirent *dirp;
    if ((dp = opendir(dir.c_str())) == NULL) {
        //cout << "Error(" << errno << ") opening " << dir << endl;
        return errno;
    }

    while ((dirp = readdir(dp)) != NULL) {
        if (!string(dirp->d_name).compare(".")
            || !string(dirp->d_name).compare("..")
            || !string(dirp->d_name).compare("test.cake")
                ) {
            continue;
        }
        files.push_back(string(dirp->d_name));
    }
    closedir(dp);
    return 0;
}

#endif


void regenerateTests() {
    string fullpath = getTestDataDir() + string(TEST_DATA_PATH) + string("/source/");
    vector<string> files = vector<string>();

    getdir(fullpath, files);

    for (string sourceFileName : files) {
        cout << "regenerating:" << sourceFileName << endl;
        Cake_Parser::Parser parser = Cake_Parser::Parser();
        Cake_Parser::Node *tokenTree = parser.parseString(loadTestFile(sourceFileName));

        Cake_Parser::Environment_Ast *env = Cake_Parser::Environment_Ast::parse(tokenTree);

        //cout << env->dumpToString();

        saveAsTestFile(sourceFileName, env->dumpToString());
    }

    exit(0);
}

void dev_helpers() {
    ////////////////////////////////////////
    //// CREATING TEST FILES
    ////////////////////////////////////////////////////////////////////////////////
    bool SAVE_AS_TEST = false;
    ////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////
    //// !!! IMPORTANT USE WITH CAUSION AND COMMIT BEFORE USING !!!
    // all changes will then appear in commit
    // regenerateTests();
    ////////////////////////////////////////////////////

    //
    string testName = "test.cake";

    Cake_Parser::Parser parser = Cake_Parser::Parser();
    Cake_Parser::Node *tokenTree = parser.parseString(loadTestFile(testName));

    tokenTree->dumpWholeTree(tokenTree);

    cout << "----------------------------------------" << endl;


    Cake_Parser::Environment_Ast *env = Cake_Parser::Environment_Ast::parse(tokenTree);

    cout << env->dumpToString();

    cout << "----------------------------------------" << endl;

    Cake_Compiler::Compiler *compiler = new Cake_Compiler::Compiler();

    env->compile(compiler);

    env->dumpCompiled(compiler);

    if (SAVE_AS_TEST) {
        saveAsTestFile(testName, env->dumpToString());
    }


    exit(0);
}

void processParameters(Parameters& parameters)
{
    // Defaults
    parameters.setParameterDefaultValue("method", "main");
    parameters.setParameterDefaultValue("class", "Base");
    parameters.setParameterDefaultValue("file", "/Users/draczris/FIT/2014-ZS/RUN/cake_copy/test.cake");

}


#include "VM/NativeClasses/File.h"
#include "VM/NativeClasses/IO.h"
#include "VM/NativeClasses/String.h"

int main(int argc, char* argv[]) {

    //////////////////////////////////////////
    /////// cake --file="/path/to/source/file.cake" --class="MyClass" --method="myMethod"
    //////////////////////////////////////////

    // parameters
    Parameters parameters = Parameters(argc, argv);
    processParameters(parameters);

    //parameters.dump();

    // dev_helpers();

    // Load file
    if (FILE *file = fopen(parameters.getStringParameter("file").c_str(), "r")) {
        fclose(file);
    } else {
        cerr << "Source file \'"<< parameters.getStringParameter("file") <<"\' does not exist!" << endl;
        exit(2);
    }

    ifstream t(parameters.getStringParameter("file"));
    string source((istreambuf_iterator<char>(t)),
                  istreambuf_iterator<char>());


    // Parse file
    Cake_Parser::Parser parser = Cake_Parser::Parser();
    Cake_Parser::Node *tokenTree = parser.parseString(source);

    //tokenTree->dumpWholeTree(tokenTree);

    cout << "----------------------------------------" << endl;


    Cake_Parser::Environment_Ast *env = Cake_Parser::Environment_Ast::parse(tokenTree);

    //cout << env->dumpToString();

    cout << "----------------------------------------" << endl;

    // Compile file
    Cake_Compiler::Compiler *compiler = new Cake_Compiler::Compiler();

    // Compile native classes
    compiler->classPool->add(new ClassFile(&(compiler->methodsMap)));
    compiler->classPool->add(new ClassIO(&(compiler->methodsMap)));
    compiler->classPool->add(new ClassString(&(compiler->methodsMap)));

    env->compile(compiler);

    //env->dumpCompiled(compiler);
    //compiler->methodsMap.dump();

    int argId = NO_ARG;
    // add arg
    if (parameters.hasParameter("arg")) {
        argId = compiler->constantPool->add(new VMString(parameters.getStringParameter("arg")));
    }

    VM::VMCore* core = new VM::VMCore(new Heap::HeapManager(1000000000));

    core->setClassPool(compiler->classPool);
    core->loadConstantPool(compiler->constantPool);

    //compiler->classPool->dump();

    // locate init method
    int initClassId = compiler->classPool->getIndexOf(new VMClass(
            parameters.getStringParameter("class"),
            NULL
    ));

    int initMethodId = compiler->methodsMap.getId(parameters.getStringParameter("method"));

    cout << "------------------------------------------" << endl;
    cout.flush();
    cerr.flush();

    core->run(initClassId, initMethodId, argId);

    return 0;
}
