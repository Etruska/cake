* Item
	@content
	@next
	@prev

	+ init()
		@next = 0
		@prev = 0

	+ setPrev(prev)
		@prev = prev

	+ setNext(next)
		@next = next

	+ getNext()
		<< @next

	+ getPrev()
		<< @prev

	+ set(content)
		@content = content

	+ get()
		<< @content

* Array
	@firstItem
	@lastItem
	@size

	+ init()
		@firstItem = 0
		@lastItem = 0
		@size = 0

	+ add(item)
		arrayItem = new(Item)
		arrayItem.init()

		arrayItem.set(item)

		? @firstItem
			arrayItem.setPrev(@lastItem)
			@lastItem.setNext(arrayItem)
		:
			@firstItem = arrayItem

		@lastItem = arrayItem
		@size = @size + 1

	+ get(id)
		iterator = @firstItem
		counter = id

		% iterator
			? counter > 0
				counter = counter - 1
				result = iterator.get()
				iterator = iterator.getNext()

			:
				<< iterator.get()


		<< 0

	+ dump()
		io = new(IO)

		io.printLine("Array:")
		io.printLine(@size)

		iterator = @firstItem
		% iterator
			iterator.get().dump()
			iterator = iterator.getNext()

	+ debugDump()
		io = new(IO)

		io.print("Array (")
		io.print(@size)
		io.printLine("):")

		iterator = @firstItem
		counter = 0
		% iterator
			io.print("  ")
			io.print(counter)
			io.print(": ")
			io.printLine(iterator.get())
			counter = counter + 1
			iterator = iterator.getNext()

	+ removeLast()

		? @lastItem
			tmp = @lastItem
			@lastItem = tmp.getPrev()

			? @lastItem
				@lastItem.setNext(0)
			:
				@firstItem = 0

			@size = @size - 1

	+ size()
		<< @size

* StringToArrayConverter
	+ convertPerLine(content)
		array = new(Array)
		array.init()

		sm = new(StringModifier)
		linesCount = sm.getLinesCount(content)
		counter = 0

		% counter < linesCount
			array.add(sm.getLine(content, counter))
			counter = counter + 1
		<< array

	+ convertPerCharacter(content, delimiter)
		array = new(Array)
		array.init()

		sm = new(StringModifier)
		partsCount = sm.getExplodeSize(content, delimiter)
		counter = 0

		% counter < partsCount
			array.add(sm.getExplodePart(content, delimiter, counter))
			counter = counter + 1
		<< array


* Literal
	@varId
	@negated

	+ init(varId, negated)
		@varId = varId
		@negated = negated

	+ negated?()
		<< @negated

	+ getVarId()
		<< @varId

	+ dump()
		io = new(IO)
		io.print("        Id:")
		io.print(@varId)
		io.print(" negated?: ")
		? @negated
			io.print("true")
		:
			io.print("false")

		io.lineBreak()

	+ true?(configuration)
		confVal = configuration.get(@varId)

		? @negated
			<< !confVal
		:
			<< confVal

* Clause
	@literals

	+ init()
		@literals = new(Array)
		@literals.init()

	+ add(literal)
		@literals.add(literal)

	+ dump()
		io = new(IO)
		io.printLine("    Clause:")
		@literals.dump()

	+ parse(line)
		converter = new(StringToArrayConverter)
		sm = new(StringModifier)

		res = converter.convertPerCharacter(line, ",")

		negations = res.get(res.size() - 1)

		counter = 0
		% counter < (res.size() - 1)
			it = new(Literal)
			? sm.getCharOnPosition(negations, counter) == "1"
				isNegated = 1
			:
				isNegated = 0

			it.init(sm.toInteger(res.get(counter)), isNegated)

			@literals.add(it)
			counter = counter + 1

	+ maxVarId()
		maxVarId = 0
		counter = 0
		% counter < @literals.size()
			? maxVarId < @literals.get(counter).getVarId()
				maxVarId = @literals.get(counter).getVarId()
			counter = counter + 1

		<< maxVarId

	+ true?(configuration)
		counter = 0
		% counter < @literals.size()
			? @literals.get(counter).true?(configuration)
				<< 1
			counter = counter + 1

		<< 0


* Instance
	@clauses

	+ init()
		@clauses = new(Array)
		@clauses.init()

	+ parseFile(filePath)
		converter = new(StringToArrayConverter)
		fileReader = new(File)
		res = converter.convertPerLine(fileReader.read(filePath))
		counter = 0

		% counter < res.size()
			clause = new(Clause)
			clause.init()
			clause.parse(res.get(counter))

			@clauses.add(clause)
			counter = counter + 1

	+ true?(configuration)
		counter = 0
		% counter < @clauses.size()
			? !@clauses.get(counter).true?(configuration)
				<< 0
			counter = counter + 1

		<< 1

	+ varCount()
		counter = 0
		maxVarId = 0
		% counter < @clauses.size()
			? maxVarId < @clauses.get(counter).maxVarId()
				maxVarId = @clauses.get(counter).maxVarId()
			counter = counter + 1

		<< maxVarId + 1

	+ dump()
		io = new(IO)
		io.printLine("Instance:")
		@clauses.dump()

* Base
	+ main()
		io = new(IO)

		filePath = "/Users/draczris/FIT/2014-ZS/RUN/cake_copy/test.dat"
		instance = new(Instance)
		instance.init()
		instance.parseFile(filePath)
		instance.dump()

		configuration = new(Array)
		configuration.init()

		? @.generateConfiguration(instance, configuration, instance.varCount())
			io.printLine("Solution:")
			@.printResult(instance, configuration)
		:
			io.printLine("Instance is not solvable")

	+ printResult(instance, configuration)
		counter = 0
		io = new(IO)

		% counter < configuration.size()
			io.print("varId: ")
			io.print(counter)
			io.print(" = ")

			? configuration.get(counter)
				io.printLine("TRUE")
			:
				io.printLine("FALSE")
			counter = counter + 1

	+ generateConfiguration(instance, configuration, size)
		? !size
			<< instance.true?(configuration)
		:
			configuration.add(1)
			? @.generateConfiguration(instance, configuration, size - 1)
				<< 1

			configuration.removeLast()

			configuration.add(0)
			? @.generateConfiguration(instance, configuration, size - 1)
				<< 1

			configuration.removeLast()
		<< 0

