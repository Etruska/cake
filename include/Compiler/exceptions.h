

#include <exception>
using namespace std;

namespace Cake_Compiler {

    #define COMPILER__EXCEPTIONS__HELPER(__NAME__,__CONTENT__)\
    class __NAME__: public exception {\
        virtual const char* what() const throw() { return __CONTENT__; }\
    };
    COMPILER__EXCEPTIONS__HELPER(InvalidAssignTarget, "Invalid left operand for operation assign");
}

