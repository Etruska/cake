#ifndef PARSER_COMPILER__H
#define PARSER_COMPILER__H

#include <iostream>
#include <sstream>
#include <string>

#include "Utils/Pool.h"
#include "VM/ClassPool.h"
#include "VM/VMClass.h"
#include "Parser/ast/class.h"
#include "Parser/ast/method.h"

using namespace std;
using namespace VM;
using namespace Cake_Parser;

namespace Cake_Parser {
    class Class_Ast;
}

namespace Cake_Compiler {
    class Compiler {
    public:
        ConstantPool *constantPool;
        ClassPool<VMClass*> *classPool;
        Pool<string> methodsMap;

        Method_Ast* currentMethod;
        Class_Ast* currentClass;

        Compiler();

        unsigned short current = 0;

        static void PUSH__IDX(Cake_Compiler::Compiler *compiler, stringstream &byteCode, unsigned short value);

        static void RESERVE__PUSH__IDX_WITH_OP(Cake_Compiler::Compiler *compiler);
    };
}

#endif