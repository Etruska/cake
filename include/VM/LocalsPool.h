#ifndef VM_LOCALSPOOL_H
#define VM_LOCALSPOOL_H

#include "VMObject.h"

#include <string>
#include <vector>
#include "Utils/VMObjectPool.h"

using namespace std;

namespace VM {
    class LocalsPool {
    public:
        void store(int index, VMObject *obj);

        VMObject *load(int index);

        int getSize();
        void dump();
    private:
        VMObjectPool<VMObject *> pool;
    };
}

#endif