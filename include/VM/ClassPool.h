#ifndef VM_CLASSPOOL_H
#define VM_CLASSPOOL_H

#include "VMObject.h"

#include <string>
#include <iostream>
#include <vector>

using namespace std;

namespace VM {
    template<class A>
    class ClassPool {

    public:

        int add(A constant) {
            int index = getIndexOf(constant);
            if (index == -1) {
                this->pool.push_back(constant);
                return this->pool.size() - 1;
            }
            return index;
        }

        A get(int index) {
            if (!this->isIndexInRange(index)) {
                cerr << "ClassPool: Index " << index << " out of range." << endl;
            }

            return this->pool[index];
        }

        int getIndexOf(A constant) {
            for (int i = 0; i < this->pool.size(); i++) {
                if (this->pool[i]->equals(constant))
                    return i;
            }
            return -1;
        }

        bool isIndexInRange(int index) {
            return !(index < 0 || index >= this->pool.size());
        }

        int size(){
            return this->pool.size();
        }
        void dump() {
            cout << "Dump class pool" << endl;
            for (int i = 0; i < this->pool.size(); i++) {
                cout << "\t" << i << ": " << this->pool[i]->getName() << endl;
            }
        }

    protected:
        vector<A> pool;
    };
}
#endif