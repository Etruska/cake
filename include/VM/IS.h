#ifndef VM_IS_H
#define VM_IS_H

#include <string>

using namespace std;

#define VM__INSTRUCTION                     (unsigned char)(0b10000000)
#define VM__INSTRUCTION__OPERATOR           ((unsigned char)(0b01000000) | VM__INSTRUCTION )

#define VM__INSTRUCTION__STORE              (  1 | VM__INSTRUCTION )
#define VM__INSTRUCTION__LOAD               (  2 | VM__INSTRUCTION )
#define VM__INSTRUCTION__FETCH              (  3 | VM__INSTRUCTION )
#define VM__INSTRUCTION__PUSH_CONST         (  4 | VM__INSTRUCTION )
#define VM__INSTRUCTION__PRINT              (  5 | VM__INSTRUCTION )
#define VM__INSTRUCTION__GOTO               (  6 | VM__INSTRUCTION )
#define VM__INSTRUCTION__RETURN             (  7 | VM__INSTRUCTION )
#define VM__INSTRUCTION__IF_CMPR            (  8 | VM__INSTRUCTION )
#define VM__INSTRUCTION__NEW                (  9 | VM__INSTRUCTION )
#define VM__INSTRUCTION__GETFIELD           (  10 | VM__INSTRUCTION )
#define VM__INSTRUCTION__SETFIELD           (  11 | VM__INSTRUCTION )
#define VM__INSTRUCTION__INVOKE             (  12 | VM__INSTRUCTION )

#define VM__INSTRUCTION__OP__LESS_THAN      (  1 | VM__INSTRUCTION__OPERATOR )
#define VM__INSTRUCTION__OP__GREATER_THAN   (  2 | VM__INSTRUCTION__OPERATOR )
#define VM__INSTRUCTION__OP__GREATER_OR_EQ  (  3 | VM__INSTRUCTION__OPERATOR )
#define VM__INSTRUCTION__OP__LESS_OR_EQ     (  4 | VM__INSTRUCTION__OPERATOR )
#define VM__INSTRUCTION__OP__EQUAL          (  5 | VM__INSTRUCTION__OPERATOR )
#define VM__INSTRUCTION__OP__NOT_EQUAL      (  6 | VM__INSTRUCTION__OPERATOR )



#define VM__INSTRUCTION__OP__PLUS           (  7 | VM__INSTRUCTION__OPERATOR )
#define VM__INSTRUCTION__OP__MINUS          (  8 | VM__INSTRUCTION__OPERATOR )
#define VM__INSTRUCTION__OP__DIVIDE         (  9 | VM__INSTRUCTION__OPERATOR )
#define VM__INSTRUCTION__OP__RESIDUUM       ( 10 | VM__INSTRUCTION__OPERATOR )
#define VM__INSTRUCTION__OP__TIMES          ( 11 | VM__INSTRUCTION__OPERATOR )

#define VM__INSTRUCTION__OP__NEGATE         ( 12 | VM__INSTRUCTION__OPERATOR )

namespace VM {
    class IS {
    public:
        enum Instructions {
            /** STORE <idx> stores variable from top of stack to locals pool at index <idx> */
                    STORE = VM__INSTRUCTION__STORE,
            /** LOAD <idx> loads variable from locals pool at index <idx> and stores it  */
                    LOAD = VM__INSTRUCTION__LOAD,
            FETCH = VM__INSTRUCTION__FETCH,
            /** PUSH_CONST <idx> pushes variable at index <idx> from constant pool to stack  */
                    PUSH_CONST = VM__INSTRUCTION__PUSH_CONST,
            /** D_PRINT prints debug information about object from top of stack */
                    D_PRINT = VM__INSTRUCTION__PRINT,
            /** GOTO <BPidx1, BPidx2> jumps to bytecode at index BPidx */
                    GOTO = VM__INSTRUCTION__GOTO,
            /** IF_CMPR <jumpIdx1, jumpIdx2> if STACK[TOP] is true,
            * jump to index jumpIdx
            * operation codes:
            * 0: <
            * 1: <=
            * 2: >=
            * 3: >
            */
                    IF_CMPR = VM__INSTRUCTION__IF_CMPR,
            /** OPERATOR */
                    OPERATOR = VM__INSTRUCTION__OPERATOR,
            /** NEW <idx> creates object of type <idx> from class index and pushs it on STACK[TOP] */
                    NEW = VM__INSTRUCTION__NEW,
            /** GETFIELD <idx> gets field <idx> from object on STACK[TOP] */
                    GETFIELD = VM__INSTRUCTION__GETFIELD,
            /** SETFIELD <idx> sets field at <idx> to value STACK[TOP] on object STACK[TOP - 1] */
                    SETFIELD = VM__INSTRUCTION__SETFIELD,
            /** INVOKE calls method <idx> from object STACK[TOP] */
                    INVOKE = VM__INSTRUCTION__INVOKE,
            /** RETURN returns to  */
                    RETURN = VM__INSTRUCTION__RETURN
        };

        static string toString(const unsigned char instruction) {
            switch (instruction) {
                case VM__INSTRUCTION__STORE:
                    return "STORE";
                case VM__INSTRUCTION__LOAD:
                    return "LOAD";
                case VM__INSTRUCTION__FETCH:
                    return "FETCH";
                case VM__INSTRUCTION__PUSH_CONST:
                    return "PUSH_CONST";
                case VM__INSTRUCTION__PRINT:
                    return "PRINT";
                case VM__INSTRUCTION__GOTO:
                    return "GOTO";
                case VM__INSTRUCTION__RETURN:
                    return "RETURN";
                case VM__INSTRUCTION__IF_CMPR:
                    return "IF_CMPR";
                case VM__INSTRUCTION__NEW:
                    return "NEW";
                case VM__INSTRUCTION__GETFIELD:
                    return "GETFIELD";
                case VM__INSTRUCTION__SETFIELD:
                    return "SETFIELD";
                case VM__INSTRUCTION__INVOKE:
                    return "INVOKE";

                    // OPERATORS
                case VM__INSTRUCTION__OP__LESS_THAN:
                    return "OP__LESS_THAN";
                case VM__INSTRUCTION__OP__GREATER_THAN:
                    return "OP__GREATER_THAN";
                case VM__INSTRUCTION__OP__GREATER_OR_EQ:
                    return "OP__GREATER_OR_EQ";
                case VM__INSTRUCTION__OP__LESS_OR_EQ:
                    return "OP__LESS_OR_EQ";
                case VM__INSTRUCTION__OP__EQUAL:
                    return "OP__EQUAL";
                case VM__INSTRUCTION__OP__NOT_EQUAL:
                    return "OP__NOT_EQUAL";

                case VM__INSTRUCTION__OP__PLUS:
                    return "OP__PLUS";
                case VM__INSTRUCTION__OP__MINUS:
                    return "OP__MINUS";
                case VM__INSTRUCTION__OP__DIVIDE:
                    return "OP__DIVIDE";
                case VM__INSTRUCTION__OP__RESIDUUM:
                    return "OP__RESIDUUM";
                case VM__INSTRUCTION__OP__TIMES:
                    return "OP__TIMES";

                default:
                    return "N/A";
            }
        }


    };
}

#endif
