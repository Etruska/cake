#ifndef VM_VMNATIVECLASS_H
#define VM_VMNATIVECLASS_H

#include <string>
#include <vector>
#include "VMClass.h"
#include "Utils/pool.h"

using namespace std;
namespace VM {
    class VMNativeClass : public VMClass {
    public:
        VMNativeClass(string name, Pool<string>* methodMap);

        virtual void executeMethod(int methodId, VMObject* instance, VMCore* core) {}
        virtual void registerMethodNames() = 0;

        virtual int getType() const {
            return VM__NATIVE_CLASS;
        }

    protected:
        Pool<string>* methodMap;
    };
}

#endif