#ifndef VM_STACK_H
#define VM_STACK_H

#include "VMObject.h"
#include <vector>

using namespace std;

namespace VM {
    class Stack {
    public:
        void push(VMObject *obj);

        VMObject *pop();

        bool isEmpty();

        int size();

        void dump();
    private:
        vector<VMObject *> items;
    };
}

#endif