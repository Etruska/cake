#ifndef VM_VMCORE_H
#define VM_VMCORE_H

#include "Bytecode.h"
#include "ConstantPool.h"
#include "LocalsPool.h"
#include "ClassPool.h"
#include "Stack.h"
#include "StackFrame.h"
#include <sstream>
#include <Heap/HeapManager.h>

using namespace std;

#define NO_ARG -1

namespace VM {
    class VMClass;
    class StackFrame;
    class VMCore {
    public:
        VMCore(Heap::HeapManager* heap);

        ~VMCore();

        void runBytecode(Bytecode *bytecode);


        void loadConstantPool(ConstantPool *pool);

        Stack *getStack();

        Heap::HeapManager* getHeap();

        stringstream printStream;

        string getLastPrintline();

        void setClassPool(ClassPool<VMClass *> *classPool) {
            this->classPool = classPool;
        }

        int mergeBytes(char idx1, char idx2);

        void print();

        void pushConst(int index);

        void debugPrint();

        void gotoIdx(int jmpIdx);

        void ifCompare(int jmpIdx);

        void store(int idx);

        void load(int idx);

        void callOperator(int operatorType);

        void newObj(int idx);

        void getField(int idx);

        void setField(int idx);

        void invoke(int idx);

        void methodReturn();

        StackFrame *getCurrStackFrame() const {
            return this->stackFrame;
        }

        void run(int classIdx, int methodIdx, int argId = NO_ARG);
        StackFrame * rootStackFrame;

        LocalsPool * getLocalsPool();
    private:
        void runBytecode();
        StackFrame *stackFrame;
        string lastPrintline;
        Heap::HeapManager* heap;
        ConstantPool *constantPool;
        ClassPool<VMClass *> *classPool;
        Stack *stack;
    };
}

#endif