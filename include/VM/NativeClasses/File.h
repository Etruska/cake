#ifndef VM_NATIVE_CLASS__FILE_H
#define VM_NATIVE_CLASS__FILE_H

#include "VM/VMNativeClass.h"

namespace VM {
    class ClassFile : public VMNativeClass {
    public:
        ClassFile(Pool<string>* methodMap) : VMNativeClass("File", methodMap) {registerMethodNames();};
        virtual void executeMethod(int methodId, VMObject* instance, VMCore* core);

        virtual void registerMethodNames();
    };
}

#endif