#ifndef VM_NATIVE_CLASS__STRING_H
#define VM_NATIVE_CLASS__STRING_H

#include "VM/VMNativeClass.h"

namespace VM {
    class ClassString : public VMNativeClass {
    public:
        ClassString(Pool<string>* methodMap) : VMNativeClass("StringModifier", methodMap) {registerMethodNames();};
        virtual void executeMethod(int methodId, VMObject* instance, VMCore* core);

        virtual void registerMethodNames();
    };
}

#endif