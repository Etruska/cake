#ifndef VM_NATIVE_CLASS__IO_H
#define VM_NATIVE_CLASS__IO_H

#include "VM/VMNativeClass.h"

#include <string>

using namespace std;

namespace VM {
    class ClassIO : public VMNativeClass {
    public:
        ClassIO(Pool<string>* methodMap) : VMNativeClass(string("IO"), methodMap) {registerMethodNames();};
        virtual void executeMethod(int methodId, VMObject* instance, VMCore* core);

        virtual void registerMethodNames();
    };
}

#endif