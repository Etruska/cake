#ifndef VM_VMOBJECT_H
#define VM_VMOBJECT_H

#include <string>

using namespace std;

#define COMPARE__SMALLER            ( 0b00000001 )
#define COMPARE__EQUAL              ( 0b00000010 )
#define COMPARE__BIGGER             ( 0b00000100 )

#define COMPARE__NOT_EQUAL          ( COMPARE__SMALLER | COMPARE__BIGGER )

#define COMPARE__SMALLER_OR_EQUAL   ( COMPARE__SMALLER | COMPARE__EQUAL )
#define COMPARE__BIGGER_OR_EQUAL    ( COMPARE__BIGGER  | COMPARE__EQUAL )

namespace Heap {
    class HeapManager;
}

namespace VM {
    class VMObject {
    public:
        enum Type {
            VMInteger, VMBoolean, VMString, VMBuiltinFunction, VMUserFunction, VMObjectRef
        };

        string typeName[6] = {"integer", "boolean", "string", "builtin function", "user function", "object reference"};

        virtual Type getType() = 0;

        virtual bool equals(VMObject *obj) = 0;

        virtual unsigned char compare(VMObject *obj = NULL) = 0;

        virtual string toString() = 0;

        virtual VMObject * instance();

        virtual VMObject * clone(Heap::HeapManager* heap, bool forceCopy = false) = 0;

        virtual string debugToString();

        virtual unsigned int heapSize();

        bool isGcManaged();

        virtual bool isFalse();

        virtual void markAsAlive(Heap::HeapManager* heap);

        bool sameType(VMObject *obj) {
            if (!obj) {
                return false;
            }
            return this->getType() == obj->getType();
        }

        bool isAlive = false;

        string getTypeName();
    };
}

#endif