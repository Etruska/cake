#ifndef VM_VMEXCEPTION_H
#define VM_VMEXCEPTION_H

namespace VM {
    class VMException {
    public:
        enum Bytecode {
            BP_OUT_OF_RANGE = 1
        };
        enum Stack {
            POP_ON_EMPTY_STACK = 2
        };
        enum VMCore {
            UNCOMPARABLE_ENTRIES = 3,
            UNKNOWN_OPERATOR = 4,
            GETFIELD_FROM_NONOBJECT = 5,
            SETFIELD_TO_NONOBJECT = 6
        };
        enum LocalsPool {
            INVALID_LOCATION = 7
        };
        enum VMClass {
            METHOD_ALREADY_LOADED = 8,
            FIELD_ALREADY_LOADED = 9
        };
        enum VMMethod {
            BUILTIN_FUNCTION_HAS_NO_BYTECODE = 10,
            INVOKE_ON_NONOBJECT = 11,
            INVOKE_ON_NIL = 12
        };
    };
}

#endif
