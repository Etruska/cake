#ifndef VM_VMSTRING_H
#define VM_VMSTRING_H

#include "VMobject.h"
#include <string>

using namespace std;

namespace VM {
    class VMString : public VMObject {
    public:
        VMString(string value);

        string getValue();

        Type getType();

        bool equals(VMObject *obj);

        string toString();
        string debugToString();

        unsigned char compare(VMObject *obj = NULL);


        virtual VMObject *clone(Heap::HeapManager* heap, bool forceCopy = false);

    private:
        string value;
    };
}

#endif