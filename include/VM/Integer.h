#ifndef VM_INTEGER_H
#define VM_INTEGER_H

#include "VMobject.h"

using namespace std;

namespace VM {
    class Integer : public VMObject {
    public:
        Integer(int value);

        Integer(string value);

        int getValue();

        Type getType();

        string toString();

        bool equals(VMObject *obj);

        unsigned char compare(VMObject *obj = NULL);

        VMObject * clone(Heap::HeapManager* heap, bool forceCopy = false);

        void setValue(int value) {
            Integer::value = value;
        }

        virtual bool isFalse();

    private:
        int value;
    };
}

#endif