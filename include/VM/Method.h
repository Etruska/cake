#ifndef VM_METHOD_H
#define VM_METHOD_H

#include <string>
#include "Bytecode.h"
#include "ConstantPool.h"
#include "VMField.h"

using namespace std;

namespace VM {
    class Method{
    public:
        enum Type {
            VMMethod, BuiltInFunction
        };

        string typeName[2] = {"user method", "builtin function"};

        virtual Type getType() = 0;

        string getTypeName() {
            return this->typeName[this->getType()];
        }

        int addArgument(VMField *field) {
            this->arguments.push_back(field);
            return this->arguments.size() - 1;
        }

        virtual string getName() const {
            return name;
        }

    protected:
        string name;
        vector<VMField *> arguments;
    };
}

#endif