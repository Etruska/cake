#ifndef VM_VMMETHOD_H
#define VM_VMMETHOD_H

#include <string>
#include "Method.h"

using namespace std;

namespace VM {
    class VMMethod : public Method{
    public:
        VMMethod(string name, Bytecode *code);

        virtual Type getType();

        vector<VMField *> const &getArguments() const {
            return arguments;
        }

        Bytecode *getBytecode() const {
            return bytecode;
        }

    protected:
        Bytecode * bytecode;
    };
}

#endif