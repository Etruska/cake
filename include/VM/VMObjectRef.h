#ifndef VM_VMOBJECTREF_H
#define VM_VMOBJECTREF_H

#include "VMObject.h"
#include "VMClass.h"

using namespace std;
namespace VM {
    class VMClass;
    class VMObjectRef : public VMObject {
    public:
        VMObjectRef(VMObject **propertiesStart, VMClass *classRef);

        VMObject **getPointer();

        VMClass *getClassRef();

        Type getType();

        bool equals(VMObject *obj);

        string toString();

        string debugToString();

        virtual unsigned char compare(VMObject *obj);


        virtual VMObject *clone(Heap::HeapManager* heap, bool forceCopy = false);


        virtual unsigned int heapSize() override;


        virtual void markAsAlive(Heap::HeapManager* heap) override;

// GC Flasg
        bool copied;
        bool visited;
        bool alive;
    private:
        VMObject** objPointer;
        VMClass *classRef;
    };
}

#endif