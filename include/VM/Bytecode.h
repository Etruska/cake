#ifndef VM_BYTECODE_H
#define VM_BYTECODE_H

#include <iostream>
#include "VM/ConstantPool.h"

using namespace std;

namespace VM {
    class Bytecode {
    public:

        Bytecode();

        Bytecode(const unsigned char *code, int length);
        Bytecode(string code);

        void loadCode(const unsigned char *code, int length);

        bool hasNext();

        unsigned char getNext();

        unsigned char getCodeAt(int bp);

        int getPointer();

        void setPointer(int bp);

        ~Bytecode();

        void dump(ConstantPool *constantPool = NULL);

        int getLength() const {
            return length;
        }

    private:
        const unsigned char *code;
        int length = 0;
        int pointer = 0;
    };
}

#endif