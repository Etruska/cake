#ifndef VM_BUILTINFUNCTION_H
#define VM_BUILTINFUNCTION_H

#include "VMObject.h"

#include "VMCore.h"

#include <string>

using namespace std;

namespace VM {
    class BuiltinFunction : public Method {
    public:
        BuiltinFunction(String name, void *(*function)(VMCore *, int));

        void execute();

        Type getType();

    private:
        string name;

        /**
        * pointer to function, which receives VMCore instance and argument count
        */
        void *(*function)(VMCore *, int);
    };
}

#endif