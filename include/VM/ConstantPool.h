#ifndef VM_CONSTANTPOOL_H
#define VM_CONSTANTPOOL_H

#include "VMObject.h"

#include <string>
#include <vector>

using namespace std;

#define VM__TRUE__ID 0
#define VM__FALSE__ID 1

namespace VM {
    class ConstantPool {

    public:
        ConstantPool();

        int add(VMObject *constant);

        int getIndexOf(VMObject *constant);

        VMObject *get(int index);

        bool isIndexInRange(int index);

        // PREDEFINED CONSTANTS
        VMObject *c_TRUE;
        VMObject *c_FALSE;

    protected:
        vector<VMObject *> pool;
    };
}

#endif