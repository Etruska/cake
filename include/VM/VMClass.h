#ifndef VM_VMCLASS_H
#define VM_VMCLASS_H

#include <string>
#include <vector>
#include "ClassPool.h"
#include "Bytecode.h"
#include "ConstantPool.h"
#include "Method.h"
#include "VMField.h"
#include "Utils/pool.h"
#include "VMCore.h"

#define VM__USER_CLASS 0
#define VM__NATIVE_CLASS 1

using namespace std;
namespace VM {
    class VMCore;
    class VMClass {
    public:
        VMClass(string name, VMClass *superclass);

        int addMethod(Method *method);

        Method *getMethod(string signature);

        bool hasMethod(string signature);

        int addField(VMField *field);

        void updateField(VMField *field, int index);

        VMField *getField(int index);

        VMClass *getSuperClass();

        string getName() const {
            return name;
        }

        virtual void executeMethod(int methodId, VMObject* instance, VMCore* core) {}

        virtual int getType() const {
            return VM__USER_CLASS;
        }

        ClassPool<VMField *> getFields();

        bool equals(VMClass * targetClass){
            if(this->name.compare(targetClass->getName()) != 0){
                return false;
            } else {
                return true;
            }
        }

        unsigned int heapSize();

        Pool<Method *> methods;

    protected:
        string name;

        VMClass *superClass;
        ClassPool<VMField *> fields;
    };
}

#endif