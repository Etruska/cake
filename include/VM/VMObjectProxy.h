#ifndef VM_VMOBJECTPROXY_H
#define VM_VMOBJECTPROXY_H

#include "VMObject.h"
#include "Integer.h"
#include "VMString.h"
#include "VMObjectRef.h"

using namespace std;

namespace VM {
    class VMObjectProxy : public VMObject {
    public:
        VMObjectProxy(VMObject* instance, VMObjectProxy* previous, unsigned int size);

        virtual VMObject* instance();

        virtual Type getType();

        virtual bool equals(VMObject *obj);

        virtual unsigned char compare(VMObject *obj = NULL);

        virtual string toString();

        virtual VMObject * clone(Heap::HeapManager* heap, bool forceCopy);
        void heapClone(Heap::HeapManager* heap);

        bool copied;
        bool visited;
        bool alive;
        unsigned int size;


        void resetFlags();
        VMObjectProxy* getPreviousProxy();
        void setPreviousProxy(VMObjectProxy* proxy);
        void setInstance(VMObject* instance);

        virtual void markAsAlive(Heap::HeapManager* heap) override;

    protected:
        VMObject* _instance;
        VMObjectProxy* _previous;
    };
}

#endif