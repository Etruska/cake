#ifndef VM_STACKFRAME_H
#define VM_STACKFRAME_H

#include "VMMethod.h"
#include "VMObjectProxy.h"
#include "LocalsPool.h"

using namespace std;

namespace VM {
    class VMObjectProxy;
    class StackFrame {
    public:
        StackFrame(VMObjectProxy * proxy, Method * method, StackFrame *parent);
        StackFrame *parent;
        Method * method;
        Bytecode * getBytecode();
        LocalsPool * getLocalsPool();
        VMObjectProxy * thisObjProxy;
        int bp;
        void savePointer();
        void loadPointer();
        void dump(bool root = true);
        void dumpError();
    private:
        LocalsPool * localsPool;
    };
}

#endif