//
// Created by Vojtech Petrus on 11/04/15.
//

#ifndef CAKE_VMFIELD_H
#define CAKE_VMFIELD_H

#include <string>
#include "VMObject.h"

using namespace std;

namespace VM {
    class VMField {

    public:
        VMField();

        VMField(VMObject::Type type, string signature);

        string getSignature();

        bool equals(VMField * target){
            return false;
        }

    private:
        string signature;
    };
}


#endif //CAKE_VMFIELD_H
