#ifndef PAA_PARAMETERS_H
#define PAA_PARAMETERS_H

#include <vector>
#include <map>

#define DEFAULT_PARAMETER_STRING_PRECISION 3

using namespace std;

class Parameters
{
protected:
    map<string, string> parameters;

public:
    Parameters();
    Parameters(int argc, char* argv[]);

    void parseParametersFromString(int argc, char* argv[]);

    map<string, string>& getParameters();

    bool hasParameter(const string& key);
    string getParameter(const string& key);
    void setParameter(const string& key, const string& parameterValue);
    /*void setParameter(const string& key, const bool& parameterValue);
    void setParameter(const string& key, const int& parameterValue);
    void setParameter(const string& key, const double& parameterValue, const int &precision = DEFAULT_PARAMETER_STRING_PRECISION);*/
    void setParameterDefaultValue(const string& key, const string& parameterValue);

    string getStringParameter(const string& key);
    bool getBooleanParameter(const string& key);
    int getIntegerParameter(const string& key);
    double getDoubleParameter(const string& key);

    void dump();
};


#endif