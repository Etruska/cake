#ifndef PAA_STRING_UTILS_H
#define PAA_STRING_UTILS_H

#include <vector>
#include <iostream>
#include <sstream>
#include <string>
#include <map>
#include <functional>
#include <algorithm>

using namespace std;

class StringUtils
{
public:
    static vector<string> explode(const string & s, char delim);

    static bool stringToBool(const string raw);
    static int stringToInt(const string raw);
    static double stringToDouble(const string raw);

    static string boolToString(const bool& value);
    static string intToString(const int& value);
    static string doubleToString(const double& value, const int &precision = 3);
    static string randomString(size_t length);
};


#endif
