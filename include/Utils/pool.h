#ifndef CAKE__UTILS__POOL
#define CAKE__UTILS__POOL

#include <string>
#include <map>
#include "VM/VMMethod.h"


using namespace std;
using namespace VM;

#define POOL__NO_LIMIT -1
#define POOL__DEFAULT_LIMIT 127

template <class Type> class Pool {
protected:
    map<int, Type> poolData;
    int limit;


public:
    Pool(int limit) : limit(limit) { }
    Pool() : limit(POOL__DEFAULT_LIMIT) { }

    int getId(Type item) {
        for (auto poolItem : poolData) {
            if (poolItem.second == item) {
                return poolItem.first;
            }
        }

        int poolSize = poolData.size();

        if (limit != POOL__NO_LIMIT && poolSize + 1 > limit) {
            throw "POOL LIMIT EXCEED";
        }

        poolData.insert(pair<int, Type>(poolData.size(), item));

        return poolSize;
    }

    Type getById(int id){
        if(poolData.find(id) == poolData.end()) {
            cerr << "POOL: ACCESSING UNKNOWN ID " << endl;
            throw "POOL: ACCESSING UNKNOWN ID ";
        }
        return poolData.find(id)->second;
    }

    int getByValue(Type item){
        for (auto poolItem : poolData) {
            if (poolItem.second == item) {
                return poolItem.first;
            }
        }

        cerr << "POOL: ACCESSING UNKNOWN ID " << endl;
        throw "POOL: ACCESSING UNKNOWN ID ";
    }


    int setItem(Type item, int index) {
        int poolSize = poolData.size();

        if (limit != POOL__NO_LIMIT && poolSize + 1 > limit) {
            throw "POOL LIMIT EXCEED";
        }


        if(poolData.find(index) == poolData.end()) {
            poolData.insert(pair<int, Type>(index, item));
        }  else {
            poolData.find(index)->second = item;
        }

        //TODO: remove
        return index;
    }

    int size() {
        return poolData.size();
    }

    void dump() {
        for (auto item : poolData) {
            cout << item.first << ": " << item.second << endl;
        }
    }
};

#endif