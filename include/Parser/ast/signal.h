#ifndef PARSER_AST__SIGNAL__H
#define PARSER_AST__SIGNAL__H

#include "Parser/node.h"
#include "Parser/astNode.h"
#include "Parser/ast/expression.h"

#include <iostream>
#include <string>
#include <sstream>

using namespace std;

#define SIGNAL__NOT_SET             0
#define SIGNAL__CALL                1
#define SIGNAL__METHOD_ON_OBJECT    2
#define SIGNAL__CALLED_OBJECT       3
#define SIGNAL__CALLED_METHOD       4
#define SIGNAL__OPERATOR            5
#define SIGNAL__RETURN              6
#define SIGNAL__THROW               7
#define SIGNAL__PRINT               8
#define SIGNAL__BREAK               9
#define SIGNAL__CONTINUE           10
#define SIGNAL__THIS               11
#define SIGNAL__SUPER              12
#define SIGNAL__INSTANCE_VARIABLE  13


namespace Cake_Parser {
    class Signal_Ast : public Expression_AstNode {
    public:
        string name;
        unsigned int signalType;
        unsigned int type;
        vector<Expression_AstNode *> *parameters;

        Signal_Ast();

        unsigned int parametersCount();

        virtual void dump(stringstream &out, string prefix) override;

        static Signal_Ast *createCustom(unsigned int signalType = SIGNAL__CALL,
                                        const unsigned int type = PROCEDURAL_NODE, string content = "");

        static Signal_Ast *parse(Node **iterator, Expression_AstNode **chainsLastPeace = NULL);

        static Signal_Ast *parseOrphan(Node **iterator, unsigned int signalType = SIGNAL__CALL);

        static void iteratorNext(Node ***iterator, Node **node);

        static string signalTypeToString(unsigned int signalType);

        virtual string compile(Cake_Compiler::Compiler *compiler);
    };
}
#endif