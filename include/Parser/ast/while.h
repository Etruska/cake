#ifndef PARSER_AST__WHILE__H
#define PARSER_AST__WHILE__H

#include "Parser/node.h"
#include "Parser/astNode.h"
#include "Parser/ast/expression.h"

#include <iostream>
#include <string>

using namespace std;

namespace Cake_Parser {
    class While_Ast : public Expression_AstNode {
    public:
        While_Ast();

        Expression_AstNode *condition;
        Expression_AstNode *body;

        virtual void dump(stringstream &out, string prefix);

        static While_Ast *parse(Node **node);

        virtual string compile(Cake_Compiler::Compiler *compiler);
    };
}
#endif