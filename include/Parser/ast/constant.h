#ifndef PARSER_AST__CONSTANT__H
#define PARSER_AST__CONSTANT__H

#include "Parser/node.h"
#include "Parser/astNode.h"
#include "Parser/ast/expression.h"

#include <iostream>
#include <string>

using namespace std;

namespace Cake_Parser {
    class Constant_Ast : public Expression_AstNode {
    public:
        string name;
        unsigned int constantType;

        Constant_Ast();

        void dump(stringstream &out, string prefix);


        static Expression_AstNode *parse(Node **iterator);

        virtual string compile(Cake_Compiler::Compiler *compiler);
    };
}
#endif