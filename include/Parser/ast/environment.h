#ifndef PARSER_AST__ENVIRONMENT__H
#define PARSER_AST__ENVIRONMENT__H

#include "Parser/node.h"
#include "Parser/astNode.h"

#include "Parser/ast/class.h"

using namespace std;

namespace Cake_Parser {
    class Environment_Ast : public AstNode {
    public:
        vector<Class_Ast *> classes;

        Environment_Ast();

        static Environment_Ast *parse(Node *node);

        void dump(stringstream &out);

        virtual void dump();

        virtual string dumpToString();

        virtual string dumpToInlineString();

        static void validate(Node *node);

        virtual string compile(Cake_Compiler::Compiler *compiler);

        virtual void dumpCompiled(Cake_Compiler::Compiler *compiler);
    };

}
#endif