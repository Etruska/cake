#ifndef PARSER_AST__CLASS__H
#define PARSER_AST__CLASS__H

#include "Parser/node.h"
#include "Parser/astNode.h"
#include "Parser/ast/method.h"

#include <iostream>
#include <string>
#include <sstream>

#include "Utils/pool.h"
#include "VM/VMClass.h"

using namespace std;
using namespace VM;

namespace Cake_Parser {
    class Method_Ast;

    class Class_Ast : public AstNode {
    public:
        string name;
        Class_Ast *parent;
        vector<string> rawProperties;

        Pool<string> propertiesMap;

        vector<Method_Ast *> methods;

        VMClass* linkedClass;

        Class_Ast();

        void dump(stringstream &out);

        void addMethod(Method_Ast *method);

        static Class_Ast *parse(Node *node);

        virtual string compile(Cake_Compiler::Compiler *compiler);


        virtual void dumpCompiled(Cake_Compiler::Compiler *compiler) override;
    };
}
#endif