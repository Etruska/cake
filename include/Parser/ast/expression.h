#ifndef PARSER_AST__EXPRESSION__H
#define PARSER_AST__EXPRESSION__H


#include "Parser/node.h"
#include "Parser/astNode.h"

#include <iostream>
#include <string>
#include <vector>
#include <sstream>

using namespace std;

#define EXPRESSION_TYPE__EXPRESSION 0
#define EXPRESSION_TYPE__SIGNAL 1
#define EXPRESSION_TYPE__CONSTANT 2
#define EXPRESSION_TYPE__IF 3
#define EXPRESSION_TYPE__TRY 4
#define EXPRESSION_TYPE__WHILE 5

#define CONTEXT__BASE                 0b0000000000000001
#define CONTEXT__DOT_CHAIN            0b0000000000000010

#define PARSER_MODE__TO_EOL                 0b0000000000000001
#define PARSER_MODE__ONLY_ONE               0b0000000000000010
#define PARSER_MODE__DONT_UNWRAP_SINGLE     0b0000000000000100


namespace Cake_Parser {
    class Expression_AstNode : public AstNode {
    protected:
        unsigned int type;
    public:
        virtual unsigned int getType();

        virtual void dump();

        virtual void dump(stringstream &out);

        virtual void dump(stringstream &out, string prefix);

        vector<Expression_AstNode *> content;

        Expression_AstNode();

        void addContent(Expression_AstNode *node);

        static Expression_AstNode *parseTree(Node *node, unsigned int mode = PARSER_MODE__TO_EOL);

        static Expression_AstNode *parse(Node **from, Node *to, unsigned int mode = PARSER_MODE__TO_EOL);

        static Expression_AstNode *parseUnary(Node **node);

        static vector<Expression_AstNode *> *parseParams(Node *node, vector<Expression_AstNode *> *params = NULL);

        static string typeToString(unsigned int type);

        virtual string compile(Cake_Compiler::Compiler *compiler);
    };
}
#endif