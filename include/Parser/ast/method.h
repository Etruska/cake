#ifndef PARSER_AST__METHOD__H
#define PARSER_AST__METHOD__H

#include "Parser/astNode.h"
#include "Parser/node.h"
#include "Parser/ast/expression.h"
#include "Utils/pool.h"
#include <iostream>
#include <string>

using namespace std;

using namespace VM;

namespace Cake_Parser {
    class Expression_AstNode;

    class Method_Ast : public AstNode {
    public:
        string name;
        vector<Expression_AstNode *> *arguments;
        Expression_AstNode *body;

        void dump(stringstream &out);

        Method_Ast();

        static Method_Ast *parse(Node *node);

        virtual string compile(Cake_Compiler::Compiler *compiler);

        // compiled
        Pool<string> localsMap;
        Bytecode* methodBytecode;


        virtual void dumpCompiled(Cake_Compiler::Compiler *compiler) override;
    };
}

#endif