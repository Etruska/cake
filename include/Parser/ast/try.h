#ifndef PARSER_AST__TRY__H
#define PARSER_AST__TRY__H

#include "Parser/node.h"
#include "Parser/astNode.h"
#include "Parser/ast/expression.h"

#include <iostream>
#include <string>

using namespace std;

namespace Cake_Parser {
    class Try_Ast : public Expression_AstNode {
    public:
        Try_Ast();

        unsigned int type();

        void dump();

        static Try_Ast *parse(Node **node);

        virtual string compile(Cake_Compiler::Compiler *compiler);
    };
}
#endif