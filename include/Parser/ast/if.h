#ifndef PARSER_AST__IF__H
#define PARSER_AST__IF__H

#include "Parser/node.h"
#include "Parser/astNode.h"
#include "Parser/ast/expression.h"

#include <iostream>
#include <string>

using namespace std;

#define PARSER_IF_MODE__IF 0
#define PARSER_IF_MODE__ESLEIF 1

namespace Cake_Parser {
    class If_Ast : public Expression_AstNode {
    public:
        If_Ast();

        Expression_AstNode *condition;
        Expression_AstNode *body;
        vector<If_Ast *> elseIfs;
        Expression_AstNode *elseBody;

        virtual void dump(stringstream &out, string prefix);

        static If_Ast *parse(Node **node, unsigned int mode = PARSER_IF_MODE__IF);

        virtual string compile(Cake_Compiler::Compiler *compiler);
    };
}
#endif