#ifndef PARSER_AST_NODE__H
#define PARSER_AST_NODE__H

#include <string>
#include <iostream>
#include <sstream>

#include "Configuration/config.h"
#include "Helpers/bytecode.h"

using namespace std;

namespace Cake_Compiler {
    class Compiler;
}

namespace Cake_Parser {
    class AstNode {
    public:
        virtual void dump();

        virtual string dumpToString();

        virtual void dump(stringstream &out) = 0;

        static void compile_error(string message);

        virtual string compile(Cake_Compiler::Compiler *compiler) = 0;

        virtual void dumpCompiled(Cake_Compiler::Compiler *compiler) {};
    };
}

#include "Compiler/compiler.h"

#endif