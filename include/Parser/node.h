#ifndef PARSER_NODE__H
#define PARSER_NODE__H

#include <string>
#include <iostream>
#include <vector>
#include "nodeTypes.h"

using namespace std;

#define IGNORE_NODE_CONTENT "____IGNORE_NODE___qwerty"

namespace Cake_Parser {
    class Parser;
    class Node {
    public:
        Node *parent;
        vector<Node *> children;
        unsigned int type;
        string content;

        Node(unsigned int type, Node *_parent = NULL) : type(type) {
            children = vector<Node *>();
            content = "";

            if (_parent) {
                _parent->addChild(this);
            } else {
                parent = NULL;
            }
        }

        static bool parse(string content, int &i, int &context, int indentationLevel, Node *parent, Parser *parser);

        static string typeToString(const unsigned int &type);

        void dump(string prefix = "");

        string dumpContent();

        void dumpTree(string prefix = "", bool isLast = true, Node *highlight = NULL);

        void dumpFromParent();

        void dumpWholeTree(Node *highlight = NULL);

        void addChild(Node *child);

        Node *addChild(unsigned int type, string content);

        Node *lastChild();

        Node *firstChild();

        string childsContent(unsigned int id);

        Node *prevSibling(Node *child);

        Node *nextSibling(Node *child);

        Node *prev();

        Node *self();

        Node *next();

        bool prevIs(unsigned int type, string content = IGNORE_NODE_CONTENT);

        bool nextIs(unsigned int type, string content = IGNORE_NODE_CONTENT);

        bool nodeIs(Node *node, unsigned int type, string content = IGNORE_NODE_CONTENT);


        bool is(unsigned int type, const string content = IGNORE_NODE_CONTENT);

        bool isLike(unsigned int type, unsigned int mask = 0);

        bool childIs(const unsigned int id, unsigned int type, const string content = IGNORE_NODE_CONTENT);

        bool hasChildren(unsigned int count);

        bool hasNoChildren();
    };
}

#include "nodes/expression.h"
#include "nodes/class.h"
#include "nodes/method.h"
#include "nodes/if.h"
#include "nodes/elseif.h"
#include "nodes/else.h"
#include "nodes/while.h"
#include "nodes/return.h"
#include "nodes/try.h"
#include "nodes/catch.h"
#include "nodes/throw.h"
#include "nodes/print.h"
#include "nodes/super.h"
#include "nodes/this.h"
#include "nodes/continue.h"
#include "nodes/break.h"


#endif