#ifndef PARSER_AST_NODE__ELSE_IF_H
#define PARSER_AST_NODE__ELSE_IF_H

#include "Parser/parser.h"
#include "Parser/node.h"

#include <string>
#include <Helpers/string.h>
#include <Parser/parser.h>

#define PARSER_ELSEIF_PREFIX ":? "

using namespace std;

namespace Cake_Parser {
    class Parser;

    class ElseIf_Node : public Node {
    public:
        static bool parse(string content, int &i, int &context, int indentationLevel, Node *parent, Parser *parser);
    };
}


#endif