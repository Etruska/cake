#ifndef PARSER_AST_NODE__EXPRESSION_H
#define PARSER_AST_NODE__EXPRESSION_H

#include "Parser/parser.h"
#include "Parser/node.h"

#include <string>
#include <Helpers/string.h>
#include <Parser/parser.h>

using namespace std;

#define PARSER_EXPRESSION_MATCHER_HELPER(_name_, _regex_) static bool _name_(string &raw, string &parsed) { return basicMatch( _regex_, raw, parsed); }

namespace Cake_Parser {
    class Parser;
    class Expression_Node : public Node {
    public:
        static bool parse(string content, int &i, int &context, int indentationLevel, Node *parent, Parser *parser);

        static void parseExpression(string &raw, Node *parent, unsigned int level);

        static void parseSubExpression(string &raw, Node *parent, unsigned int level);

        static bool basicMatch(string regexString, string &raw, string &parsed);

        static unsigned int decodeTypeFromContent(const string &content);

        PARSER_EXPRESSION_MATCHER_HELPER(matchSymbol, "^([a-zA-Z]+[a-zA-Z0-9]*[\\?\\!]{0,1})(.*)$")

        PARSER_EXPRESSION_MATCHER_HELPER(matchSpecialSymbol,
                                         "^(<=|>=|==|!=|!!|\\|\\|&&|<|\\^|>|!|=|\\+|-|\\*|%|/|\\.|@|,|\\(|\\)|\\||&)(.*)$")

        PARSER_EXPRESSION_MATCHER_HELPER(matchString, "^\\\"(.*)\\\"(.*)$")

        PARSER_EXPRESSION_MATCHER_HELPER(matchInteger, "^([0-9]+)(.*)$")

        PARSER_EXPRESSION_MATCHER_HELPER(matchFloat, "^([0-9]+\\.[0-9]+)(.*)$")

    };
}


#endif