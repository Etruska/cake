#ifndef PARSER_AST_NODE__THROW_H
#define PARSER_AST_NODE__THROW_H

#include "Parser/parser.h"
#include "Parser/node.h"

#include <string>
#include <Helpers/string.h>
#include <Parser/parser.h>

#define PARSER_THROW_PREFIX "<! "

using namespace std;

namespace Cake_Parser {
    class Parser;

    class Throw_Node : public Node {
    public:
        static bool parse(string content, int &i, int &context, int indentationLevel, Node *parent, Parser *parser);
    };
}


#endif