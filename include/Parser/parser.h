#ifndef PARSER_PARSER_H
#define PARSER_PARSER_H

#include <string>
#include <vector>

#define INDENT_CHAR "\t"
#define INDENT_SEQENCE_LENGTH 1

#define EMPTY_LINE -1
#define INVALID_INDENTATION -2

#define CONTEXT_EMPTY       0b00000000
#define CONTEXT_CLASS       0b00000001
#define CONTEXT_METHOD      0b00000010

#define CONTEXT_AFTER_IF    0b00000110
#define CONTEXT_AFTER_TRY   0b00001010

#define INVALID_LINE -2

#include "node.h"

using namespace std;

class IndentedLine {
public:
    int indetation = 0;
    int lineNumber = 0;
    string content = "";

    IndentedLine() { }

    IndentedLine(string line, int lineNumber = 0) {
        size_t found = line.find_first_not_of(INDENT_CHAR);

        // Get indentation
        if (found == string::npos) {
            indetation = EMPTY_LINE;
        } else {
            indetation =
                    (((int) found) % INDENT_SEQENCE_LENGTH)
                    ? INVALID_INDENTATION
                    : (((int) found) / INDENT_SEQENCE_LENGTH);
        }

        // Remove indentation from line and save as content
        if (indetation >= 0) {
            content = line.substr(found);
        }

        //cout << found << "|" << content << endl;

        this->lineNumber = lineNumber;
    }

    void dump() {
        cout << "line: " << lineNumber << ", indented: " << indetation << ", content: " << content << endl;
    }
};

namespace Cake_Parser {
    class Parser {
    protected:
        vector<IndentedLine> lines;

        void loadString(string content);
        public:
                 Parser();
                 ~Parser();

        Node *parseString(string input);

        Node *parse();

        void coutWithIndent(string message, int indentation, string prefix = "");

        int parseLinesBlock(int startLine, unsigned int indentationLevel, Node *parent, int context);
    };
}

#endif