#ifndef CAKE__CONFIG__H
#define CAKE__CONFIG__H

#include "Helpers/settings.h"

namespace Config {
    extern const unsigned int S__DEBUG;
    extern const bool S__COLORS;
}

#endif