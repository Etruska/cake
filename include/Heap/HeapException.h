#ifndef HEAP_HEAPEXCEPTION_H
#define HEAP_HEAPEXCEPTION_H

namespace Heap {
    class HeapException {
    public:
        enum Heap {
            PROP_INDEX_OUT_OF_RANGE = 1,
            CALLING_METHOD_ON_PROXY = 2,
            TRYING_TO_ACCESS_ALREADY_RELEASED_MEMORY = 3
        };
        enum GC {
            INSUFFICIENT_MEMORY = 4,
            INSUFFICIENT_MEMORY__GC_ACTIVATED_IN_GC = 5
        };
    };
}

#endif
