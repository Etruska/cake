//
// Created by Vojtech Petrus on 12/04/15.
//

#ifndef CAKE_HEAPMANAGER_H
#define CAKE_HEAPMANAGER_H

#include <VM/VMObjectProxy.h>
#include "VM/VMClass.h"
#include "VM/VMObjectRef.h"

using namespace VM;

typedef unsigned char byte;

#define GC__INACTIVE    0
#define GC__ACTIVE      1

#define GC__POOL_LEFT    0
#define GC__POOL_RIGHT   1

namespace VM{
    class VMObjectRef;
    class VMClass;
    class VMObjectProxy;
}
namespace Heap {
    class HeapManager {
    public:

        HeapManager(unsigned int poolSize, bool resetGcEnabled = true);
        ~HeapManager();

        VMObjectRef*        heapCloneObject(VMObjectRef*);

        VMObjectRef*        createRawObject(VMClass *classRef);
        Integer*            createRawInteger(const int value);
        VMString*           createRawString(const string content);

        VMObjectProxy* createObject(VMClass *classRef);
        VMObjectProxy* createInteger(const int value);
        VMObjectProxy* createString(const string content);

        VMObjectProxy *getProperty(VMObjectRef *objRef, int propertyIndex);

        void setProperty(VMObjectRef *objRef, int propertyIndex, VMObject *property);

        void runGarbageCollector(unsigned int queriedMemorySize = 0);
        int getMaxMemory();
        int getMemoryUsage();
        VMObjectProxy* createProxyFor(VMObject* instance);
        byte* allocateMemory(unsigned int size);
        void dump();
        void dumpRef(VMObjectRef* ref);

        VMObject** getPropertyPointer(VMObjectRef* ref, int propertyIndex, bool mute = false);

        void resetGcFlags();

    protected:
        int getPropertiesCount(VMClass *);

        unsigned int poolSize;
        unsigned int poolBytesAllocated;

        unsigned int gcStatus;
        unsigned int currentPool;

        byte* leftPool;
        byte* rightPool;

        byte* poolCursor;


        VMObjectProxy* lastProxyObject;
        void clearPool(byte* pool);
        bool resetGcEnabled;
    };
}

#endif //CAKE_HEAPMANAGER_H
