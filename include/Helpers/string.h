#ifndef HELPERS_STRING_H
#define HELPERS_STRING_H

#include <string>
#include <iostream>
#include <vector>
#include <algorithm>
#define HELPERS_STRING__WHITESPACE_CHARACTERS " \t\f\v\n\r"

namespace Cake_Helpers {
    class String {
    public:
        static inline std::string extactDelimitedPrefix(std::string& stream, const std::string delimiter, bool & matched) {
            std::string::size_type pos = 0;
            std::string prefix = "";

            pos = stream.find(delimiter);
            if (pos != std::string::npos) {
                prefix = stream.substr(0, pos);
                stream.erase(0, pos);
                matched = true;
            } else {
                matched = false;
            }

            return prefix;
        }

        static inline std::vector<std::string> splitWithWhitespaces(const std::string &stream,
                                                                    const std::string delimiters = HELPERS_STRING__WHITESPACE_CHARACTERS) {
            std::string::size_type pos, lastPos = 0;

            std::vector<std::string> result;

            while (true) {
                pos = stream.find_first_of(delimiters, lastPos);
                if (pos == std::string::npos) {
                    pos = stream.length();

                    if (pos != lastPos) {
                        result.push_back(stream.substr(lastPos, pos - lastPos));
                    }

                    break;
                } else {
                    if (pos != lastPos) {
                        result.push_back(stream.substr(lastPos, pos - lastPos));
                    }
                }

                lastPos = pos + 1;
            }

            return result;
        }

        static inline bool isPrefixOfString(const std::string& input, const std::string& prefix ) {
            return input.substr(0, prefix.size()).compare(prefix) == 0;
        }

        static inline bool matchAndRemovePrefix(std::string& input, const std::string& prefix ) {
            if (String::isPrefixOfString(input, prefix)) {
                input.erase(0, prefix.length());

                return true;
            } else {
                return false;
            }
        }

        static inline void removeWhitespaces(std::string& input, const char* trimChars = HELPERS_STRING__WHITESPACE_CHARACTERS) {
            input.erase(std::remove_if(input.begin(),
                    input.end(),
                    [](char x){return std::isspace(x);}),
                    input.end()
            );
        }

        static inline std::string& rtrim(std::string& input, const char* trimChars = HELPERS_STRING__WHITESPACE_CHARACTERS) {
            input.erase(input.find_last_not_of(trimChars) + 1);
            return input;
        }

        static inline std::string& ltrim(std::string& input, const char* trimChars = HELPERS_STRING__WHITESPACE_CHARACTERS) {
            input.erase(0, input.find_first_not_of(trimChars));
            return input;
        }

        static inline std::string& trim(std::string& input, const char* trimChars = HELPERS_STRING__WHITESPACE_CHARACTERS) {
            return ltrim(rtrim(input, trimChars), trimChars);
        }
    };
}

#endif