#ifndef CAKE__BYTECODE_HELPERS__H
#define CAKE__BYTECODE_HELPERS__H

#include "Configuration/config.h"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////// COMPILER
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#define BYTE (unsigned char)

#define CONST_POOL__PUSH(__TYPE__, __CONTENT__) \
    compiler->current += 2;\
    byteCode << BYTE IS::PUSH_CONST << BYTE compiler->constantPool->add(new __TYPE__(__CONTENT__));\
    if (IS_DEBUG(DEBUG__COMPILER)) {\
        cout << "IS::PUSH_CONST | " << compiler->constantPool->add(new __TYPE__(__CONTENT__)) << endl;\
    }

#define CONST_POOL__INT(__NUMBER__)     CONST_POOL__PUSH(Integer,  __NUMBER__ )
#define CONST_POOL__STRING(__CONTENT__) CONST_POOL__PUSH(VMString, __CONTENT__)



#define INST__OPERATOR(__TYPE__) \
    compiler->current++;\
    byteCode << BYTE __TYPE__;\
    if (IS_DEBUG(DEBUG__COMPILER)) {\
        cout << "IS::OPERATOR | " << typeToString(__TYPE__) << endl;\
    }

#define BC__PRINT() \
    compiler->current++;\
    byteCode << BYTE VM::IS::D_PRINT ;\
    if (IS_DEBUG(DEBUG__COMPILER)) {\
        cout << "IS::D_PRINT" << endl;\
    }

#define BC__IF() \
    byteCode << BYTE VM::IS::IF_CMPR ;\
    if (IS_DEBUG(DEBUG__COMPILER)) {\
        cout << "IS::IF_CMPR" << endl;\
    }

#define BC__GOTO() \
    byteCode << BYTE VM::IS::GOTO ;\
    if (IS_DEBUG(DEBUG__COMPILER)) {\
        cout << "IS::GOTO" << endl;\
    }

#define CONST_POOL__GET_SYMBOL(__TYPE__, __CONTENT__) \
    compiler->current++;\
    byteCode << BYTE compiler->constantPool->add(new __TYPE__(__CONTENT__));\

#define CONST_POOL__GET_STRING(__CONTENT__) CONST_POOL__GET_SYMBOL(VMString, __CONTENT__)
#define CONST_POOL__GET_INT(__CONTENT__) CONST_POOL__GET_SYMBOL(Integer, __CONTENT__)

#define LOAD(__NAME__) \
    compiler->current += 2;\
    byteCode << BYTE VM__INSTRUCTION__LOAD << BYTE compiler->currentMethod->localsMap.getId(__NAME__);\

#define LOAD_THIS() LOAD("@")

#define GET_FIELD(__NAME__) \
    compiler->current += 2;\
    byteCode << BYTE VM__INSTRUCTION__GETFIELD << BYTE compiler->currentClass->propertiesMap.getId(__NAME__);\

#define SET_FIELD(__NAME__) \
    compiler->current += 2;\
    byteCode << BYTE VM__INSTRUCTION__SETFIELD << BYTE compiler->currentClass->propertiesMap.getId(__NAME__);\

#define STORE(__NAME__) \
    compiler->current += 2;\
    byteCode << BYTE VM__INSTRUCTION__STORE << BYTE compiler->currentMethod->localsMap.getId(__NAME__);\

#define INVOKE(__NAME__) \
    compiler->current += 2;\
    byteCode << BYTE VM__INSTRUCTION__INVOKE << BYTE compiler->methodsMap.getId(__NAME__);\


#define NEW(__NAME__) \
    compiler->current += 2;\
    byteCode << BYTE VM__INSTRUCTION__NEW << BYTE compiler->classPool->getIndexOf(new VMClass(__NAME__, NULL));\

#define RETURN() \
    compiler->current += 1;\
    byteCode << BYTE VM__INSTRUCTION__RETURN;\


#define NEGATE() \
    compiler->current += 1;\
    byteCode << BYTE VM__INSTRUCTION__OP__NEGATE;\


#endif



