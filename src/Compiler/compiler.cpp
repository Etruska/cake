#include "Compiler/compiler.h"

namespace Cake_Compiler {
    Compiler::Compiler() {
        constantPool = new VM::ConstantPool();
        classPool = new VM::ClassPool<VM::VMClass*>();
    }

    void Compiler::PUSH__IDX(Cake_Compiler::Compiler *compiler, stringstream &byteCode, unsigned short value) {
        unsigned char *it = (unsigned char *) &value;
        it++;
        byteCode << *it;
        it--;
        byteCode << *it;
    }

    void Compiler::RESERVE__PUSH__IDX_WITH_OP(Cake_Compiler::Compiler *compiler) {
        compiler->current += 3;
    }
}