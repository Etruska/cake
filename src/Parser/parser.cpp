
#include <iostream>

using namespace std;


#include "Parser/parser.h"

#include "Helpers/string.h"

namespace Cake_Parser {

    Parser::~Parser() {
    }

    Parser::Parser() {
    }

    void Parser::loadString(string content) {
        vector<string> rawLines = Cake_Helpers::String::splitWithWhitespaces(content, "\n");
        IndentedLine indentedLine;

        int lineCounter = 0;

        for (string &line : rawLines) {
            lineCounter++;
            indentedLine = IndentedLine(line, lineCounter);
            lines.push_back(indentedLine);
        }
    }

    Node *Parser::parseString(string content) {
        loadString(content);
        return parse();
    }

    void Parser::coutWithIndent(string message, int indentation, string prefix) {
        for (int i = 0; i < indentation; i++) {
            cout << "\t";
        }

        cout << prefix << message << endl;
    }

    Node *Parser::parse() {
        Node *root = new Node(ROOT_NODE);

        int lastLine = parseLinesBlock(0, 0, root, CONTEXT_EMPTY);

        if (lastLine != lines.size() - 1) {
            cout << "INCOMPLETE PARSE" << endl;
        }

        return root;
    }

#ifndef PARSER_NODE_HELPER
#define PARSER_NODE_HELPER(_name_) if (_name_::parse(line.content, i, context, indentationLevel, parent, this)) { continue; }
#endif

    int Parser::parseLinesBlock(int startLine, unsigned int indentationLevel, Node *parent, int context) {
        IndentedLine line;

        for (int i = startLine; i < lines.size(); i++) {
            line = lines[i];

            // Skip comments
            if (Cake_Helpers::String::isPrefixOfString(line.content, "#") || line.indetation == EMPTY_LINE) {
                continue;
            }

            // break from recursion
            if (line.indetation < indentationLevel) {
                return i - 1;
            }

            // validate
            if (line.indetation != indentationLevel || line.indetation == INVALID_INDENTATION) {
                cout << "error: invalid indentation on line " << line.lineNumber << " with " << line.content << endl;
                return INVALID_LINE;
            }

            // CLASS
            PARSER_NODE_HELPER(Class_Node)
            // METHOD
            PARSER_NODE_HELPER(Method_Node)

            // PROCEDURAL
            if (context & CONTEXT_METHOD) {

                // IF
                PARSER_NODE_HELPER(If_Node)
                if (context == CONTEXT_AFTER_IF) {
                    PARSER_NODE_HELPER(ElseIf_Node)
                    PARSER_NODE_HELPER(Else_Node)

                    // If after IF no else or elseif block reset context
                    context = CONTEXT_METHOD;
                }

                PARSER_NODE_HELPER(While_Node)


                // TRY
                PARSER_NODE_HELPER(Try_Node)
                if (context == CONTEXT_AFTER_TRY) {
                    PARSER_NODE_HELPER(Catch_Node)

                    context = CONTEXT_METHOD;
                }

                // Special methods
                PARSER_NODE_HELPER(Throw_Node)
                PARSER_NODE_HELPER(Return_Node)
                PARSER_NODE_HELPER(Print_Node)
                PARSER_NODE_HELPER(This_Node)
                PARSER_NODE_HELPER(Super_Node)

                PARSER_NODE_HELPER(Break_Node)
                PARSER_NODE_HELPER(Continue_Node)
            }

            PARSER_NODE_HELPER(Expression_Node)
        }

        return (int) lines.size() - 1;
    }
}