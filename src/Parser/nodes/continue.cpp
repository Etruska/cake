#include "Parser/nodes/continue.h"

using namespace std;

bool Cake_Parser::Continue_Node::parse(string content, int &i, int &context, int indentationLevel, Node *parent,
                                       Parser *parser) {

    string innerContent = content;

    if (!Cake_Helpers::String::matchAndRemovePrefix(innerContent, PARSER_CONTINUE_PREFIX)) {
        return false;
    }

    if (context & CONTEXT_METHOD) {
        Node *node = new Node(NODE_CONTINUE, parent);
    }

    return true;
}