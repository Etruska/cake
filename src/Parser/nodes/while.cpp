#include "Parser/nodes/while.h"

using namespace std;

bool Cake_Parser::While_Node::parse(string content, int &i, int &context, int indentationLevel, Node *parent,
                                    Parser *parser) {

    string innerContent = content;

    if (!Cake_Helpers::String::matchAndRemovePrefix(innerContent, PARSER_WHILE_PREFIX)) {
        return false;
    }

    if (context & CONTEXT_METHOD) {
        Node *node = new Node(NODE_WHILE, parent);

        Expression_Node::parseExpression(innerContent, node, indentationLevel + 1);

        // Content wrap
        Node *wrap = new Node(EXPRESSION_NODE_BLOCK, node);

        i = parser->parseLinesBlock(i + 1, indentationLevel + 1, wrap, context);
    }

    return true;
}