#include "Parser/nodes/class.h"

using namespace std;

bool Cake_Parser::Class_Node::parse(string content, int &i, int &context, int indentationLevel, Node *parent,
                                    Parser *parser) {

    string innerContent = content;

    if (!Cake_Helpers::String::matchAndRemovePrefix(innerContent, PARSER_CLASS_PREFIX)) {
        return false;
    }

    if (context == CONTEXT_EMPTY) {
        Node *node = new Node(NODE_CLASS, parent);

        Expression_Node::parseExpression(innerContent, node, indentationLevel + 1);

        i = parser->parseLinesBlock(i + 1, indentationLevel + 1, node, CONTEXT_CLASS);
    }

    return true;
}