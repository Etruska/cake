#include "Parser/nodes/expression.h"

#include <regex>

using namespace std;

bool Cake_Parser::Expression_Node::parse(string content, int &i, int &context, int indentationLevel, Node* parent, Parser* parser)
{
    Cake_Parser::Expression_Node::parseExpression(content, parent, (unsigned int) indentationLevel);

    return true;
}

bool Cake_Parser::Expression_Node::basicMatch(string regexString, string &raw, string &parsed)
{

    smatch m;
    regex_search(raw, m, regex(regexString));

    if (m.empty()) {
        return false;
    }

    parsed = m[1];
    raw = m[2];

    raw = Cake_Helpers::String::ltrim(raw);

    return true;
}


#define CONVERT_NODE(__FROM__, __TO__) case __FROM__: {child->type = __TO__; break;}
void Cake_Parser::Expression_Node::parseExpression(string &raw, Node* parent, unsigned int level)
{
    Node* expression = new Node(EXPRESSION_NODE_BLOCK, parent);
    Cake_Parser::Expression_Node::parseSubExpression(
            raw,
            expression,
            level
    );


    // convert @ => this
    // convert ^ => super
    for (Node* child : expression->children) {
        switch (child->type) {
            CONVERT_NODE(SPEC_SYM__AT, NODE_THIS)
            CONVERT_NODE(SPEC_SYM__ROOFTOP, NODE_SUPER)
        }
    }
}

#define STRING_SWITCH(__symbol__) ;} else if (!content.compare(__symbol__)) { return


unsigned int Cake_Parser::Expression_Node::decodeTypeFromContent(const string &content) {

    if (false) {
        STRING_SWITCH(";")  SPEC_SYM__SEMICOLON
        STRING_SWITCH(",")  SPEC_SYM__COMMA
        STRING_SWITCH("=")  SPEC_SYM__ASSIGN
        STRING_SWITCH("(")  SPEC_SYM__PARENTHESES_OPEN
        STRING_SWITCH(")")  SPEC_SYM__PARENTHESES_CLOSE
        STRING_SWITCH("{")  SPEC_SYM__CURLY_OPEN
        STRING_SWITCH("}")  SPEC_SYM__CURLY_CLOSE
        STRING_SWITCH("[")  SPEC_SYM__BRAKETS_OPEN
        STRING_SWITCH("]")  SPEC_SYM__BRAKETS_CLOSE
        STRING_SWITCH("<")  SPEC_SYM__CHEVRON_OPEN
        STRING_SWITCH(">")  SPEC_SYM__CHEVRON_CLOSE
        STRING_SWITCH("\"") SPEC_SYM__DOUBLE_QUOTES
        STRING_SWITCH("\\") SPEC_SYM__BACKSLASH
        STRING_SWITCH("/")  SPEC_SYM__SLASH
        STRING_SWITCH("*")  SPEC_SYM__TIMES
        STRING_SWITCH(".")  SPEC_SYM__DOT
        STRING_SWITCH("|")  SPEC_SYM__PIPE
        STRING_SWITCH("@")  NODE_THIS
        //STRING_SWITCH("@")  SPEC_SYM__AT
        STRING_SWITCH("#")  SPEC_SYM__HASH
        //STRING_SWITCH("^")  SPEC_SYM__ROOFTOP
        STRING_SWITCH("^")  NODE_SUPER
        STRING_SWITCH("&")  SPEC_SYM__BINARY_AND
        STRING_SWITCH("%")  SPEC_SYM__PERCENT
        STRING_SWITCH("-")  SPEC_SYM__MINUS
        STRING_SWITCH("+")  SPEC_SYM__PLUS
        STRING_SWITCH("!")  SPEC_SYM__EXCLAMATION_MARK
        STRING_SWITCH("?")  SPEC_SYM__QUESTION_MARK
        STRING_SWITCH(" ")  SPEC_SYM__WHITESPACE

        STRING_SWITCH("==")  SPEC_SYM__EQUAL
        STRING_SWITCH("!=")  SPEC_SYM__NOT_EQUAL
        STRING_SWITCH("<=")  SPEC_SYM__LESS_OR_EQUAL
        STRING_SWITCH(">=")  SPEC_SYM__GREATER_OR_EQUAL

        STRING_SWITCH("&&")  SPEC_SYM__AND
        STRING_SWITCH("||")  SPEC_SYM__OR
    ;}


    return EXPRESSION_NODE_SPECIAL_SYMBOL;
}

void Cake_Parser::Expression_Node::parseSubExpression(string &raw, Node* parent, unsigned int level)
{
    string parsed;
    unsigned int type;


    while (raw.length()) {
        raw = Cake_Helpers::String::ltrim(raw);

        if (matchSymbol(raw, parsed)) {
            parent->addChild(EXPRESSION_NODE_SYMBOL, parsed);
            continue;
        }

        if (matchSpecialSymbol(raw, parsed)) {
            if (parsed.length()) {
                type = decodeTypeFromContent(parsed);

                switch(type) {

                    case SPEC_SYM__PARENTHESES_OPEN:
                    case SPEC_SYM__BRAKETS_OPEN:
                    case SPEC_SYM__CURLY_OPEN:
                        Cake_Parser::Expression_Node::parseSubExpression(
                                raw,
                                parent->addChild(type, parsed),
                                level + 1
                        );
                        break;

                    case SPEC_SYM__PARENTHESES_CLOSE:
                    case SPEC_SYM__BRAKETS_CLOSE:
                    case SPEC_SYM__CURLY_CLOSE:
                        return;

                    default:
                        parent->addChild(type, parsed);

                }
            }
            continue;
        }

        if (matchString(raw, parsed)) {
            parent->addChild(EXPRESSION_NODE_STRING, parsed);
            continue;
        }

        if (matchFloat(raw, parsed)) {
            parent->addChild(EXPRESSION_NODE_FLOAT, parsed);
            continue;
        }

        if (matchInteger(raw, parsed)) {
            parent->addChild(EXPRESSION_NODE_INTEGER, parsed);
            continue;
        }

        cout << "ERROR - FAILED TO PARSE EXPRESSION: " << raw << endl;
        exit(1);
    }
}