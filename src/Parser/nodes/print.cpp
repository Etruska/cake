#include "Parser/nodes/print.h"

using namespace std;

bool Cake_Parser::Print_Node::parse(string content, int &i, int &context, int indentationLevel, Node *parent,
                                    Parser *parser) {

    string innerContent = content;

    if (!Cake_Helpers::String::matchAndRemovePrefix(innerContent, PARSER_PRINT_PREFIX)) {
        return false;
    }

    if (context & CONTEXT_METHOD) {
        Node *node = new Node(NODE_PRINT, parent);

        Expression_Node::parseExpression(innerContent, node, indentationLevel + 1);
    }

    return true;
}