#include "Parser/nodes/method.h"

using namespace std;

bool Cake_Parser::Method_Node::parse(string content, int &i, int &context, int indentationLevel, Node *parent,
                                     Parser *parser) {

    string innerContent = content;

    if (!Cake_Helpers::String::matchAndRemovePrefix(innerContent, PARSER_METHOD_PREFIX)) {
        return false;
    }

    if (context == CONTEXT_CLASS) {
        Node *node = new Node(NODE_METHOD, parent);

        Expression_Node::parseExpression(innerContent, node, indentationLevel + 1);

        Node *body = new Node(EXPRESSION_NODE_BLOCK, node);

        i = parser->parseLinesBlock(i + 1, indentationLevel + 1, body, CONTEXT_METHOD);
    }

    return true;
}