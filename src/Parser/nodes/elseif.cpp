#include "Parser/nodes/elseif.h"

using namespace std;

bool Cake_Parser::ElseIf_Node::parse(string content, int &i, int &context, int indentationLevel, Node *parent,
                                     Parser *parser) {

    string innerContent = content;

    if (!Cake_Helpers::String::matchAndRemovePrefix(innerContent, PARSER_ELSEIF_PREFIX)) {
        return false;
    }

    if (context & CONTEXT_METHOD) {
        Node *node = new Node(NODE_ELSEIF, parent);

        Expression_Node::parseExpression(innerContent, node, indentationLevel + 1);

        Node *wrap = new Node(EXPRESSION_NODE_BLOCK, node);

        i = parser->parseLinesBlock(i + 1, indentationLevel + 1, wrap, CONTEXT_METHOD);
    }

    return true;
}