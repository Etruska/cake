#include "Parser/nodes/catch.h"

using namespace std;

bool Cake_Parser::Catch_Node::parse(string content, int &i, int &context, int indentationLevel, Node *parent,
                                    Parser *parser) {

    string innerContent = content;

    if (!Cake_Helpers::String::matchAndRemovePrefix(innerContent, PARSER_CATCH_PREFIX)) {
        return false;
    }

    if (context & CONTEXT_METHOD) {
        Node *node = new Node(NODE_CATCH, parent);

        Expression_Node::parseExpression(innerContent, node, indentationLevel + 1);

        i = parser->parseLinesBlock(i + 1, indentationLevel + 1, node, CONTEXT_METHOD);
    }

    return true;
}