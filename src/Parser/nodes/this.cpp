#include "Parser/nodes/this.h"

using namespace std;

bool Cake_Parser::This_Node::parse(string content, int &i, int &context, int indentationLevel, Node *parent,
                                   Parser *parser) {

    string innerContent = content;

    if (!Cake_Helpers::String::matchAndRemovePrefix(innerContent, PARSER_THIS_PREFIX)) {
        return false;
    }

    if (context & CONTEXT_METHOD) {
        Node *wrapNode = new Node(EXPRESSION_NODE_BLOCK, parent);

        new Node(NODE_THIS, wrapNode);

        Expression_Node::parseSubExpression(innerContent, wrapNode, indentationLevel);
    }

    return true;
}