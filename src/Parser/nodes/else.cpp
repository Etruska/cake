#include "Parser/nodes/else.h"

using namespace std;

bool Cake_Parser::Else_Node::parse(string content, int &i, int &context, int indentationLevel, Node *parent,
                                   Parser *parser) {

    string innerContent = content;

    if (!Cake_Helpers::String::matchAndRemovePrefix(innerContent, PARSER_ELSE_PREFIX)) {
        return false;
    }

    if (context & CONTEXT_METHOD) {
        Node *node = new Node(NODE_ELSE, parent);

        i = parser->parseLinesBlock(i + 1, indentationLevel + 1, node, CONTEXT_METHOD);

        context = CONTEXT_METHOD;
    }

    return true;
}