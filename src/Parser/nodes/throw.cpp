#include "Parser/nodes/throw.h"

using namespace std;

bool Cake_Parser::Throw_Node::parse(string content, int &i, int &context, int indentationLevel, Node *parent,
                                    Parser *parser) {

    string innerContent = content;

    if (!Cake_Helpers::String::matchAndRemovePrefix(innerContent, PARSER_THROW_PREFIX)) {
        return false;
    }

    if (context & CONTEXT_METHOD) {
        Node *node = new Node(NODE_THROW, parent);

        Expression_Node::parseExpression(innerContent, node, indentationLevel + 1);
    }

    return true;
}