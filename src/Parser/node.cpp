#include "Parser/node.h"

#include <algorithm>

#include <bitset>

#define SHOW_ADRRESSES false

bool Cake_Parser::Node::parse(string content, int &i, int &context, int indentationLevel, Node *parent,
                              Parser *parser) {
    return false;
}

void Cake_Parser::Node::dump(string prefix) {
#if SHOW_ADRRESSES
        cout << prefix << typeToString(type) << " :: " << content << " @" << (unsigned long)this << endl;
    #else
    cout << prefix << typeToString(type) << " :: " << content << endl;
#endif
}

#include <sstream>

string Cake_Parser::Node::dumpContent() {
    stringstream s;

    if (type == EXPRESSION_NODE_STRING) {
        s << "\"" << content << "\"";
    } else {
        s << content;

        for (Node *child : children) {
            s << child->dumpContent();
        }

        switch (type) {
            case SPEC_SYM__PARENTHESES_CLOSE:
                s << ")";
            case SPEC_SYM__CURLY_CLOSE:
                s << "}";
            case SPEC_SYM__BRAKETS_CLOSE:
                s << "]";
            case SPEC_SYM__CHEVRON_CLOSE:
                s << ">";
            default:
                break;
        }
    }

    return s.str();
}

void Cake_Parser::Node::dumpTree(string prefix, bool isLast, Node *highlight) {
    if (highlight == this) {
        cout << prefix << (isLast ? "╚ " : "╠ ");
    } else {
        cout << prefix << (isLast ? "└ " : "├ ");
    }

    this->dump();

    prefix += (isLast ? "  " : "│ ");

    for (Node *child : children) {
        if (child == children.back()) {
            child->dumpTree(prefix, true, highlight);

            if (!isLast) {
                cout << prefix << endl;
            }

        } else {
            child->dumpTree(prefix, false, highlight);
        }
    }
}


void Cake_Parser::Node::dumpFromParent() {
    parent->dumpTree("", true, this);
}

void Cake_Parser::Node::dumpWholeTree(Cake_Parser::Node *highlight) {
    if (!parent) {
        dumpTree("", true, highlight);
        return;
    }

    parent->dumpWholeTree(highlight);
}

void Cake_Parser::Node::addChild(Node *child) {
    children.push_back(child);
    child->parent = this;
}


Cake_Parser::Node *Cake_Parser::Node::addChild(unsigned int type, string content) {

    Node *child = new Node(type, this);
    child->content = content;

    return child;
}

Cake_Parser::Node *Cake_Parser::Node::lastChild() {
    if (children.empty()) {
        return NULL;
    }

    return children.back();
}

Cake_Parser::Node *Cake_Parser::Node::firstChild() {
    if (children.empty()) {
        return NULL;
    }

    return children[0];
}


string Cake_Parser::Node::childsContent(unsigned int id) {
    return children[id]->content;
}


bool Cake_Parser::Node::is(unsigned int type, const string content) {

    if (!content.compare(IGNORE_NODE_CONTENT)) {
        return this->type == type;
    } else {
        return this->type == type && !this->content.compare(content);
    }
}

bool Cake_Parser::Node::isLike(unsigned int type, unsigned int mask) {
//     cout << "~~~~~~~~~~~~~~~~~~~~~~~" << endl;
//     cout << bitset<32>(this->type) << endl;
//     cout << bitset<32>(type) << endl;
//     cout << bitset<32>(~mask) << endl;
    //
    return (bool) (this->type & type & ~mask);
}


bool Cake_Parser::Node::hasChildren(unsigned int count) {
    return children.size() == count;
}

bool Cake_Parser::Node::hasNoChildren() {
    return children.empty();
}


bool Cake_Parser::Node::childIs(unsigned int type, unsigned int const id, const string content) {
    return children[id]->is(type, content);
}

Cake_Parser::Node *Cake_Parser::Node::nextSibling(Node *child) {
    bool found = false;
    for (Node *node : children) {
        if (found) {
            return node;
        }

        if (node == child) {
            found = true;
        }
    }

    if (!found) {
        cout << "nextSibling: NOT MY CHILD" << endl;

        exit(1);
    } else {
        // child was last child
        return NULL;
    }
}

Cake_Parser::Node *Cake_Parser::Node::prevSibling(Node *child) {
    Node *previous = NULL;

    for (Node *node : children) {
        if (node == child) {
            return previous;
        }

        previous = node;
    }

    cout << "nextSibling: NOT MY CHILD" << endl;

    exit(1);
}

Cake_Parser::Node *Cake_Parser::Node::next() {
    Node *nodeNext = parent->nextSibling(this);
    return parent->nextSibling(this);
}

Cake_Parser::Node *Cake_Parser::Node::prev() {
    return parent->prevSibling(this);
}

Cake_Parser::Node *Cake_Parser::Node::self() {
    return this;
}


bool Cake_Parser::Node::nextIs(unsigned int type, string content) {
    return nodeIs(next(), type, content);
}

bool Cake_Parser::Node::prevIs(unsigned int type, string content) {
    return nodeIs(prev(), type, content);
}

bool Cake_Parser::Node::nodeIs(Node *node, unsigned int type, string content) {
    return (bool) (node) && node->is(type, content);
}


string Cake_Parser::Node::typeToString(const unsigned int &type) {
    switch (type) {
        case ROOT_NODE:
            return "ROOT";

            // Expression
        case EXPRESSION_NODE_BLOCK:
            return "EXPRESSION";
        case EXPRESSION_NODE_SYMBOL:
            return "SYMBOL";
        case EXPRESSION_NODE_SPECIAL_SYMBOL:
            return "SPEC SYM";
        case EXPRESSION_NODE_STRING:
            return "STRING";
        case EXPRESSION_NODE_INTEGER:
            return "INTEGER";
        case EXPRESSION_NODE_FLOAT:
            return "FLOAT";

            // Logic
        case NODE_CLASS:
            return "CLASS";
        case NODE_METHOD:
            return "METHOD";
        case NODE_IF:
            return "IF";
        case NODE_ELSEIF:
            return "ELSEIF";
        case NODE_ELSE:
            return "ELSE";
        case NODE_WHILE:
            return "WHILE";
        case NODE_RETURN:
            return "RETURN";
        case NODE_TRY:
            return "TRY";
        case NODE_CATCH:
            return "CATCH";
        case NODE_THROW:
            return "THROW";
        case NODE_PRINT:
            return "PRINT";
        case NODE_SUPER:
            return "SUPER";
        case NODE_THIS:
            return "THIS";
        case NODE_BREAK:
            return "BREAK";
        case NODE_CONTINUE:
            return "CONTINUE";


            // Special symbols
        case SPEC_SYM__SEMICOLON:
            return "SEMICOLON";
        case SPEC_SYM__COMMA:
            return "COMMA";
        case SPEC_SYM__EQUAL:
            return "EQUAL";
        case SPEC_SYM__PARENTHESES_OPEN:
            return "PARENTHESES_OPEN";
        case SPEC_SYM__PARENTHESES_CLOSE:
            return "PARENTHESES_CLOSE";
        case SPEC_SYM__CURLY_OPEN:
            return "CURLY_OPEN";
        case SPEC_SYM__CURLY_CLOSE:
            return "CURLY_CLOSE";
        case SPEC_SYM__BRAKETS_OPEN:
            return "BRAKETS_OPEN";
        case SPEC_SYM__BRAKETS_CLOSE:
            return "BRAKETS_CLOSE";
        case SPEC_SYM__CHEVRON_OPEN:
            return "CHEVRON_OPEN";
        case SPEC_SYM__CHEVRON_CLOSE:
            return "CHEVRON_CLOSE";
        case SPEC_SYM__DOUBLE_QUOTES:
            return "DOUBLE_QUOTES";
        case SPEC_SYM__BACKSLASH:
            return "BACKSLASH";
        case SPEC_SYM__SLASH:
            return "SLASH";
        case SPEC_SYM__TIMES:
            return "TIMES";
        case SPEC_SYM__DOT:
            return "DOT";
        case SPEC_SYM__PIPE:
            return "PIPE";
        case SPEC_SYM__AT:
            return "AT";
        case SPEC_SYM__HASH:
            return "HASH";
        case SPEC_SYM__ROOFTOP:
            return "ROOFTOP";
        case SPEC_SYM__AND:
            return "AND";
        case SPEC_SYM__PERCENT:
            return "PERCENT";
        case SPEC_SYM__MINUS:
            return "MINUS";
        case SPEC_SYM__PLUS:
            return "PLUS";
        case SPEC_SYM__EXCLAMATION_MARK:
            return "EXCLAMATION_MARK";
        case SPEC_SYM__QUESTION_MARK:
            return "QUESTION_MARK";
        case SPEC_SYM__WHITESPACE:
            return "WHITESPACE";
        case SPEC_SYM__EOT:
            return "EOT";
        case SPEC_SYM__ASSIGN:
            return "ASSIGN";

        default:
            return "UNKNOWN";
    }
}