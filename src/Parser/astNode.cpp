#include "Parser/astNode.h"

namespace Cake_Parser {
    void AstNode::dump() {
        stringstream ss;
        dump(ss);

        cout << ss.str();
    }

    string AstNode::dumpToString() {
        stringstream ss;
        dump(ss);

        return ss.str();
    }


    void AstNode::compile_error(string message) {
        cout << "[ERROR]: " << message << endl;

        exit(1);
    }

}