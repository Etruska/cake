#include "Parser/ast/method.h"
#include "Parser/ast/signal.h"

#include <algorithm>
#include "Helpers/bytecode.h"
#include "VM/IS.h"

namespace Cake_Parser {
    void Method_Ast::dump(stringstream &out) {
        out << "       name: " << name << endl;
        out << "       args: " << endl;
        for (Expression_AstNode *argument : *(arguments)) {
            argument->dump(out, "           ");
        }

        out << "       body: " << endl;

        if (body) {
            body->dump(out, "           ");
        }

        out << endl;
    }


    Method_Ast::Method_Ast() {
        arguments = new vector<Expression_AstNode *>();

        // this local
        localsMap.getId("@");
    }

    Method_Ast *Method_Ast::parse(Node *node) {
        Node *signature = node->firstChild();

        Method_Ast *c_method = new Method_Ast();

        /**
         * [0] -> name
         * -- arguments (optional)
         * [1][0-n] - args
         */
        c_method->name = signature->childsContent(0);

        if (signature->hasChildren(2)) {
            Expression_AstNode::parseParams(signature->children[1], c_method->arguments);
            reverse(c_method->arguments->begin(),c_method->arguments->end());
        }

        c_method->body = Expression_AstNode::parseTree(node->children[1], PARSER_MODE__DONT_UNWRAP_SINGLE);

        return c_method;
    }

    string Method_Ast::compile(Cake_Compiler::Compiler *compiler) {
        stringstream byteCode;
        // reset compiler
        compiler->current = 0;
        compiler->currentMethod = this;


        // init stackFrame with arguments
        for (Expression_AstNode *argument : *(arguments)) {
            STORE(((Signal_Ast*)(argument))->name);
        }

        // pop this
        //STORE("@");

        // parse children
        if (body) {
            byteCode << body->compile(compiler);
        }

        RETURN();

        methodBytecode = new Bytecode(byteCode.str());

        VMMethod* c_method = new VMMethod(this->name, methodBytecode);
        compiler->currentClass->linkedClass->methods.setItem(
                c_method,
                compiler->methodsMap.getId(this->name)
        );

        compiler->currentMethod = NULL;

        return "";
    }

    void Method_Ast::dumpCompiled(Cake_Compiler::Compiler *compiler) {
        cout << "       name: " << name << endl;
        cout << "       args: " << endl;

        localsMap.dump();

        cout << "       body: " << endl;

        if (body) {
            methodBytecode->dump(compiler->constantPool);
        }

        cout << endl;
    }
}