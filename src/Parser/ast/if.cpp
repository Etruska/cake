#include "Parser/ast/if.h"

#include "VM/IS.h"

namespace Cake_Parser {
    If_Ast::If_Ast() {
        condition = NULL;
        body = NULL;
        elseIfs = vector<If_Ast *>();
        elseBody = NULL;

        type = EXPRESSION_TYPE__IF;
    }


    void If_Ast::dump(stringstream &out, string prefix) {
        out << prefix << "IF:" << endl;
        out << prefix << "  Cond:" << endl;

        condition->dump(out, prefix + "    ");

        out << prefix << "  Content:" << endl;

        if (body) {
            body->dump(out, prefix + "    ");
        }

        if (!elseIfs.empty()) {
            out << prefix << "  Else if branches:" << endl;
            for (If_Ast *elseIf : elseIfs) {
                elseIf->dump(out, prefix + "    ");
            }
        }

        if (elseBody) {
            out << prefix << "  Else branch:" << endl;
            elseBody->dump(out, prefix + "    ");
        }
    }

    If_Ast *If_Ast::parse(Node **iterator, unsigned int mode) {
        Node *node = *iterator;
        If_Ast *c_if = new If_Ast();
        c_if->type = node->type;

        if (IS_DEBUG(DEBUG__PARSER)) {
            cout << ":IF---------------------" << endl;
            node->dumpFromParent();
            cout << "---------------------------" << endl;
        }

        node = node->firstChild();
        if (IS_DEBUG(DEBUG__PARSER)) {
            cout << "IF-COND--------------------" << endl;
            node->dumpFromParent();
            cout << "---------------------------" << endl;
        }

        c_if->condition = Expression_AstNode::parseTree(node);

        node = node->next();
        // Content
        if (IS_DEBUG(DEBUG__PARSER)) {
            cout << "IF-CONTENT-----------------" << endl;
            node->dumpFromParent();
            cout << "---------------------------" << endl;
        }

        c_if->body = Expression_AstNode::parseTree(node);

        // Move to next expression
        *iterator = node = (*iterator)->next();

        if (mode == PARSER_IF_MODE__ESLEIF) {
            return c_if;
        }

        // Parse else ifs
        while (node && node->is(NODE_ELSEIF)) {
            if (IS_DEBUG(DEBUG__PARSER)) {
                cout << "ELSE-IF-CONTENT-------------" << endl;
                node->dumpFromParent();
                cout << "---------------------------" << endl;
            }
            c_if->elseIfs.push_back(If_Ast::parse(&node, PARSER_IF_MODE__ESLEIF));
        }

        // Parse else
        if (node && node->is(NODE_ELSE)) {
            if (IS_DEBUG(DEBUG__PARSER)) {
                cout << "ELSE-IF-CONTENT-------------" << endl;
                node->dumpFromParent();
                cout << "---------------------------" << endl;
            }
            c_if->elseBody = Expression_AstNode::parseTree(node);
        }

        *iterator = node;

        return c_if;
    }

    string If_Ast::compile(Cake_Compiler::Compiler *compiler) {
        stringstream byteCode;
        unsigned short endIdx;
        unsigned short elseIdx;
        unsigned short firstElseOrElseIfIdx = 0;

        vector<string> elseIfsBodies;
        vector<string> elseIfsConditions;
        vector<unsigned short> elseIfsIdxes;

        string elseIfBody;
        string elseIfCondition;

        bool hasElseIfs = !elseIfs.empty();
        bool hasElseBody = !!elseBody;

        string bodyByteCode;
        string elseByteCode;

        byteCode << condition->compile(compiler);

        // else idx + IF
        Cake_Compiler::Compiler::RESERVE__PUSH__IDX_WITH_OP(compiler);

        bodyByteCode = body->compile(compiler);

        // RESERVE JUMP TO END
        if (hasElseBody || hasElseIfs) {
            // end idx + GOTO
            Cake_Compiler::Compiler::RESERVE__PUSH__IDX_WITH_OP(compiler);
        }

        for (If_Ast *elseIf : elseIfs) {
            // record first elseif or else waypoint
            if (!firstElseOrElseIfIdx) {
                firstElseOrElseIfIdx = compiler->current;
            }

            // elseif condition waypoint
            elseIfsIdxes.push_back(compiler->current);

            elseIfsConditions.push_back(elseIf->condition->compile(compiler));

            // reserve GOTO next
            Cake_Compiler::Compiler::RESERVE__PUSH__IDX_WITH_OP(compiler);

            elseIfsBodies.push_back(elseIf->body->compile(compiler));

            // reserve goto end
            Cake_Compiler::Compiler::RESERVE__PUSH__IDX_WITH_OP(compiler);
        }


        if (hasElseBody) {

            if (!firstElseOrElseIfIdx) {
                firstElseOrElseIfIdx = compiler->current;
            }

            elseIdx = compiler->current;

            elseByteCode = elseBody->compile(compiler);
        }

        endIdx = compiler->current;

        // COMPOSE

        BC__IF();
        Cake_Compiler::Compiler::PUSH__IDX(compiler, byteCode, firstElseOrElseIfIdx ? firstElseOrElseIfIdx : endIdx);

        byteCode << bodyByteCode;

        if (hasElseBody || hasElseIfs) {
            BC__GOTO();
            Cake_Compiler::Compiler::PUSH__IDX(compiler, byteCode, endIdx);
        }

        unsigned short nextElseOrElseIfIdx = 0;

        for (int iterator = 0; iterator < elseIfs.size(); iterator++) {
            byteCode << elseIfsConditions[iterator];

            if ((iterator + 1) < elseIfs.size()) {
                nextElseOrElseIfIdx = elseIfsIdxes[iterator + 1];
            } else {
                if (hasElseBody) {
                    nextElseOrElseIfIdx = elseIdx;
                } else {
                    nextElseOrElseIfIdx = endIdx;
                }
            }


            BC__IF();
            Cake_Compiler::Compiler::PUSH__IDX(compiler, byteCode, nextElseOrElseIfIdx);

            byteCode << elseIfsBodies[iterator];

            BC__GOTO();
            Cake_Compiler::Compiler::PUSH__IDX(compiler, byteCode, endIdx);
        }

        if (hasElseBody) {
            byteCode << elseByteCode;
        }


        return byteCode.str();
    }
}
