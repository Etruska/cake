#include "Parser/ast/signal.h"

#include <algorithm>

#include <bitset>

#include "Configuration/config.h"

#include "Helpers/bytecode.h"

#include "VM/Integer.h"
#include "VM/VMString.h"
#include "VM/IS.h"

#include "Compiler/exceptions.h"

using namespace VM;

namespace Cake_Parser {
    Signal_Ast::Signal_Ast() {
        parameters = new vector<Expression_AstNode *>();
        type = EXPRESSION_TYPE__SIGNAL;
    }


    unsigned int Signal_Ast::parametersCount() {
        return (unsigned int) ((parameters->size() - 1 > 0) ? (parameters->size() - 1) : 0);
    }

    void Signal_Ast::iteratorNext(Node ***iterator, Node **node) {

        if (!node) {
            **iterator = NULL;
        } else {
            **iterator = (*node)->next();
            *node = **iterator;
        }
    }


    Signal_Ast *Signal_Ast::createCustom(unsigned int signalType, const unsigned int type, string content) {
        Signal_Ast *signal = new Signal_Ast();
        signal->name = content;
        signal->type = type;
        signal->signalType = signalType;

        return signal;
    }

    Signal_Ast *Signal_Ast::parseOrphan(Node **iterator, unsigned int signalType) {
        Node *node = *iterator;

        Signal_Ast *signal = Signal_Ast::createCustom(signalType, node->type, node->content);

        // check for orphan parameters

        if (node->firstChild()) {
            Node *expression = node->firstChild();
            signal->parameters->push_back(
                    Expression_AstNode::parse(&expression, NULL, PARSER_MODE__DONT_UNWRAP_SINGLE));
        }

        if (*iterator) {
            *iterator = (*iterator)->next();
        }

        return signal;
    }

    Signal_Ast *Signal_Ast::parse(Node **iterator, Expression_AstNode **tail) {

        Node *node = *iterator;
        Node *subIterator;
        Signal_Ast *signal = Signal_Ast::createCustom(SIGNAL__CALL, node->type, node->content);

        if (node->isLike(EXPRESSION_NODE_UNARY_RIGHT_OPERATOR | EXPRESSION_NODE_BINARY_OPERATOR,
                         EXPRESSION_NODE_OPERATOR)) {
            if (*tail && node->is(SPEC_SYM__PARENTHESES_OPEN)) {
                // load prev node (tails - called object, prev - called method, node - parameters)

                if (IS_DEBUG(DEBUG__PARSER)) {
                    cout << "using tail" << endl;
                    (*tail)->dump();
                }

                signal->parameters->push_back(*tail);
                *tail = NULL;
            }

            if (!*tail && node->prev()) {

                if (IS_DEBUG(DEBUG__PARSER)) {
                    cout << "parsing as unary" << endl;
                    node->prev()->dump();
                }

                subIterator = node->prev();
                *tail = Expression_AstNode::parseUnary(&subIterator);
            }

            if (*tail) {

                if (IS_DEBUG(DEBUG__PARSER)) {
                    cout << "Addiding tail" << endl;
                    (*tail)->dump();
                }

                signal->parameters->push_back(*tail);
            }
        }

        if (node->isLike(EXPRESSION_NODE_BINARY_OPERATOR | EXPRESSION_NODE_UNARY_LEFT_OPERATOR,
                         EXPRESSION_NODE_OPERATOR)) {
            if (node->is(SPEC_SYM__PARENTHESES_OPEN)) {
                //
                // if CALL parameters are children of operator node
                Expression_AstNode::parseParams(node, signal->parameters);

            } else {
                // if CALL parameter is next expression (chain of expressions
                *iterator = node->next();

                signal->parameters->push_back(Expression_AstNode::parse(
                        iterator,
                        NULL,
                        node->is(SPEC_SYM__ASSIGN) ? PARSER_MODE__TO_EOL : PARSER_MODE__ONLY_ONE
                ));

                // rewind to prevent double next (considering method's end)
                if (*iterator) {
                    *iterator = (*iterator)->prev();
                }
            }
        }

        *tail = signal;

        if (*iterator) {
            *iterator = (*iterator)->next();
        }

        return signal;
    }

    void Signal_Ast::dump(stringstream &out, string prefix) {
        out << prefix << Signal_Ast::signalTypeToString(signalType) << " (" << Node::typeToString(type) << ") " <<
        name << endl;

        for (Expression_AstNode *parameter : *parameters) {
            if (!parameter) {
                cout << "EMPTY PARAMETER" << endl;
            }
            parameter->dump(out, prefix + "  ");
        }
    }


    string Signal_Ast::signalTypeToString(unsigned int signalType) {
        switch (signalType) {
            case SIGNAL__CALL:
                return "!CALL";
            case SIGNAL__METHOD_ON_OBJECT:
                return "!METHOD ON OBJECT";
            case SIGNAL__CALLED_METHOD:
                return "!CALLED METHOD";
            case SIGNAL__CALLED_OBJECT:
                return "!CALLED OBJECT";
            case SIGNAL__INSTANCE_VARIABLE:
                return "!INSTANCE_VARIABLE";
            case SIGNAL__OPERATOR:
                return "!OPERATOR";
            case SIGNAL__RETURN:
                return "!RETURN";
            case SIGNAL__THROW:
                return "!THROW";
            case SIGNAL__PRINT:
                return "!PRINT";
            case SIGNAL__CONTINUE:
                return "!CONTINUE";
            case SIGNAL__BREAK:
                return "!BREAK";
            case SIGNAL__THIS:
                return "!THIS";
            case SIGNAL__SUPER:
                return "!SUPER";

            case SIGNAL__NOT_SET:
            default:
                return "!!UNKNOWN!!";
        }
    }

#define SIGNAL__CONVERT_TO_OPERATOR__HELPER(__SYMBOL__, __BC_OPERATOR__) case __SYMBOL__:\
        INST__OPERATOR(__BC_OPERATOR__);\
    break;

    string Signal_Ast::compile(Cake_Compiler::Compiler *compiler) {
        stringstream byteCode;
        stringstream childCode;

        bool autoCompileParameters = true;

        // new operator
        // TODO: clean
        if (signalType == SIGNAL__RETURN) {
            for (Expression_AstNode *child : *parameters) {
                byteCode << child->compile(compiler);
            }
            RETURN();
            return byteCode.str();
        }

        if (signalType == SIGNAL__CALL && type == SPEC_SYM__EXCLAMATION_MARK) {
            Signal_Ast* arg = (Signal_Ast*)parameters->at(0);

            byteCode << arg->compile(compiler);

            NEGATE();

            return byteCode.str();
        }

        if (signalType == SIGNAL__CALL && type == SPEC_SYM__PARENTHESES_OPEN) {

            Signal_Ast* methodNode = (Signal_Ast*)parameters->at(0);
            Signal_Ast* className = (Signal_Ast*)parameters->at(1);

            if (!methodNode->name.compare("new")) {
                NEW(className->name);

                return byteCode.str();
            }

            // method call
            bool calledObjectFlag = false;
            bool calledMethodFlag = false;

            for (Expression_AstNode *child : *parameters) {
                if (calledObjectFlag && calledMethodFlag) {
                    byteCode << child->compile(compiler);
                } else if (calledObjectFlag) {
                    calledMethodFlag = true;
                } else {
                    calledObjectFlag = true;
                }
            }

            // called object is last
            byteCode << parameters->at(0)->compile(compiler);

            INVOKE(((Signal_Ast*)parameters->at(1))->name);

            return byteCode.str();
        }

        // Special cases of signals
        if (signalType == SIGNAL__CALL && type == SPEC_SYM__ASSIGN) {

            // store to variable 0
            Signal_Ast* target = dynamic_cast<Signal_Ast*>(parameters->at(0));

            // validation
            if (!(
                    (target->signalType == SIGNAL__CALL && target->type == EXPRESSION_NODE_SYMBOL) ||
                    (target->signalType == SIGNAL__INSTANCE_VARIABLE && target->type == EXPRESSION_NODE_SYMBOL)
            )) {
                throw new Cake_Compiler::InvalidAssignTarget();
            }

            switch (target->signalType) {
                case SIGNAL__CALL:
                    byteCode << parameters->at(1)->compile(compiler);
                    STORE(target->name)
                    break;

                case SIGNAL__INSTANCE_VARIABLE:
                    LOAD_THIS();
                    byteCode << parameters->at(1)->compile(compiler);
                    SET_FIELD(target->name);
                    break;
            }

            autoCompileParameters = false;
        }

        switch (signalType) {
            case SIGNAL__CALL:
                switch (type) {
                    SIGNAL__CONVERT_TO_OPERATOR__HELPER(SPEC_SYM__GREATER_THAN, VM__INSTRUCTION__OP__LESS_THAN);
                    SIGNAL__CONVERT_TO_OPERATOR__HELPER(SPEC_SYM__LESS_THAN, VM__INSTRUCTION__OP__GREATER_THAN);
                    SIGNAL__CONVERT_TO_OPERATOR__HELPER(SPEC_SYM__GREATER_OR_EQUAL, VM__INSTRUCTION__OP__GREATER_OR_EQ);
                    SIGNAL__CONVERT_TO_OPERATOR__HELPER(SPEC_SYM__LESS_OR_EQUAL, VM__INSTRUCTION__OP__LESS_OR_EQ);
                    SIGNAL__CONVERT_TO_OPERATOR__HELPER(SPEC_SYM__EQUAL, VM__INSTRUCTION__OP__EQUAL);
                    SIGNAL__CONVERT_TO_OPERATOR__HELPER(SPEC_SYM__NOT_EQUAL, VM__INSTRUCTION__OP__NOT_EQUAL);

                    SIGNAL__CONVERT_TO_OPERATOR__HELPER(SPEC_SYM__PLUS, VM__INSTRUCTION__OP__PLUS);
                    SIGNAL__CONVERT_TO_OPERATOR__HELPER(SPEC_SYM__MINUS, VM__INSTRUCTION__OP__MINUS);
                    SIGNAL__CONVERT_TO_OPERATOR__HELPER(SPEC_SYM__SLASH, VM__INSTRUCTION__OP__DIVIDE);
                    SIGNAL__CONVERT_TO_OPERATOR__HELPER(SPEC_SYM__PERCENT, VM__INSTRUCTION__OP__RESIDUUM);
                    SIGNAL__CONVERT_TO_OPERATOR__HELPER(SPEC_SYM__TIMES, VM__INSTRUCTION__OP__TIMES);



                    case EXPRESSION_NODE_SYMBOL:
                        LOAD(name);
                        autoCompileParameters = false;
                        break;
                }
                break;
            case SIGNAL__PRINT:
                BC__PRINT();
                break;
            case SIGNAL__THIS:
                LOAD_THIS();
                break;
            case SIGNAL__INSTANCE_VARIABLE:
                LOAD_THIS();
                GET_FIELD(name);
                break;


            default:
                break;
        }

        if (autoCompileParameters) {
            for (Expression_AstNode *child : *parameters) {
                childCode << child->compile(compiler);
            }
        }

        // prepend children to this expression
        childCode << byteCode.str();

        return childCode.str();
    }
}
