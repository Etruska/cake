#include "Parser/ast/environment.h"

#include <iomanip>

namespace Cake_Parser {

    void Environment_Ast::dump(stringstream &out) {
        out << "Environment" << endl;

        for (Class_Ast *c_class : classes) {
            c_class->dump(out);
        }
    }


    void Environment_Ast::dump() {
        AstNode::dump();
    }

    string Environment_Ast::dumpToString() {
        vector<string> rawLines = Cake_Helpers::String::splitWithWhitespaces(AstNode::dumpToString(), "\n");
        IndentedLine indentedLine;
        string output = "";

        int lineCounter = 0;

        stringstream tmp;

        for (string &line : rawLines) {
            lineCounter++;
            tmp.str(std::string());
            //tmp << setw(6) << lineCounter << ":  ";
            tmp << line << endl;
            output += tmp.str();
        }

        return output;
    }

    Environment_Ast::Environment_Ast() {
        classes = vector<Class_Ast *>();
    }

    Environment_Ast *Environment_Ast::parse(Node *node) {
        Environment_Ast *env = new Environment_Ast();

        for (Node *child : node->children) {
            if (child->is(NODE_CLASS)) {
                env->classes.push_back(Class_Ast::parse(child));
            } else {
                compile_error("Env should contain only classes");
            }
        }

        return env;
    }


    void Environment_Ast::validate(Node *node) {
        Cake_Parser::Environment_Ast::parse(node);

        exit(0);
    }


    string Environment_Ast::dumpToInlineString() {

        string out = dumpToString();

        string res;
        for (int i = 0; i < out.length(); ++i) {
            switch (out[i]) {
                case '\r':
                    res += "\\r";
                    break;
                case '\n':
                    res += "\\n";
                    break;
                case '\\':
                    res += "\\\\";
                    break;
                case '\t':
                    res += "\\t";
                    break;
                    //add other special characters if necessary...
                default:
                    res += out[i];
            }
        }
        return res;
    }

    string Environment_Ast::compile(Cake_Compiler::Compiler *compiler) {
        stringstream bytecode;
        for (Class_Ast *c_class : classes) {
            c_class->compile(compiler);
        }

        return bytecode.str();
    }


    void Environment_Ast::dumpCompiled(Cake_Compiler::Compiler *compiler) {
        cout << "Environment" << endl;

        for (Class_Ast *c_class : classes) {
            c_class->dumpCompiled(compiler);
        }
    }
}

