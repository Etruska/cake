#include "Parser/ast/expression.h"

#include "Parser/ast/constant.h"
#include "Parser/ast/signal.h"
#include "Parser/ast/while.h"
#include "Parser/ast/try.h"
#include "Parser/ast/if.h"

#include "Configuration/config.h"

namespace Cake_Parser {
    string Expression_AstNode::typeToString(unsigned int type) {
        switch (type) {
            case EXPRESSION_TYPE__SIGNAL:
                return "SIGNAL";
            case EXPRESSION_TYPE__CONSTANT:
                return "CONSTANT";
            case EXPRESSION_TYPE__IF:
                return "IF";
            case EXPRESSION_TYPE__TRY:
                return "TRY";
            case EXPRESSION_TYPE__WHILE:
                return "WHILE";
            case EXPRESSION_TYPE__EXPRESSION:
                return "EXPRESSION";
            default:
                return "UNKNOWN";
        }
    }

    Expression_AstNode::Expression_AstNode() {
        type = EXPRESSION_TYPE__EXPRESSION;
    }

    void Expression_AstNode::dump() {
        stringstream out;
        dump(out, "   ");

        cout << out.str();
    }

    void Expression_AstNode::dump(stringstream &out) {
        dump(out, "   ");
    }

#define POLYMORPH_DUMP_HELPER(__TYPE__) {__TYPE__* exp##__TYPE__ = dynamic_cast<__TYPE__*>(this); exp##__TYPE__->dump(out, prefix); return;}

    void Expression_AstNode::dump(stringstream &out, string prefix) {
        out << prefix << Expression_AstNode::typeToString(getType()) << endl;

        for (Expression_AstNode *child : content) {
            if (!child) {
                out << "NULL CHILD EXPRESSION" << endl;
            } else {
                child->dump(out, prefix + "  ");
            }
        }
    }

    void Expression_AstNode::addContent(Expression_AstNode *node) {
        if (!node) {
            compile_error("TRYING TO ADD NULL NODE");
        }

        content.push_back(node);
    }

    Expression_AstNode *Expression_AstNode::parseUnary(Node **node) {
        return Expression_AstNode::parse(node, (*node)->next());
    }

    vector<Expression_AstNode *> *Expression_AstNode::parseParams(Node *node, vector<Expression_AstNode *> *params) {
        if (!params) {
            params = new vector<Expression_AstNode *>();
        }

        // if no children -> no parameters
        if (node->hasNoChildren()) {
            return params;
        }

        // mutable iteration
        Node *iterator = node->firstChild();
        Node *start = iterator;

        while (iterator) {
            if (IS_DEBUG(DEBUG__PARSER)) {
                cout << "P--------------------------" << endl;
                iterator->dumpFromParent();
                cout << "---------------------------" << endl;
            }
            if (iterator->is(SPEC_SYM__COMMA)) {
                params->push_back(Expression_AstNode::parse(&start, iterator));

                start = iterator->next();
            }

            // if no rule hits step to next node
            iterator = iterator->next();
        }

        // finish last parameter
        if (start) {
            params->push_back(Expression_AstNode::parse(&start, NULL));
        }

        return params;
    }

#define PARSE_NODE_TYPE_HELPER(__TYPE__) expNode->addContent(__TYPE__::parse(&iterator)); continue;

#define PARSE_CUSTOM_ORPHAN_SIGNAL(__SIGNAL_TYPE__) expNode->addContent(Signal_Ast::parseOrphan(&iterator, __SIGNAL_TYPE__)); continue;

    Expression_AstNode *Expression_AstNode::parse(Node **node, Node *to, unsigned int mode) {
        // FROM NODE to TO (TO is excluded)
        Expression_AstNode *expNode = new Expression_AstNode();

        // mutable iteration
        Node *iterator = *node;
        Node *start = *node;
        Node *subIterator;

        Expression_AstNode *tail = NULL;

        // no node -> empty expression
        if (!iterator) {
            return expNode;
        }

        while (iterator && (iterator != to)) {
            if (IS_DEBUG(DEBUG__PARSER)) {
                cout << "I--------------------------" << endl;
                iterator->dumpFromParent();
                cout << "---------------------------" << endl;
            }

            if (iterator->isLike(PROCEDURAL_NODE)) {
                if (IS_DEBUG(DEBUG__PARSER)) {
                    cout << "<< Checking type of procedural node" << endl;
                }
                switch (iterator->type) {
                    // SPECIAL BLOCKS
                    case NODE_WHILE:
                    PARSE_NODE_TYPE_HELPER(While_Ast);

                    case NODE_TRY:
                        //PARSE_NODE_TYPE_HELPER(Try_Ast);
                        continue;

                    case NODE_IF:
                    PARSE_NODE_TYPE_HELPER(If_Ast);

                        // SPECIAL ALIAS SYMBOLS

                        // access instances variables or
                    case NODE_THIS:
                        if (iterator->nextIs(EXPRESSION_NODE_SYMBOL)) {
                            // "@x" - next is symbol -> instance variable access
                            subIterator = iterator = iterator->next();
                            tail = Signal_Ast::parseOrphan(&subIterator, SIGNAL__INSTANCE_VARIABLE);

                        } else {
                            // "@" or "@.x()" - current instance object access
                            tail = Signal_Ast::createCustom(SIGNAL__THIS, NODE_THIS);
                        }
                        iterator = iterator->next();
                        continue;

                    case NODE_SUPER:
                        tail = Signal_Ast::createCustom(SIGNAL__SUPER, NODE_SUPER);
                        iterator = iterator->next();
                        continue;

                        // special methods
                    case NODE_PRINT:
                    PARSE_CUSTOM_ORPHAN_SIGNAL(SIGNAL__PRINT);

                    case NODE_RETURN:
                    PARSE_CUSTOM_ORPHAN_SIGNAL(SIGNAL__RETURN);

                    case NODE_BREAK:
                    PARSE_CUSTOM_ORPHAN_SIGNAL(SIGNAL__BREAK);

                    case NODE_CONTINUE:
                    PARSE_CUSTOM_ORPHAN_SIGNAL(SIGNAL__CONTINUE);

                    default:
                        if (IS_DEBUG(DEBUG__PARSER)) {
                            cout << "<< Checking type of procedural node: No match" << endl;
                        }
                        break;
                }
            }

            // line of code
            if (iterator->is(EXPRESSION_NODE_BLOCK)) {
                // saving line can be done right away

                expNode->addContent(Expression_AstNode::parseTree(iterator));

                iterator = iterator->next();

                if (IS_DEBUG(DEBUG__PARSER)) {
                    cout << "<< Parsed line of code" << endl;
                }
                continue;
            }

            // skip symbols (processed by signals and expressions)
            if (iterator->isLike(EXPRESSION_NODE_CONSTANT)) {
                if (IS_DEBUG(DEBUG__PARSER)) {
                    cout << "<< Skipping constant" << endl;
                }
                iterator = iterator->next();
                continue;
            }

            // Check if end of single chain
            if (!iterator->isLike(EXPRESSION_NODE_CHAINABLE_OPERATOR, EXPRESSION_NODE_OPERATOR)) {
                if (IS_DEBUG(DEBUG__PARSER)) {
                    cout << "<< Exnding chain" << endl;
                }
                if (mode == PARSER_MODE__ONLY_ONE) {
                    if (!tail && iterator->prev()) {
                        Node *tempIterator = iterator->prev();
                        expNode->addContent(Expression_AstNode::parseUnary(&tempIterator));
                    }
                    goto after_loop;
                }
            }

            if (iterator->isLike(EXPRESSION_NODE_OPERATOR)) {
                // Check for sub expression
                if (!tail && iterator->is(SPEC_SYM__PARENTHESES_OPEN) && !iterator->prevIs(EXPRESSION_NODE_SYMBOL)) {
                    if (IS_DEBUG(DEBUG__PARSER)) {
                        cout << "<< Subexpression" << endl;
                    }
                    // Sub expression
                    tail = Expression_AstNode::parseTree(iterator);
                    iterator = iterator->next();

                } else {
                    if (IS_DEBUG(DEBUG__PARSER)) {
                        cout << "<< Processing operator" << endl;
                    }

                    // chain or operator
                    Signal_Ast::parse(&iterator, &tail);
                }
            }
        }

        after_loop:



        // append last tail section
        if (tail) {
            expNode->addContent(tail);
        }

        // Parse unary
        if (start && start != to && expNode->content.empty()) {
            if (IS_DEBUG(DEBUG__PARSER)) {
                cout << "<< Parsing unary" << endl;
                cout << "U--------------------------" << endl;
                start->dumpTree();
                cout << "---------------------------" << endl;
            }

            // TODO: free expNode

            switch (start->type) {
                case EXPRESSION_NODE_INTEGER:
                case EXPRESSION_NODE_FLOAT:
                case EXPRESSION_NODE_STRING:
                    expNode = Constant_Ast::parse(&start);
                    break;

                case EXPRESSION_NODE_SYMBOL:
                case NODE_SUPER:
                case NODE_THIS:
                    expNode = Signal_Ast::parseOrphan(&start);
                    break;

                    // special signals
                case NODE_RETURN:
                    expNode = Signal_Ast::parseOrphan(&start, SIGNAL__RETURN);
                    break;

                case NODE_PRINT:
                    expNode = Signal_Ast::parseOrphan(&start, SIGNAL__PRINT);
                    break;

                default:
                    // handle blocks
                    if (start->isLike(EXPRESSION_NODE_NESTED_OPERATOR)) {
                        expNode = Expression_AstNode::parseTree(start);
                        break;
                    }

                    compile_error("unknown unary operation");
            }
        }

        // forward iterator to end
        *node = iterator;
        if (IS_DEBUG(DEBUG__PARSER)) {
            cout << "<< Setting iterator to" << endl;
            cout << "U--------------------------" << endl;
            if (*node) {
                (*node)->dumpTree();
            } else {
                cout << "NULL" << endl;
            }
            cout << "---------------------------" << endl;
        }

        // if result expression has only one child unwrap it
        if (mode != PARSER_MODE__DONT_UNWRAP_SINGLE && expNode->content.size() == 1) {
            return expNode->content.front();
        }

        return expNode;
    }

    Expression_AstNode *Expression_AstNode::parseTree(Node *node, unsigned int mode) {
        Node *iterator = node->firstChild();
        return Expression_AstNode::parse(&iterator, NULL, mode);
    }

    unsigned int Expression_AstNode::getType() {
        return type;
    }

    string Expression_AstNode::compile(Cake_Compiler::Compiler *compiler) {
        stringstream byteCode;

        // parse children
        for (Expression_AstNode *child : content) {
            byteCode << child->compile(compiler);
        }

        return byteCode.str();
    }
}