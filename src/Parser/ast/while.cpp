#include "Parser/ast/while.h"

#include "VM/IS.h"

namespace Cake_Parser {
    While_Ast::While_Ast() {
        type = EXPRESSION_TYPE__WHILE;
    }


    void While_Ast::dump(stringstream &out, string prefix) {
        out << prefix << "WHILE:" << endl;
        out << prefix << "  Cond:" << endl;

        condition->dump(out, prefix + "    ");

        out << prefix << "  Body:" << endl;

        body->dump(out, prefix + "    ");
    }

    While_Ast *While_Ast::parse(Node **iterator) {
        Node *node = *iterator;
        While_Ast *c_while = new While_Ast();
        c_while->type = node->type;

        if (IS_DEBUG(DEBUG__PARSER)) {
            cout << ":WHILE---------------------" << endl;
            node->dumpTree();
            cout << "---------------------------" << endl;
        }

        node = node->firstChild();
        // Condition
        if (IS_DEBUG(DEBUG__PARSER)) {
            cout << "WHILE-COND-----------------" << endl;
            node->dumpTree();
            cout << "---------------------------" << endl;
        }

        c_while->condition = Expression_AstNode::parseTree(node);

        node = node->next();

        // Content
        if (IS_DEBUG(DEBUG__PARSER)) {
            cout << "WHILE-CONTENT--------------" << endl;
            node->dumpTree();
            cout << "---------------------------" << endl;
        }

        c_while->body = Expression_AstNode::parseTree(node);

        *iterator = (*iterator)->next();

        return c_while;
    }

    string While_Ast::compile(Cake_Compiler::Compiler *compiler) {
        stringstream byteCode;
        unsigned short endIdx;
        unsigned short startIdx;

        string bodyByteCode;

        startIdx = compiler->current;

        byteCode << condition->compile(compiler);

        // IF
        Cake_Compiler::Compiler::RESERVE__PUSH__IDX_WITH_OP(compiler);

        bodyByteCode = body->compile(compiler);

        // LOOP
        Cake_Compiler::Compiler::RESERVE__PUSH__IDX_WITH_OP(compiler);

        endIdx = compiler->current;

        // COMPOSE

        BC__IF();
        Cake_Compiler::Compiler::PUSH__IDX(compiler, byteCode, endIdx);

        byteCode << bodyByteCode;

        BC__GOTO();
        Cake_Compiler::Compiler::PUSH__IDX(compiler, byteCode, startIdx);

        return byteCode.str();
    }
}
