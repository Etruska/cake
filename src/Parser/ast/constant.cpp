#include "Parser/ast/constant.h"

#include "Helpers/bytecode.h"

#include "VM/Integer.h"
#include "VM/VMString.h"
#include "VM/IS.h"

using namespace VM;

namespace Cake_Parser {
    Constant_Ast::Constant_Ast() {
        type = EXPRESSION_TYPE__CONSTANT;
    }

    void Constant_Ast::dump(stringstream &out, string prefix) {
        out << prefix << Expression_AstNode::typeToString(getType()) << " | (" << Node::typeToString(constantType) <<
        ") " << name << endl;
    }

    Expression_AstNode *Constant_Ast::parse(Node **iterator) {
        Node *node = *iterator;
        Constant_Ast *n_constant = new Constant_Ast();
        n_constant->name = node->content;
        n_constant->constantType = node->type;

        return n_constant;
    }

    string Constant_Ast::compile(Cake_Compiler::Compiler *compiler) {
        stringstream byteCode;

        switch (constantType) {
            case EXPRESSION_NODE_INTEGER:
            CONST_POOL__INT(name);
                break;

            case EXPRESSION_NODE_STRING:
            CONST_POOL__STRING(name);
                break;

                // TODO: case EXPRESSION_NODE_FLOAT:
        }

        return byteCode.str();
    }
}
