#include "Parser/ast/class.h"

namespace Cake_Parser {
    Class_Ast::Class_Ast() {
        name = "";
        parent = NULL;
        methods = vector<Method_Ast *>();
    }


    void Class_Ast::dump(stringstream &out) {
        out << "  " << name << endl;

        if (parent) {
            out << "    PARENT: " << parent->name << endl;
        }

        out << "    PROPERTIES: " << endl;
        for (string property : rawProperties) {
            out << "       " << property << endl;
        }

        out << "    METHODS: " << endl;
        for (Method_Ast *c_method : methods) {
            c_method->dump(out);
        }
        out << endl;
    }

    Class_Ast *Class_Ast::parse(Node *node) {
        Node *expression = node->firstChild();

        // [0] => signature
        // [0][0] => class name
        Class_Ast *c_class = new Class_Ast();
        c_class->name = expression->firstChild()[0].content;

        // extending:
        // [0][1] => spec sym <
        // [0][2] => parent class name
        if (expression->hasChildren(3) && expression->childIs(SPEC_SYM__CHEVRON_CLOSE, 1)) {
            Class_Ast *c_parent_class = new Class_Ast();
            c_parent_class->name = expression->childsContent(2);

            c_class->parent = c_parent_class;
        }

        bool firstExpressionFlag = true;

        // [1..n] => properties
        // [1..n] => methods
        for (Node *child : node->children) {
            if (child->is(EXPRESSION_NODE)) {
                if (!firstExpressionFlag) {
                    c_class->rawProperties.push_back(child->children.at(1)->content);
                }
                firstExpressionFlag = false;
                continue;
            }

            if (child->is(NODE_METHOD)) {
                c_class->methods.push_back(Method_Ast::parse(child));
            } else {
                compile_error("Class should contain only methods");
            }
        }

        return c_class;
    }


    string Class_Ast::compile(Cake_Compiler::Compiler *compiler) {
        VMClass* c_class = new VMClass(name, parent ? new VMClass(parent->name, NULL) : NULL);
        this->linkedClass = c_class;

        compiler->classPool->add(c_class);

        // set properties
        for (const string property : rawProperties) {
            propertiesMap.setItem(property, c_class->addField(new VMField()));
        }

        compiler->currentClass = this;

        // parse children
        for (Method_Ast *child : methods) {
            child->compile(compiler);
        }

        compiler->currentClass = NULL;

        return "";
    }

    void Class_Ast::dumpCompiled(Cake_Compiler::Compiler *compiler) {
        cout << "  " << name << endl;

        if (parent) {
            cout << "    PARENT: " << parent->name << endl;
        }

        cout << "    PROPERTIES: " << endl;
        for (string property : rawProperties) {
            cout << "       " << property << endl;
        }

        cout << "    METHODS: " << endl;
        for (Method_Ast *c_method : methods) {
            c_method->dumpCompiled(compiler);
        }
        cout << endl;
    }
}
