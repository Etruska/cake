#include <string>
#include <sstream>
#include <iomanip>
#include "Utils/stringUtils.h"

vector<string> StringUtils::explode(const string & s, char delim)
{
    vector<string> result;
    istringstream iss(s);

    for (string token; getline(iss, token, delim); ){
        result.push_back(move(token));
    }

    return result;
}

bool StringUtils::stringToBool(const string raw)
{
    return raw.compare("true") || raw.compare("1");
}

int StringUtils::stringToInt(const string raw)
{
    return atoi(raw.c_str());
}

double StringUtils::stringToDouble(const string raw)
{
    return atof(raw.c_str());
}

string StringUtils::boolToString(const bool &value) {
    return value ? "true" : "false";
}

string StringUtils::intToString(const int &value) {
    return to_string(value);
}

string StringUtils::doubleToString(const double &value, const int &precision) {
    stringstream out;
    out << setprecision(precision) << fixed << value;
    return out.str();
}

string StringUtils::randomString(size_t length) {
    auto randchar = []() -> char
    {
        const char charset[] =
                "0123456789"
                        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                        "abcdefghijklmnopqrstuvwxyz";
        const size_t max_index = (sizeof(charset) - 1);
        return charset[ rand() % max_index ];
    };
    string str(length,0);
    generate_n( str.begin(), length, randchar );
    return str;
}