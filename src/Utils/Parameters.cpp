#include "Utils/parameters.h"

#include <iostream>
#include <stdlib.h>
#include <stdexcept>
#include <sstream>

#include "Utils/stringUtils.h"

Parameters::Parameters()
{
}

Parameters::Parameters(int argc, char* argv[])
{
    this->parseParametersFromString(argc, argv);
}



void Parameters::parseParametersFromString(int argc, char* argv[])
{
    string tmp;
    vector<string> explodedArg;
    for(int i = 1; i < argc; i++) {
        tmp = string(argv[i]);

        tmp.erase(0, 2);
        explodedArg = StringUtils::explode(tmp, '=');

        this->parameters[explodedArg[0]] = (explodedArg.size() == 1) ? "true" : explodedArg[1];
    }
}

bool Parameters::hasParameter(const string& key)
{
    return this->parameters.count(key) > 0;
}

string Parameters::getParameter(const string& key)
{
    if (!this->hasParameter(key)) {
        stringstream str;
        str << "Missing mandatory parameter \"" << key << "\"!" << endl;

        this->dump();
        throw runtime_error(str.str());
    }
    return parameters[key];
}

void Parameters::setParameter(const string& key, const string& parameterValue)
{
    this->parameters[key] = parameterValue;
}
/*
void Parameters::setParameter(const string& key, const bool& parameterValue)
{
    this->parameters[key] = StringUtils::boolToString(parameterValue);
}

void Parameters::setParameter(const string& key, const int& parameterValue)
{
    this->parameters[key] = StringUtils::intToString(parameterValue);
}

void Parameters::setParameter(const string& key, const double& parameterValue, const int &precision)
{
    this->parameters[key] = StringUtils::doubleToString(parameterValue, precision);
}*/

void Parameters::setParameterDefaultValue(const string& key, const string& parameterValue) {
    if (!hasParameter(key)) {
        setParameter(key, parameterValue);
    }
}

string Parameters::getStringParameter(const string& key)
{
    return this->getParameter(key);
}

bool Parameters::getBooleanParameter(const string& key)
{
    return StringUtils::stringToBool(this->getParameter(key));
}

int Parameters::getIntegerParameter(const string& key)
{
    return StringUtils::stringToInt(this->getParameter(key));
}

double Parameters::getDoubleParameter(const string& key)
{
    return StringUtils::stringToDouble(this->getParameter(key));
}

void Parameters::dump()
{
    cout << "Parameters:" << endl;

    for (auto it = parameters.cbegin(); it != parameters.cend(); ++it) {
        cout << "  --" << it->first << ": " << it->second << endl;
    }

    cout << endl;
}

map<string, string>& Parameters::getParameters() {
    return parameters;
}
