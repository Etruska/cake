#ifndef HEAP_HEAPMANAGER_CPP
#define HEAP_HEAPMANAGER_CPP

#include <VM/VMString.h>
#include "Heap/HeapManager.h"
#include "Heap/HeapException.h"
#include "VM/VMObjectRef.h"
#include "VM/Integer.h"
#include "Configuration/config.h"

using namespace std;
using namespace VM;

namespace Heap {


    HeapManager::HeapManager(unsigned int poolSize, bool resetGcEnabled) {
        this->poolSize = poolSize;
        this->resetGcEnabled = resetGcEnabled;
        leftPool = (byte*) malloc((size_t)(this->poolSize * sizeof(byte)));
        rightPool = (byte*) malloc(this->poolSize * sizeof(byte));
        if (IS_DEBUG(DEBUG_GC)) {
            cout << "LEFT POOL: (" << (size_t) (this->poolSize * sizeof(byte)) << ")" << (void *) leftPool << " - " <<
            (void *) (leftPool + (this->poolSize * sizeof(byte))) << endl;
            cout << "RIGHT POOL: (" << (size_t) (this->poolSize * sizeof(byte)) << ")" << (void *) rightPool << " - " <<
            (void *) (rightPool + (this->poolSize * sizeof(byte))) << endl;
        }
        clearPool(leftPool);
        clearPool(rightPool);
        poolCursor = leftPool;
        currentPool = GC__POOL_LEFT;
        poolBytesAllocated = 0;
        lastProxyObject = NULL;
    }


    HeapManager::~HeapManager() {
        free(leftPool);
        free(rightPool);
    }


    VMObjectRef *HeapManager::heapCloneObject(VMObjectRef *object) {
        VMObjectRef* res = createRawObject(object->getClassRef());

        // clone fields
        byte* source = (byte*) (object->getPointer());
        byte* target = (byte*) (res->getPointer());
        memcpy(target, source, getPropertiesCount(res->getClassRef()) * sizeof(VMObjectProxy **));

        return res;
    }

    VMObjectRef *HeapManager::createRawObject(VMClass *classRef) {
        int propertiesCount = this->getPropertiesCount(classRef);
        int propertiesSize = (int)sizeof(VMObject*) * propertiesCount;

        VMObjectRef* ref = (VMObjectRef*)allocateMemory(sizeof(VMObjectRef));

        VMObject** propertyStart = (VMObject**)allocateMemory(propertiesSize);

        new(ref) VMObjectRef(propertyStart, classRef);

        // prepare empty proxies
        for (int i = 0; i < propertiesCount; i++) {
            VMObject** filedPointer = getPropertyPointer(ref, i);
            *filedPointer = createProxyFor(NULL);
        };

        if (IS_DEBUG(DEBUG_GC)) {
            cout << "New object (" << classRef->getName() << ") (" << (void*)ref << ") " << propertiesCount << " properties on (" << (void**)propertyStart << " - " << (void**)(propertyStart + propertiesCount) << ")" << endl;
            dumpRef(ref);
        }

        return ref;
    }

    Integer *HeapManager::createRawInteger(const int value) {
        unsigned int heapSize = sizeof(Integer);
        Integer* pointer = (Integer*) allocateMemory(heapSize);

        new(pointer) Integer(value);

        return pointer;
    }

    VMString *HeapManager::createRawString(const string content) {
        unsigned int heapSize = sizeof(VMString);
        VMString* pointer = (VMString*) allocateMemory(heapSize);

        new(pointer) VMString(content);

        return pointer;
    }

    VMObjectProxy *HeapManager::createObject(VMClass *classRef) {
        return createProxyFor(createRawObject(classRef));
    }


    VMObjectProxy *HeapManager::createInteger(const int value) {
        return createProxyFor(createRawInteger(value));
    }

    VMObjectProxy *HeapManager::createString(const string content) {
        return createProxyFor(createRawString(content));
    }

    VMObjectProxy *HeapManager::getProperty(VMObjectRef *objRef, int propertyIndex) {
        VMClass *classRef = objRef->getClassRef();

        int propertiesCount = this->getPropertiesCount(classRef);
        if (propertyIndex >= propertiesCount) {
            throw HeapException::PROP_INDEX_OUT_OF_RANGE;
        }
        VMObjectProxy *propertyCell = (VMObjectProxy *) *getPropertyPointer(objRef, propertyIndex);

        return propertyCell;
    }


    int HeapManager::getPropertiesCount(VMClass *classRef) {
        return classRef->getFields().size();
    }

    void HeapManager::setProperty(VMObjectRef *objRef, int propertyIndex, VMObject *property) {
        VMClass *classRef = objRef->getClassRef();
        int propertiesCount = this->getPropertiesCount(classRef);
        if (propertyIndex >= propertiesCount) {
            throw HeapException::PROP_INDEX_OUT_OF_RANGE;
        }

        //dumpRef(objRef);
        VMObject **field = getPropertyPointer(objRef, propertyIndex);
        if (IS_DEBUG(DEBUG__VM)) {
            cout << "Setting field: " << (void**)field  << " of value " << (void*)*field;
        }
        *field = createProxyFor(property->instance() ? property->instance()->clone(this) : NULL);
        if (IS_DEBUG(DEBUG__VM)) {
            cout << " to " << (void *) *field << endl;
        }

        //dumpRef(objRef);
    }


    VMObjectProxy *HeapManager::createProxyFor(VMObject *instance) {
        lastProxyObject = new VMObjectProxy(instance, lastProxyObject, 0);
        if (IS_DEBUG(DEBUG_GC)) {
            cout << "Proxy on: " << (void *) lastProxyObject << " for ";
            if (instance) {
                cout << (void*)instance;
            } else {
                cout << "NULL";
            }

            cout <<  endl;
        }
        return lastProxyObject;
    }

// GC
    byte *HeapManager::allocateMemory(unsigned int size) {
        if (IS_DEBUG(DEBUG_GC)) {
            //cout << "Allocation of " << size << " from (" << poolCursor << ")" <<poolBytesAllocated << "/" << poolSize << endl;
        }
        if (poolBytesAllocated + size > poolSize) {
            runGarbageCollector(size);
        }

        byte* allocatedMemoryPointer = poolCursor;

        poolBytesAllocated += size;
        poolCursor += size;
        if (IS_DEBUG(DEBUG_GC)) {
            cout << "Reserved (" << size << "): " << (void *) (poolCursor - size) << " - " << (void *) (poolCursor) << endl;
        }
        return allocatedMemoryPointer;
    }

    void HeapManager::runGarbageCollector(unsigned int queriedMemorySize) {
        if (gcStatus == GC__ACTIVE) {
            throw HeapException::GC::INSUFFICIENT_MEMORY__GC_ACTIVATED_IN_GC;
        }
        if (IS_DEBUG(DEBUG_GC)) {
            cout << "-------------------" << "GC started"<< endl<< poolBytesAllocated << "/" << poolSize << endl;
        }
        gcStatus = GC__ACTIVE;

        // set root
        if (resetGcEnabled) {
            resetGcFlags();
        }
        // find all accessible

        // copy pool
        if (currentPool == GC__POOL_LEFT) {
            currentPool = GC__POOL_RIGHT;
            poolCursor = rightPool;
            if (IS_DEBUG(DEBUG_GC)) {
                cout << "From left to right" << endl;
            }
        } else {
            currentPool = GC__POOL_LEFT;
            poolCursor = leftPool;
            if (IS_DEBUG(DEBUG_GC)) {
                cout << "From right to left" << endl;
            }
        }

        poolBytesAllocated = 0;

        VMObjectProxy* iterator = lastProxyObject;
        VMObjectProxy* previous = NULL;
        VMObjectProxy* chainHolder = NULL;

        while (iterator) {
            if (!iterator->alive) {
                if (IS_DEBUG(DEBUG_GC)) {
                    cout << "Cleaning" << endl << "\t" << iterator->instance()->toString() << endl;
                }
                previous = iterator->getPreviousProxy();

                iterator->setInstance(NULL);

                iterator = previous;
            } else {
                if (IS_DEBUG(DEBUG_GC)) {
                    cout << "Moving" << endl << "\t" << iterator->instance()->toString() << endl;
                }

                if (chainHolder) {
                    chainHolder->setPreviousProxy(iterator);
                }
                iterator->heapClone(this);
                chainHolder = iterator;

                iterator = iterator->getPreviousProxy();
            }
        }

        if (chainHolder) {
            chainHolder->setPreviousProxy(NULL);
        }

        // Just in dev to clearly see errors in GC
        if (currentPool == GC__POOL_LEFT) {
            clearPool(rightPool);
        } else {
            clearPool(leftPool);
        }

        gcStatus = GC__INACTIVE;
        if (IS_DEBUG(DEBUG_GC)) {
            cout << "-------------------" << "GC ended"<< endl<< poolBytesAllocated << "/" << poolSize << endl;
        }

        if (poolBytesAllocated + queriedMemorySize > poolSize) {
            throw HeapException::GC::INSUFFICIENT_MEMORY;
        }
    }


    void HeapManager::resetGcFlags() {
        VMObjectProxy* iterator = lastProxyObject;
        while (iterator) {
            iterator->resetFlags();

            iterator = iterator->getPreviousProxy();
        }
    }

    int HeapManager::getMaxMemory() {
        return poolSize;
    }

    int HeapManager::getMemoryUsage() {
        return poolBytesAllocated;
    }


    void HeapManager::clearPool(byte *pool) {
        memset(pool, 0, poolSize);
    }


    void HeapManager::dump() {
        VMObjectProxy* iterator = lastProxyObject;

        cout << "GC Proxy dump" << endl;
        while (iterator) {
            cout << iterator << ":" << iterator->instance() << ":";
            if (iterator->instance()) {
                if (iterator->instance()->getType() == VMObject::VMObjectRef) {
                    dumpRef((VMObjectRef*)iterator->instance());
                }
            } else {
                cout << "NULL" << endl;
            }

            iterator = iterator->getPreviousProxy();
        }
    }


    void HeapManager::dumpRef(VMObjectRef *ref) {
        cout << "\t\t" << "Class: " << ref->getClassRef()->getName() << endl;
        for (int i = 0; i < getPropertiesCount(ref->getClassRef()); i++) {
            cout << "\t\t\t" << i << ": " << (void**)(getPropertyPointer(ref, i, true)) << " > " <<  (void*)(*getPropertyPointer(ref, i, true)) << " I:";

            VMObject* property = *getPropertyPointer(ref, i, true);

            if (!property->instance()) {
                cout << " NULL";
            } else {
                cout << (void*)property->instance() << " || " << property->instance()->debugToString();
            }

            cout << endl;
        }
    }


    VMObject **HeapManager::getPropertyPointer(VMObjectRef *ref, int propertyIndex, bool mute) {
        if (!mute) {
            if (IS_DEBUG(DEBUG_GC)) {
                cout << "OBJ props: (" << (void**)ref->getPointer() <<")  >> property: " << propertyIndex << " >> " << (void**)(ref->getPointer() + propertyIndex) << endl;
            }
        }
        return ref->getPointer() + propertyIndex;
    }
};

#endif