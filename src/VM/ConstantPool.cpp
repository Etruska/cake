#ifndef VM_CONSTANTPOOL_CPP
#define VM_CONSTANTPOOL_CPP

#include "VM/VMObject.h"
#include "VM/ConstantPool.h"

#include <string>
#include <iostream>
#include <vector>
#include "VM/Integer.h"

using namespace std;

namespace VM {

#define CONSTANT_POOL__INIT__HELPER(__VALUE__, __VAR_NAME__) __VAR_NAME__ = new Integer(__VALUE__); add(__VAR_NAME__);

    ConstantPool::ConstantPool() {
        cout << "Constant pool init" << endl;
        CONSTANT_POOL__INIT__HELPER(false, c_FALSE);
        CONSTANT_POOL__INIT__HELPER(true, c_TRUE);
    }

    int ConstantPool::add(VMObject *constant) {
        int index = getIndexOf(constant);
        if (index == -1) {
            this->pool.push_back(constant);
            return this->pool.size() - 1;
        }
        return index;
    }

    int ConstantPool::getIndexOf(VMObject *constant) {
        for (vector<VMObject *>::size_type i = 0; i != this->pool.size(); i++) {
            if (this->pool[i]->equals(constant))
                return i;
        }
        return -1;
    }

    VMObject *ConstantPool::get(int index) {
        if (!isIndexInRange(index)) {
            cerr << "ConstantPool: Index " << index << " out of range." << endl;
        }

        return this->pool[index];
    }


    bool ConstantPool::isIndexInRange(int index) {
        return !(index < 0 || index >= this->pool.size());
    }
}

#endif