#include "VM/StackFrame.h"
#include "VM/VMException.h"

using namespace std;

namespace VM {

    StackFrame::StackFrame(VMObjectProxy * proxy, Method * method, StackFrame *parent) {
        if(proxy->instance()->getType() != VMObject::VMObjectRef){
            throw VMException::INVOKE_ON_NONOBJECT;
        }
        this->parent = parent;
        this->method = method;
        this->thisObjProxy = proxy;
        this->localsPool = new LocalsPool();
        this->bp = 0;
    }

    Bytecode* StackFrame::getBytecode() {
        if(this->method->getType() == Method::VMMethod){
            VMMethod * met = (VMMethod *) this->method;
            return met->getBytecode();
        } else {
            throw VMException::BUILTIN_FUNCTION_HAS_NO_BYTECODE;
        }
    }

    LocalsPool * StackFrame::getLocalsPool() {
        return this->localsPool;
    }

    void StackFrame::savePointer() {
        //cout << "Saving pointer to: " <<  this->getBytecode()->getPointer() << ", previous: " << this->bp << endl;
        this->bp = this->getBytecode()->getPointer();
    }

    void StackFrame::loadPointer() {
        //cout << "Loading pointer to: " <<  this->bp << ", previous: " << this->getBytecode()->getPointer() << endl;
        this->getBytecode()->setPointer(this->bp);
    }


    void StackFrame::dumpError() {
        if (method) {
            cout << ((VMObjectRef*)thisObjProxy->instance())->getClassRef()->getName() << ":" << this->method->getName() << ":" << getBytecode()->getPointer() << endl;
        } else {
            cerr << "NO method in frame" << endl;
        }

        if (parent) {
            parent->dumpError();
        }
    }

    void StackFrame::dump(bool root) {
        if (method) {
            cout << ((VMObjectRef*)thisObjProxy->instance())->getClassRef()->getName() << ":" << this->method->getName() << ":" << getBytecode()->getPointer() << endl;
            if (root) {
                getBytecode()->dump();
            }
        } else {
            cout << "NO method in frame" << endl;
        }

        if (parent) {
            parent->dump(false);
        }
    }
}
