#ifndef VM_VMOBJECTPROXY_CPP
#define VM_VMOBJECTPROXY_CPP

#include <VM/Integer.h>
#include "VM/VMObjectProxy.h"
#include "Heap/HeapException.h"
#include "Heap/HeapManager.h"

using namespace std;

namespace VM {
    VMObject* VMObjectProxy::instance() {
//        if (!_instance) {
//            throw Heap::HeapException::Heap::TRYING_TO_ACCESS_ALREADY_RELEASED_MEMORY;
//        }
        return _instance;
    }

    VMObjectProxy::VMObjectProxy(VMObject* instance, VMObjectProxy* previous, unsigned int size) {
        _instance = instance;
        _previous = previous;
        this->size = size;
    }

    void VMObjectProxy::resetFlags() {
        alive = copied = visited = false;
    }

    unsigned char VMObjectProxy::compare(VMObject *obj) {
        throw Heap::HeapException::CALLING_METHOD_ON_PROXY;
    }

    VMObject::Type VMObjectProxy::getType() {
        throw Heap::HeapException::CALLING_METHOD_ON_PROXY;
    }

    bool VMObjectProxy::equals(VMObject *obj) {
        throw Heap::HeapException::CALLING_METHOD_ON_PROXY;
    }

    string VMObjectProxy::toString() {
        throw Heap::HeapException::CALLING_METHOD_ON_PROXY;
    }

    VMObject *VMObjectProxy::clone(Heap::HeapManager *heap, bool forceCopy) {
        return heap->createProxyFor(instance()->clone(heap, forceCopy));
    }

    VMObjectProxy *VMObjectProxy::getPreviousProxy() {
        return _previous;
    }


    void VMObjectProxy::setInstance(VMObject *instance) {
        _instance = instance;
    }


    void VMObjectProxy::heapClone(Heap::HeapManager *heap) {
        _instance = instance()->clone(heap, true);
    }

    void VMObjectProxy::setPreviousProxy(VMObjectProxy *proxy) {
        _previous = proxy;
    }

    void VMObjectProxy::markAsAlive(Heap::HeapManager* heap) {
        this->alive = true;
        this->instance()->markAsAlive(heap);
    }
}

#endif