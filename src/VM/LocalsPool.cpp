#ifndef VM_LOCALSPOOL_CPP
#define VM_LOCALSPOOL_CPP

#include "VM/VMObject.h"
#include "VM/LocalsPool.h"
#include "VM/VMException.h"

#include <string>
#include <iostream>
#include <vector>

using namespace std;

namespace VM {

    void LocalsPool::store(int index, VMObject *obj) {
        pool.setItem(obj, index);
    }

    VMObject *LocalsPool::load(int index) {
        try {
            return pool.getById(index);
        } catch (...) {
            cerr << "LocalsPool: loading variable from invalid location " << index << endl;
            throw VMException::INVALID_LOCATION;
        }
    }

    int LocalsPool::getSize() {
        return pool.size();
    }


    void LocalsPool::dump() {
        pool.dump();
    }
}

#endif