#ifndef VM_VMOBJECTREF_CPP
#define VM_VMOBJECTREF_CPP

#include <iostream>
#include "VM/VMException.h"
#include "VM/VMObject.h"
#include "VM/VMObjectRef.h"
#include "Heap/HeapManager.h"

using namespace std;

namespace VM {

    VMObjectRef::VMObjectRef(VMObject** propertiesStart, VMClass *classRef) {
        this->objPointer = propertiesStart;
        this->classRef = classRef;
    }


    VMObject::Type VMObjectRef::getType() {
        return VMObject::VMObjectRef;
    }

    bool VMObjectRef::equals(VMObject *obj) {
        return sameType(obj) && ((VMObjectRef *) obj)->getPointer() == this->getPointer();
    }

    string VMObjectRef::toString() {
        return "(obj) " + this->classRef->getName();
    }

    string VMObjectRef::debugToString() {
        return "(" + this->typeName[this->getType()] + ") " + "\"" + this->toString() + "\"";
    }

    VMObject** VMObjectRef::getPointer() {
        return this->objPointer;
    }

    VMClass *VMObjectRef::getClassRef() {
        return this->classRef;
    }

    unsigned char VMObjectRef::compare(VMObject *obj) {
        if (!obj || !sameType(obj)) {
            throw VMException::UNCOMPARABLE_ENTRIES;
        }

        if (this->getPointer() == ((VMObjectRef *) obj)->getPointer()) {
            return COMPARE__EQUAL;
        }
        return COMPARE__NOT_EQUAL;
    }

    VMObject *VMObjectRef::clone(Heap::HeapManager* heap, bool forceCopy) {
        if (forceCopy) {
            return heap->heapCloneObject(this);
        }

        return this;
    }

    unsigned int VMObjectRef::heapSize() {
        int classRefSize = sizeof(VMClass *);
        int propertiesSize = (int)sizeof(VMObjectRef *) * classRef->getFields().size();
        int refSize = sizeof(VMObjectRef);

        return classRefSize + propertiesSize + refSize;
    }

    void VMObjectRef::markAsAlive(Heap::HeapManager* heap) {
        this->alive = true;
        for(int i = 0; i < this->getClassRef()->getFields().size(); i++){
            VMObject * obj = heap->getProperty(this, i);
            obj->markAsAlive(heap);
        }
    }
};

#endif