#ifndef VM_VMMETHOD_CPP
#define VM_VMMETHOD_CPP

#include "VM/VMMethod.h"
#include "VM/VMObject.h"
#include "VM/VMObject.h"
#include <string>

using namespace std;

namespace VM {


    VMMethod::VMMethod(string name, Bytecode *code) {
        this->name = name;
        this->bytecode = code;
    }

    Method::Type VMMethod::getType() {
        return Method::VMMethod;
    }
}

#endif