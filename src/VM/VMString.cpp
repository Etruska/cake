#ifndef VM_VMSTRING_CPP
#define VM_VMSTRING_CPP

#include <iostream>
#include <VM/VMException.h>
#include "VM/VMString.h"
#include "Heap/HeapManager.h"

using namespace std;

namespace VM {

    VMString::VMString(string value) {
        this->value = value;
    }

    string VMString::getValue() {
        return this->value;
    }

    VMObject::Type VMString::getType() {
        return VMObject::VMString;
    }

    bool VMString::equals(VMObject *obj) {
        return sameType(obj) && ((VMString *) obj)->getValue().compare(this->value) == 0;
    }

    string VMString::toString() {
        return this->value;
    }

    string VMString::debugToString() {
        return "(" + this->typeName[this->getType()] + ") " + "\"" + this->toString() + "\"";
    }


    unsigned char VMString::compare(VMObject *obj) {
        if (!obj || !sameType(obj)) {
            throw VMException::UNCOMPARABLE_ENTRIES;
        }

        int result = ((VMString *) obj)->getValue().compare(this->value);

        if (result > 0) {
            return COMPARE__BIGGER;
        } else if (result < 0) {
            return COMPARE__SMALLER;
        } else {
            return COMPARE__EQUAL;
        }
    }

    VMObject *VMString::clone(Heap::HeapManager* heap, bool forceCopy) {
        return heap->createRawString(this->value);
    }
};

#endif