#include "VM/NativeClasses/String.h"

#include <fstream>
#include <streambuf>

#include "Utils/stringUtils.h"

namespace VM {
    void ClassString::executeMethod(int methodId, VMObject* instance, VMCore* core) {
        if (methodId == this->methodMap->getByValue("getCharOnPosition")) {
            Integer* index = (Integer*) (core->getStack()->pop()->instance());
            VMString* content = (VMString*) (core->getStack()->pop()->instance());

            core->getStack()->push(core->getHeap()->createString(content->getValue().substr(index->getValue(), 1)));

            return;
        }

        if (methodId == this->methodMap->getByValue("pushCharBack")) {
            VMString* character = (VMString*) (core->getStack()->pop()->instance());
            VMString* content = (VMString*) (core->getStack()->pop()->instance());

            core->getStack()->push(core->getHeap()->createString(content->getValue().append(character->getValue())));

            return;
        }

        if (methodId == this->methodMap->getByValue("removeLastChar")) {
            VMString* content = (VMString*) (core->getStack()->pop()->instance());

            core->getStack()->push(core->getHeap()->createString(content->getValue().substr(0, content->getValue().length() - 1)));

            return;
        }

        if (methodId == this->methodMap->getByValue("getLength")) {
            VMString* content = (VMString*) (core->getStack()->pop()->instance());

            core->getStack()->push(core->getHeap()->createInteger(content->getValue().length()));

            return;
        }

        if (methodId == this->methodMap->getByValue("getLine")) {
            Integer* index = (Integer*) (core->getStack()->pop()->instance());
            VMString* content = (VMString*) (core->getStack()->pop()->instance());

            vector<string> lines = StringUtils::explode(content->getValue(), '\n');

            core->getStack()->push(core->getHeap()->createString(lines[index->getValue()]));

            return;
        };


        if (methodId == this->methodMap->getByValue("getExplodeSize")) {
            VMString* delimiter = (VMString*) (core->getStack()->pop()->instance());
            VMString* content = (VMString*) (core->getStack()->pop()->instance());
            //cout << "getExplodeSize|D:" << delimiter->getValue() << "|V:" << content->getValue() << endl;
            vector<string> lines = StringUtils::explode(content->getValue(), delimiter->getValue().at(0));

            core->getStack()->push(core->getHeap()->createInteger(lines.size()));

            return;
        }

        if (methodId == this->methodMap->getByValue("getExplodePart")) {
            Integer* index = (Integer*) (core->getStack()->pop()->instance());
            VMString* delimiter = (VMString*) (core->getStack()->pop()->instance());
            VMString* content = (VMString*) (core->getStack()->pop()->instance());

            //cout << "getExplodePart|D:" << delimiter->getValue() << "|V:" << content->getValue() << "|i:" << index->getValue() << endl;

            vector<string> lines = StringUtils::explode(content->getValue(), delimiter->getValue().at(0));

            core->getStack()->push(core->getHeap()->createString(lines[index->getValue()]));

            return;
        }

        if (methodId == this->methodMap->getByValue("concat")) {
            VMString* part2 = (VMString*) (core->getStack()->pop()->instance());
            VMString* part1 = (VMString*) (core->getStack()->pop()->instance());

            core->getStack()->push(core->getHeap()->createString(part1->getValue().append(part2->getValue())));

            return;
        }

        if (methodId == this->methodMap->getByValue("getLinesCount")) {
            VMString* content = (VMString*) (core->getStack()->pop()->instance());

            vector<string> lines = StringUtils::explode(content->getValue(), '\n');


            core->getStack()->push(core->getHeap()->createInteger(lines.size()));

            return;
        }

        if (methodId == this->methodMap->getByValue("toInteger")) {
            VMString* content = (VMString*) (core->getStack()->pop()->instance());

            core->getStack()->push(core->getHeap()->createInteger(StringUtils::stringToInt(content->getValue())));

            return;
        }


        cerr << "Unknow method for native class " << methodId  << " on " << name << endl;
        throw "Unknow method for native class";
    }

    void ClassString::registerMethodNames() {
        this->methodMap->getId("getCharOnPosition");
        this->methodMap->getId("getLength");
        this->methodMap->getId("pushCharBack");
        this->methodMap->getId("removeLastChar");
        this->methodMap->getId("getLine");
        this->methodMap->getId("getLinesCount");
        this->methodMap->getId("toInteger");
        this->methodMap->getId("concat");
        this->methodMap->getId("getExplodePart");
        this->methodMap->getId("getExplodeSize");
    }
}