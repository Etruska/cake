#include "VM/NativeClasses/IO.h"


namespace VM {
    void ClassIO::executeMethod(int methodId, VMObject* instance, VMCore* core) {
        if (methodId == this->methodMap->getByValue("print")) {
            VMObject* content = (VMObject*) (core->getStack()->pop()->instance());

            cout << content->toString();

            return;
        }

        if (methodId == this->methodMap->getByValue("printLine")) {
            VMObject* content = (VMObject*) (core->getStack()->pop()->instance());

            cout << content->toString() << endl;

            return;
        }

        if (methodId == this->methodMap->getByValue("printTab")) {
            cout << "\t";

            return;
        }

        if (this->methodMap->getByValue("lineBreak")) {
            cout << endl;

            return;
        }

        cerr << "Unknow method for native class " << methodId  << " on " << name << endl;
        throw "Unknow method for native class";
    }

    void ClassIO::registerMethodNames() {
        this->methodMap->getId("print");
        this->methodMap->getId("printLine");
        this->methodMap->getId("printTab");
        this->methodMap->getId("lineBreak");
    }
}