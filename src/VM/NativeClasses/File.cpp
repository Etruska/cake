#include "VM/NativeClasses/File.h"

#include <fstream>
#include <streambuf>

namespace VM {
    void ClassFile::executeMethod(int methodId, VMObject* instance, VMCore* core) {
        if (methodId == this->methodMap->getByValue("read")) {
            VMString* filePath = (VMString*) (core->getStack()->pop()->instance());
            ifstream t(filePath->getValue());
            string source((istreambuf_iterator<char>(t)),
                          istreambuf_iterator<char>());
            core->getStack()->push(core->getHeap()->createString(source));
            return;
        }

        cerr << "Unknow method for native class " << methodId  << " on " << name << endl;
        throw "Unknow method for native class";
    }

    void ClassFile::registerMethodNames() {
        this->methodMap->getId("read");
    }
}