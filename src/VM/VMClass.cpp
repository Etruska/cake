#ifndef VM_VMCLASS_CPP
#define VM_VMCLASS_CPP

#include "VM/VMClass.h"
#include "VM/VMException.h"

using namespace std;

namespace VM {
    VMClass::VMClass(string name, VMClass *superClass) {
        this->name = name;
        this->superClass = superClass;
    }

    int VMClass::addField(VMField *field) {
        this->fields.add(field);
        return this->fields.size() - 1;
    }

    VMField *VMClass::getField(int index) {
        if (index < 0 || index >= this->fields.size())
            cerr << "ClassField: Index " << index << " out of range." << endl;

        return this->fields.get(index);
    }

    VMClass *VMClass::getSuperClass() {
        return this->superClass;
    }


    ClassPool<VMField *> VMClass::getFields() {
        return this->fields;
    }
}

#endif