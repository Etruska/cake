#ifndef VM_BYTECODE_CPP
#define VM_BYTECODE_CPP

#include "VM/Bytecode.h"
#include "VM/VMException.h"
#include <iostream>
#include <bitset>
#include <iomanip>
#include <Compiler/compiler.h>

#include "VM/IS.h"

//#include "Utils/utils.h"
#include "Configuration/config.h"

using namespace std;

namespace VM {

    Bytecode::Bytecode() {

    }

    Bytecode::~Bytecode() {
        delete [] code;
    }

    Bytecode::Bytecode(const unsigned char *code, int length) {
        loadCode(code, length);
    }

    Bytecode::Bytecode(string code) {
        unsigned char* codeBuffer = (unsigned char*) malloc ((code.length() + 1) * sizeof(unsigned char));

        length = (int)code.copy((char*)codeBuffer, code.length());
        codeBuffer[length] = '\0';

        this->code = codeBuffer;
    }

    void Bytecode::loadCode(const unsigned char *code, int length) {
        this->code = code;
        this->length = length;
    }

    bool Bytecode::hasNext() {
        return (this->pointer <= this->length - 1);
    }

    unsigned char Bytecode::getNext() {
        unsigned char next = code[this->pointer++];
        return next;
    };

    unsigned char Bytecode::getCodeAt(int bp) {
        return this->code[bp];
    }

    int Bytecode::getPointer() {
        return pointer;
    }

    void Bytecode::setPointer(int bp) {
        if (bp > this->length - 1 || bp < 0) {
            cerr << "Cannot set bytecode pointer at position " << bp << ". (max " << this->length - 1 << ")" << endl;
            throw VMException::BP_OUT_OF_RANGE;
        }
        this->pointer = bp;
    }

#define OUT_COLOR(__COLOR__) // if (IS_COLORS) { colorize::setColor(rlutil::__COLOR__); }

    void Bytecode::dump(ConstantPool *constantPool) {
        for (std::size_t i = 0; i < length; i++) {
            if (pointer == i) {
                cout << ">>";
            } else {
                cout << "  ";
            }
            OUT_COLOR(WHITE)

            OUT_COLOR(BLUE)

            cout << setw(8) << i;

            OUT_COLOR(WHITE)

            cout << ": ";

            cout << bitset<8>((unsigned char) code[i]);


            OUT_COLOR(CYAN)
            cout << " | " << setw(12);
            OUT_COLOR(WHITE)

            if (code[i] & VM__INSTRUCTION) {
                cout << IS::toString((unsigned char) code[i]);
            } else {
                OUT_COLOR(GREEN)
                cout << bitset<8>((unsigned char) code[i]).to_ulong();
                OUT_COLOR(WHITE)

                if (constantPool) {
                    if (constantPool->isIndexInRange(code[i])) {
                        cout << " | " << constantPool->get(code[i])->toString();
                    } else {
                        cout << " | " << ((unsigned short) code[i]);
                    }


                }
            }

            cout << endl;
        }
    }
}

#endif