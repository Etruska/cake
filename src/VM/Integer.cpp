#ifndef VM_INTEGER_CPP
#define VM_INTEGER_CPP

#include <iostream>
#include "VM/Integer.h"
#include <string>
#include <VM/VMException.h>
#include "Heap/HeapManager.h"

using namespace std;

namespace VM {

    Integer::Integer(int value) {
        this->value = value;
    }

    Integer::Integer(string value) {
        this->value = atoi(value.c_str());
    }

    int Integer::getValue() {
        return this->value;
    }

    VMObject::Type Integer::getType() {
        return VMInteger;
    }

    bool Integer::equals(VMObject *obj) {
        return sameType(obj) && ((Integer *) obj)->getValue() == this->value;
    }

    unsigned char Integer::compare(VMObject *obj) {
        if (!sameType(obj)) {
            throw VMException::UNCOMPARABLE_ENTRIES;
        }

        int comparedValue = ((Integer *) obj)->getValue();

        if (value < comparedValue) {
            return COMPARE__SMALLER;
        } else if (value > comparedValue) {
            return COMPARE__BIGGER;
        } else {
            return COMPARE__EQUAL;
        }
    }

    string Integer::toString() {
        return to_string(this->value);
    }

    VMObject *Integer::clone(Heap::HeapManager* heap, bool forceCopy) {
        return heap->createRawInteger(this->value);
    }


    bool Integer::isFalse() {
        return this->getValue() == 0;
    }
};

#endif