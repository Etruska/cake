#ifndef VM_VMOBJECT_CPP
#define VM_VMOBJECT_CPP

#include "VM/VMObject.h"

using namespace std;

namespace VM {
    string VMObject::debugToString() {
        return "(" + getTypeName() + ") " + this->toString();
    }

    string VMObject::getTypeName() {
        return this->typeName[this->getType()];
    }


    VMObject *VMObject::instance() {
        return this;
    }


    bool VMObject::isGcManaged() {
        return this != instance();
    }

    unsigned int VMObject::heapSize() {
        return sizeof(this);
    }

    void VMObject::markAsAlive(Heap::HeapManager* heap) {
        if(this->isGcManaged()) {
            this->isAlive = true;
        }
    }


    bool VMObject::isFalse() {
        return false;
    }
}

#endif