#ifndef VM_STACK_CPP
#define VM_STACK_CPP

#include "VM/VMObject.h"
#include "VM/Stack.h"
#include "VM/VMException.h"
#include <iostream>

using namespace std;

namespace VM {

    void Stack::push(VMObject *obj) {

        this->items.push_back(obj);
    }


    VMObject *Stack::pop() {
        if (this->isEmpty())
            throw VMException::POP_ON_EMPTY_STACK;

        VMObject *ret = this->items.back();
        this->items.pop_back();
        return ret;
    }

    bool Stack::isEmpty() {
        return this->items.empty();
    }

    int Stack::size() {
        return this->items.size();
    }


    void Stack::dump() {
        cout << "- Stack:" << endl;
        for (VMObject* obj : items) {
            cout << "\t";
            if (obj->instance()) {
                cout << obj->instance()->debugToString();
            } else {
                cout << "NULL";
            }
            cout << endl;
        }
        cout << "---" << endl;
    }
}

#endif