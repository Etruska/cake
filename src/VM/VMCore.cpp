#ifndef VM_VMCORE_CPP
#define VM_VMCORE_CPP

#include <iostream>
#include <VM/IS.h>
#include <Heap/HeapManager.h>
#include "VM/VMCore.h"
#include "VM/VMException.h"
#include "Utils/stringUtils.h"
#include "Configuration/config.h"

using namespace std;

namespace VM {

    VMCore::VMCore(Heap::HeapManager* heap) {
        this->heap = heap;
        this->stack = new Stack;
        this->classPool = new ClassPool<VMClass *>;
    }

    VMCore::~VMCore() {
        delete this->stack;
        delete this->heap;
    }

    int VMCore::mergeBytes(char idx1, char idx2) {
        return ((int) idx1 << 8) + (int) idx2;
    }

    void VMCore::runBytecode(Bytecode *bytecode) {
        //create plain main class with main methode and given bytecode
        VMMethod * method = new VMMethod("main", bytecode);
        string className = "_main" + StringUtils::randomString(8);
        VMClass * mainClass = new VMClass(className, NULL);
        int methodIdx = mainClass->methods.setItem(method, 0);
        int classIdx = this->classPool->add(mainClass);
        this->run(classIdx, methodIdx);
    }

    void VMCore::runBytecode() {
        while (this->stackFrame && this->stackFrame->getBytecode()->hasNext()) {
            if (IS_DEBUG(DEBUG__VM)) {
                cout << "-D----------------------" << endl;
                this->stackFrame->dump();
                this->stack->dump();
                this->getLocalsPool()->dump();
                cout << "-D----------------------" << endl;
            }
            if (IS_DEBUG(DEBUG_GC)) {
                cout << "-D----------------------" << endl;
                this->stackFrame->dump(false);
                this->heap->dump();
                cout << "-D----------------------" << endl;
            }
            try {
                Bytecode *bytecode = this->stackFrame->getBytecode();
                unsigned char inst = bytecode->getNext();
                switch (inst) {
                    case IS::PUSH_CONST:
                        pushConst((int) bytecode->getNext());
                        break;
                    case IS::D_PRINT:
                        debugPrint();
                        break;
                    case IS::GOTO:
                        gotoIdx(this->mergeBytes(bytecode->getNext(), bytecode->getNext()));
                        break;
                    case IS::IF_CMPR:
                        ifCompare(mergeBytes(bytecode->getNext(), bytecode->getNext()));
                        break;
                    case IS::STORE:
                        store((int) bytecode->getNext());
                        break;
                    case IS::LOAD:
                        load((int) bytecode->getNext());
                        break;
                    case IS::INVOKE:
                        invoke((int) bytecode->getNext());
                        break;
                    case IS::RETURN:
                        methodReturn();
                        break;
                    case IS::NEW:
                        newObj((int) bytecode->getNext());
                        break;
                    case IS::GETFIELD:
                        getField((int) bytecode->getNext());
                        break;
                    case IS::SETFIELD:
                        setField((int) bytecode->getNext());
                        break;
                }

                if (IS::OPERATOR & inst) {
                    callOperator(inst);
                }
            } catch (...) {
                cout << "=========ERROR DUMP==================" << endl;
                this->stackFrame->dump();
                this->stack->dump();
                this->getLocalsPool()->dump();
                throw;
            }
        }
    }

    void VMCore::run(int classIdx, int methodIdx, int argId){
        VMClass * mainClass = this->classPool->get(classIdx);
        VMMethod * mainMethod = (VMMethod *)mainClass->methods.getById(methodIdx);
        cout << "MainClass: " << mainClass->getName() << endl;
        cout << "MainMethod: " << mainMethod->getName() << endl;

        cout << "-RUN--------------------------------------" << endl;
        VMClass * classRef = this->classPool->get(classIdx);
        VMObjectProxy *objProxy = this->heap->createObject(classRef);
        this->rootStackFrame = this->stackFrame = new StackFrame(objProxy, mainMethod, NULL);
        //push and store this instance
        this->stack->push(objProxy);
        this->store(0);

        if (argId != NO_ARG) {
            this->pushConst(argId);
        }

        this->runBytecode();
    }

    void VMCore::loadConstantPool(ConstantPool *pool) {
        this->constantPool = pool;
    }

    void VMCore::pushConst(int index) {
        this->stack->push(this->constantPool->get(index));
    }

    void VMCore::debugPrint() {
        VMObject *obj = this->stack->pop()->instance();
        stringstream ss;
        ss << obj->debugToString() << endl;
        this->lastPrintline = ss.str();
        this->printStream << ss.str();
        this->print();
    }

    void VMCore::gotoIdx(int jmpIdx) {
        this->stackFrame->getBytecode()->setPointer(jmpIdx);
    }

    void VMCore::ifCompare(int jmpIdx) {
        VMObject* top = this->stack->pop();

        if (!top->instance() || top->instance()->isFalse()) {
            gotoIdx(jmpIdx);
        }
    }

    void VMCore::newObj(int idx) {
        VMClass * classRef = this->classPool->get(idx);
        VMObjectRef *obj = (VMObjectRef*) this->heap->createObject(classRef);
        this->stack->push(obj);
    }


    void VMCore::getField(int idx) {
        VMObjectProxy* x = (VMObjectProxy*)this->stack->pop();

        //cout << "Getting s field from: " << x << endl;

        VMObjectRef * obj = (VMObjectRef *)x->instance();
        if(obj->getType() != VMObject::VMObjectRef){
            throw VMException::GETFIELD_FROM_NONOBJECT;
        }

        VMObjectProxy* propertyProxy = this->heap->getProperty(obj, idx);

        //cout << "And the file is: " << propertyProxy << endl;

        VMObject * property = propertyProxy->instance();

        //cout << "Getting field from: " << obj->toString() << ", idx: " << idx << ", value: " <<  property->toString() << ", type:" << property->getTypeName() << endl;

        this->stack->push(propertyProxy);
    }

    void VMCore::setField(int idx) {
        VMObject * field = this->stack->pop();

        if (!field->isGcManaged()) {
            VMObjectProxy* fieldProxy = new VMObjectProxy(field, NULL, field->heapSize());
            field = fieldProxy->clone(this->heap, true);
            delete fieldProxy;
        }

        VMObjectRef * obj = (VMObjectRef *)this->stack->pop()->instance();
        if(obj->getType() != VMObject::VMObjectRef){
            throw VMException::SETFIELD_TO_NONOBJECT;
        }

        //cout << "Setting field to: " << obj->toString() << ", idx: " << idx << ", value: " << field->instance()->toString() << endl;
        this->heap->setProperty(obj, idx, field);
    }


    void VMCore::invoke(int idx) {
        VMObjectProxy * objProxy = (VMObjectProxy *)this->stack->pop();
        VMObjectRef * obj = (VMObjectRef *)objProxy->instance();

        if (!obj) {
            throw VMException::INVOKE_ON_NIL;
        }

        if(obj->getType() != VMObject::VMObjectRef){
            throw VMException::INVOKE_ON_NONOBJECT;
        }

        if (obj->getClassRef()->getType() == VM__NATIVE_CLASS) {

            obj->getClassRef()->executeMethod(idx, obj, this);

        } else {
            this->stackFrame->savePointer();
            VMMethod * method = (VMMethod *) obj->getClassRef()->methods.getById(idx);
            this->stackFrame = new StackFrame(objProxy, method, this->stackFrame);
            this->stackFrame->loadPointer();
            //push and store this instance
            this->stack->push(objProxy);
            this->store(0);
        }
    }

    void VMCore::methodReturn() {
        this->stackFrame->savePointer();
        this->stackFrame = this->stackFrame->parent;
        if(this->stackFrame) {
            this->stackFrame->loadPointer();
        }
    }

    LocalsPool * VMCore::getLocalsPool() {
        return this->stackFrame->getLocalsPool();
    }

#define VM__OPERATOR_COMPARE__HELPER(__OPERATOR__, __MASK__) case __OPERATOR__:\
    tmp11 = this->stack->pop()->instance();\
    tmp21 = this->stack->pop()->instance();\
    tf = __MASK__ & tmp11->compare(tmp21);\
    if (IS_DEBUG(DEBUG__VM)) {\
        cout << bitset<8>(tf) << endl;\
    }\
    this->stack->push((__MASK__ & tmp21->compare(tmp11)) ? constantPool->c_TRUE : constantPool->c_FALSE); return;

#define VM__OPERATOR_BINARY__INTEGER__HELPER(__OPERATOR__, __OPERATION__) case __OPERATOR__:\
    tmp1 = (Integer *) this->stack->pop()->instance();\
    tmp2 = (Integer *) this->stack->pop()->instance();\
    if (IS_DEBUG(DEBUG__VM)) {\
        cout << "OP: " << string(#__OPERATION__) << "|" << #__OPERATOR__ << "|| "<< tmp1->getValue() << "|" << tmp2->getValue();\
    }\
    tmp11 = heap->createInteger(tmp2->getValue() __OPERATION__ tmp1->getValue());\
    tmp1 = (Integer*)tmp11->instance();\
    this->stack->push(tmp11);\
    if (IS_DEBUG(DEBUG__VM)) {\
        cout << " = " << tmp1->getValue() << endl;\
    }\
    return;


    void VMCore::callOperator(int operatorType) {
        Integer* tmp1;
        Integer* tmp2;
        VMObject* tmp11;
        VMObject* tmp21;
        unsigned char tf;
        switch (operatorType) {
            VM__OPERATOR_COMPARE__HELPER(VM__INSTRUCTION__OP__LESS_THAN, COMPARE__SMALLER);
            VM__OPERATOR_COMPARE__HELPER(VM__INSTRUCTION__OP__GREATER_THAN, COMPARE__BIGGER);
            VM__OPERATOR_COMPARE__HELPER(VM__INSTRUCTION__OP__GREATER_OR_EQ, COMPARE__BIGGER_OR_EQUAL);
            VM__OPERATOR_COMPARE__HELPER(VM__INSTRUCTION__OP__LESS_OR_EQ, COMPARE__SMALLER_OR_EQUAL);
            VM__OPERATOR_COMPARE__HELPER(VM__INSTRUCTION__OP__EQUAL, COMPARE__EQUAL);
            VM__OPERATOR_COMPARE__HELPER(VM__INSTRUCTION__OP__NOT_EQUAL, COMPARE__NOT_EQUAL);

            ////////////////////
            // TODO: only integers
            ////////////////////

            VM__OPERATOR_BINARY__INTEGER__HELPER(VM__INSTRUCTION__OP__PLUS, +);
            VM__OPERATOR_BINARY__INTEGER__HELPER(VM__INSTRUCTION__OP__MINUS, -);
            VM__OPERATOR_BINARY__INTEGER__HELPER(VM__INSTRUCTION__OP__DIVIDE, /);
            VM__OPERATOR_BINARY__INTEGER__HELPER(VM__INSTRUCTION__OP__RESIDUUM, %);
            VM__OPERATOR_BINARY__INTEGER__HELPER(VM__INSTRUCTION__OP__TIMES, *);

            case VM__INSTRUCTION__OP__NEGATE:
                this->stack->push(this->stack->pop()->instance()->isFalse() ? constantPool->c_TRUE : constantPool->c_FALSE);
                return;
        };
    }

    void VMCore::print() {
        cout << this->lastPrintline;
    }


    Stack *VMCore::getStack() {
        return this->stack;
    }

    void VMCore::store(int idx) {
        VMObject *obj = this->stack->pop();
        this->getLocalsPool()->store(idx, obj);
    }

    void VMCore::load(int idx) {
        VMObject *obj = this->getLocalsPool()->load(idx);
        this->stack->push(obj);
    }

    string VMCore::getLastPrintline() {
        return this->lastPrintline;
    }


    Heap::HeapManager *VMCore::getHeap() {
        return this->heap;
    }
}

#endif