#ifndef VM_VMCLASS_CPP
#define VM_VMCLASS_CPP

#include "VM/VMNativeClass.h"

using namespace std;

namespace VM {
    VMNativeClass::VMNativeClass(string name, Pool<string>* methodMap) : VMClass(name, NULL) {
        this->methodMap = methodMap;
    }
}

#endif