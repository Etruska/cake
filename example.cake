* ChildClass < ParentClass
	+ new(x, y, z)
	    # TODO
		# ^.new(x, y)

		@.x = x
		@.y = y
		@.z = z

	+ publicMethod(x, y, z)
		@.x = 10 + 5

		tmp = x.data(x, y)

		? protectedMethod(tmp, y, z) > x
			| "ok"
		:? x < 1
			| "maybe ok"
		:
			| "not ok"

		# TODO: for: % 0..2: i
		#	| i

		% i
			| i

		<< @.x

	+ methodWithException()
		!
			<! Exception.new(100, test)
		~ Exception
			# catch
		~
			# catch all exceptions